// <copyright file="LayerPainterInspector.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>07/01/2016</date>
// <summary>Inspector for Layer Painter</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SceneLayers
{
    /// <summary>
    /// Inspector for Layer Painter
    /// </summary>
    [System.Serializable]
    [KnownType(typeof(PlacementBrush))]
    [KnownType(typeof(RemoveBrush))]
    [CustomEditor(typeof(LayerPainter))]
    public class LayerPainterInspector : Editor
    {
        public SerializedObject src;
        private readonly string[] brushTypes = new string[] { "Placement", "Removal" };
        private List<IBrush> brushes = new List<IBrush>();
        private LayerPainter layerPainter;
        private string newBrushName = "New Brush";
        private int newBrushType = 0;
        private bool showNewBrush = false;
        private bool showSaveBrushWindow = false;
       
        /// <summary>
        /// Delegate for Serialization
        /// </summary>
        public void OnDeserializeLayerPainter()
		{
            if (this.layerPainter.SerializedBrushes.Length > 0)
            {
                this.brushes = LayerPainterUtility.DeserializeBrushes(this.layerPainter.SerializedBrushes);

                for(int i = 0; i < this.brushes.Count; i++)
                {
                    this.brushes[i].Parent = this.layerPainter.transform;
                }
            }
        }

        /// <summary>
        /// Unity On Disable Callback, unlink delegates and clean up
        /// </summary>
        public void OnDisable()
        {
            EditorApplication.update -= Update;
			this.layerPainter.onDrawGizmos -= this.OnDrawGizmos;
			Tools.hidden = false;

			this.brushes [this.layerPainter.CurrentBrush].OnDisable ();
        }

        /// <summary>
        /// On Draw Gizmos Callback
        /// </summary>
        public void OnDrawGizmos()
        {
            this.brushes[this.layerPainter.CurrentBrush].OnDrawGizmos();
        }

        /// <summary>
        /// On Enable Callback
        /// </summary>
        public void OnEnable()
        {
			if (target != null)
			{
				this.src = new SerializedObject (target);
				this.layerPainter = (LayerPainter)target;
				EditorApplication.update += Update;

				// Set Up Serialization Delegates
				if (this.layerPainter != null)
				{
					this.layerPainter.onSerializeLayerPainter = this.OnSerializeLayerPainter;
					this.layerPainter.onDrawGizmos += this.OnDrawGizmos;
					this.OnDeserializeLayerPainter();
					this.PrimeBrushes();

                    for(int i = 0; i < this.brushes.Count; i++)
                    {
                        this.brushes[i].Parent = this.layerPainter.transform;
                    }
				}
			}

			this.brushes [this.layerPainter.CurrentBrush].OnDisable ();
  
        }
        
        /// <summary>
        /// Inspector Callback
        /// </summary>
        override public void OnInspectorGUI()
        {
            src.Update();

			EditorGUILayout.Separator ();

			if (brushes != null && brushes.Count > 0)
			{
				EditorGUILayout.LabelField ("Current Brush: ", EditorStyles.boldLabel);
				string[] brushNames = new string[brushes.Count];

				for(int i = 0; i < brushes.Count; i++)
				{
					brushNames [i] = (i + 1) + ": " + brushes [i].Name;
				}

				layerPainter.CurrentBrush = EditorGUILayout.Popup (layerPainter.CurrentBrush, brushNames, GUILayout.Height(30));
			}

			EditorGUILayout.Separator ();

			EditorGUILayout.BeginHorizontal ();

            // There is a 10 pixel font width difference between PC and Mac
			if (GUILayout.Button ("Load Brush", GUILayout.MaxWidth (80), GUILayout.Height(20)) == true)
			{
				IBrush loadedBrush = LayerPainterFileDialogs.LoadDialog ();
				if (loadedBrush != null)
				{
					brushes.Add (loadedBrush);
					layerPainter.CurrentBrush = this.brushes.Count - 1;
					brushes [this.brushes.Count - 1].Parent = layerPainter.transform;
				}
			}

			if (GUILayout.Button ("Save Brush", GUILayout.MaxWidth (80), GUILayout.Height(20)) == true)
			{
				LayerPainterFileDialogs.SaveDialog(this.brushes[this.layerPainter.CurrentBrush]);
			}
				
			if (GUILayout.Button ("New Brush", GUILayout.MaxWidth (80), GUILayout.Height(20)) == true)
			{
				showNewBrush = !showNewBrush;
			}

			if (GUILayout.Button ("Remove Brush", GUILayout.MaxWidth (100), GUILayout.Height (20)) == true)
			{
				this.brushes.Remove (this.brushes[this.layerPainter.CurrentBrush]);

				if (this.brushes.Count == 0)
				{
					this.PrimeBrushes ();
				}

				if (this.layerPainter.CurrentBrush > 0)
				{
					this.layerPainter.CurrentBrush = this.layerPainter.CurrentBrush - 1;
				} 
				else
				{
					this.layerPainter.CurrentBrush = 0;
				}
			}

			if (showNewBrush == true)
			{
				GUILayout.EndHorizontal ();
				GUILayout.BeginHorizontal ();
				EditorGUILayout.LabelField ("New Brush Name: ");
				newBrushName = EditorGUILayout.TextField (newBrushName);
				GUILayout.EndHorizontal ();

				GUILayout.BeginHorizontal ();
				EditorGUILayout.LabelField ("New Brush Type: ");
				newBrushType = EditorGUILayout.Popup (newBrushType, brushTypes);

				if(GUILayout.Button("Create New Brush") == true)
				{
					switch (newBrushType)
					{
						case 0:
							this.brushes.Add(new PlacementBrush(newBrushName));
							break;

						case 1:
							this.brushes.Add(new RemoveBrush(newBrushName));
							break;
					}

					layerPainter.CurrentBrush = brushes.Count - 1;
					showNewBrush = false;
					newBrushName = "New Brush Name";
					this.brushes [this.brushes.Count - 1].Parent = this.layerPainter.transform;
				}
			}
				
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.Separator();

			if (brushes.Count - 1 >= layerPainter.CurrentBrush)
			{
				this.brushes [layerPainter.CurrentBrush].OnInspectorGUI ();
			}

            src.ApplyModifiedProperties();
        }

        /// <summary>
        /// On Scene GUi Callback
        /// </summary>
        public void OnSceneGUI()
        {
            Tools.hidden = true;
            if (brushes != null && brushes.Count > 0)
            {
				if (brushes.Count - 1 >= layerPainter.CurrentBrush)
				{
					brushes [layerPainter.CurrentBrush].OnSceneGUI ();
				}
            }
        }

        /// <summary>
        /// Delegate for Serialization
        /// </summary>
        public void OnSerializeLayerPainter()
        {
            if (layerPainter != null)
            {
                layerPainter.SerializedBrushes = LayerPainterUtility.SerializeBrushes(this.brushes);
            }
        }

        /// <summary>
        /// Prime Brushes, if there are no brushes this will set up two default brushes
        /// A layer painter should always have a brush
        /// </summary>
        public void PrimeBrushes()
        {
            if(this.brushes.Count == 0)
            {
                brushes.Add(new PlacementBrush("Default Placement"));
                brushes.Add(new RemoveBrush("Defualt Removal"));
            }
        }
       
        /// <summary>
        /// Update Callback
        /// </summary>
        public void Update()
        {
			//Debug.Log (this.layerPainter.CurrentBrush + "   " + this.brushes.Count);
			if (brushes [this.layerPainter.CurrentBrush] != null)
			{
				brushes [this.layerPainter.CurrentBrush].Update ();
			}
        }
	}
}

