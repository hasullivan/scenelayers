﻿// <copyright file="LayerPainterFileDialogs.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>07/01/2016</date>
// <summary>File Dialogs for Layer Painter</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


using UnityEngine;
using UnityEditor;
using System.IO;

namespace SceneLayers
{
    /// <summary>
    /// File Dialogs for Layer Painter
    /// </summary>
	public class LayerPainterFileDialogs : EditorWindow {

        /// <summary>
        /// Save Brush Dialog
        /// </summary>
        /// <param name="brushPreset">Brush to save</param>
		public static void SaveDialog(IBrush brushPreset)
		{
			string path = EditorUtility.SaveFilePanel ("Save Brush Preset", Application.dataPath, brushPreset.Name ,"bpr");

			if (path.Length > 0)
			{
				brushPreset.Name = Path.GetFileNameWithoutExtension(path);
				LayerPainterUtility.SaveBrushPreset (brushPreset, path);
			}

		}

        /// <summary>
        /// Brush Load Dialog
        /// </summary>
        /// <returns>Brush</returns>
		public static IBrush LoadDialog()
		{
			string path = EditorUtility.OpenFilePanel ("Load Brush Preset", Application.dataPath, "bpr");

			if (path.Length > 0)
			{
				return LayerPainterUtility.LoadBrushPreset (path);
			}
			else
			{
				return null;
			}
		}
	}
}
