﻿// <copyright file="SceneLayerInspector.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>07/01/2016</date>
// <summary>Editor Inspector For Scene Layer</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using UnityEngine;
using UnityEditor;

namespace SceneLayers
{
    /// <summary>
    /// Scene Layer Inspector UI
    /// </summary>
    [System.Serializable]
    [CustomEditor(typeof(SceneLayer))]
    [CanEditMultipleObjects]
    public class SceneLayerInspector : Editor
    {
        /// <summary>
        /// Scene Layer Reference
        /// </summary>
        [SerializeField]
        SceneLayer sceneLayer;

        /// <summary>
        /// source / target object (scene layer as serialized object)
        /// </summary>
        public SerializedObject src;
        
        /// <summary>
        /// On Enable callback
        /// </summary>
        void OnEnable()
        {
            // No longer used
            src = new SerializedObject(target);
            sceneLayer = (SceneLayer)target;
            // Cannot Hide tools in Scene Layer as it will prevent using tools in children
            //Tools.hidden = false;
        }


        /// <summary>
        /// On Inspector GUI 
        /// </summary>
        override public void OnInspectorGUI()
        {
            src.Update();
            DrawHierarchyTintColor();
            DrawDescription();
            DrawNotes();
            ShowSceneLayerTools();
            src.ApplyModifiedProperties();
            src.Update();

            if(GUI.changed == true)
            {
                EditorApplication.RepaintHierarchyWindow();
            }
        }

        /// <summary>
        /// Draw the Tint color used in the hierarchy view for fast identification
        /// </summary>
        private void DrawHierarchyTintColor()
        {
            EditorGUILayout.LabelField("Hierarchy Tint Color");
            sceneLayer.TintColor = EditorGUILayout.ColorField(sceneLayer.TintColor);
            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.Space();
        }

        /// <summary>
        /// Draw the Scene Layers Descriptions
        /// </summary>
        private void DrawDescription()
        {
            EditorGUILayout.LabelField("Description");
            sceneLayer.Description = EditorGUILayout.TextArea(sceneLayer.Description);
        }

        /// <summary>
        /// Draw the Scene Layers Notes
        /// </summary>
        private void DrawNotes()
        {
            EditorGUILayout.LabelField("Notes");
            sceneLayer.Notes = EditorGUILayout.TextArea(sceneLayer.Notes);
        }

        /// <summary>
        /// Draw the Scene Layers tools
        /// </summary>
        private void ShowSceneLayerTools()
        {
            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.Space();

            Color bgColor = GUI.backgroundColor;

            if (GUILayout.Button(new GUIContent("Empty Scene Layer", "Remove all of the game objects from this Scene Layer")))
            {
                Undo.RegisterCompleteObjectUndo(sceneLayer, "Detached Scene Layer");
                sceneLayer.transform.DetachChildren();
            }

            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.Space();

            //TODO: Needs a rework to deal with Attached Layer Painters and Serialization
            //if (GUILayout.Button(new GUIContent("Merge Selected Scene Layers", "Merges the selected Scene Layers into a single Scene Layer")))
            //{
            //    Undo.RegisterCompleteObjectUndo(sceneLayer, "Merged Layers");
            //    SceneLayerUtility.MergeSelectedLayers();
            //}

            //EditorGUILayout.Space();
            //EditorGUILayout.Space();
            //EditorGUILayout.Space();

            GUI.backgroundColor = new Color(1.0f, 0.3f, 0.3f, 1.0f);

            if (GUILayout.Button(new GUIContent("Delete Layer without content", "Delete the Scene layer, but do not delete game objects in the Scene Layer.")))
            {
                Undo.RegisterCompleteObjectUndo(sceneLayer, "Empty and Delete");
                sceneLayer.transform.DetachChildren();
                DestroyImmediate(sceneLayer.gameObject, false);
            }

			EditorGUILayout.Space();

			if (GUILayout.Button(new GUIContent("Delete Content In Layer", "Keep the Scene Layer, but Delete All its Content.")))
			{
				Undo.RegisterCompleteObjectUndo(sceneLayer, "Delete Contents");
				SceneLayerUtility.DeleteSceneLayerContents (this.sceneLayer);
			}

            GUI.backgroundColor = bgColor;
            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.Space();
        }
    }
}



