﻿// <copyright file="SceneLayerHierarchy.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>07/01/2016</date>
// <summary>Scene Layers Hierarchy</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using UnityEngine;
using UnityEditor;

namespace SceneLayers{
	
	/// <summary>
	/// Scene layer hierarchy.
	/// </summary>
    [InitializeOnLoad]
    [ExecuteInEditMode]
    public class SceneLayerHierarchy : MonoBehaviour{ // does this need to be a monobehaviour?

    	public static Object hierarchyWindow;
    	public static Texture2D SceneLayerInvisibleIcon;
    	public static Texture2D SceneLayerVisibleIcon;
    	public static Texture2D SceneLayerIsolatedIcon;
    	public static Texture2D SceneLayerUnisolatedIcon;
    	public static Texture2D SceneLayerLockedIcon;
    	public static Texture2D SceneLayerUnlockedIcon;
    	public static Texture2D SceneLayerPersistantOnIcon;
    	public static Texture2D SceneLayerPersistantOffIcon;
    	public static GUIStyle editorOverride = new GUIStyle();
    	public static GameObject listItem;

        /// <summary>
        /// Static Constructor
        /// </summary>
        static SceneLayerHierarchy()
        {
            LoadTextures();
            EditorApplication.hierarchyWindowItemOnGUI += DrawSceneLayer;
            EditorApplication.RepaintHierarchyWindow();
	    }
    	
        /// <summary>
        /// Draw the Scene Layer Overlay in the Hierarchy
        /// </summary>
        /// <param name="instanceID"></param>
        /// <param name="drawingRect"></param>
    	private static void DrawSceneLayer( int instanceID, Rect drawingRect)
        {
            listItem = (GameObject)EditorUtility.InstanceIDToObject(instanceID);
            // If an item is deleted it can cause a null referenece, check if list item is null
            Component sceneLayerComponent = new Component();// = listItem.GetComponent("SceneLayer");

            if(listItem != null)
            {
                sceneLayerComponent = listItem.GetComponent("SceneLayer");
            }

    		if(sceneLayerComponent != null)
            {
                SceneLayer sceneLayer = listItem.GetComponent<SceneLayer>();          		
    			Color bgColor = GUI.backgroundColor;	
                DrawBox(sceneLayer, drawingRect);
                GUI.backgroundColor = bgColor;
                DrawButtons(sceneLayer, drawingRect);	
    		}		
    	}

        /// <summary>
        /// Draw the Colored Box
        /// </summary>
        /// <param name="sceneLayer"></param>
        /// <param name="drawingRect"></param>
        public static void DrawBox(SceneLayer sceneLayer, Rect drawingRect)
        {
            GUIStyle labelStyle = new GUIStyle();
            labelStyle.normal.textColor = Color.white;

            GUI.backgroundColor = sceneLayer.TintColor;

			GUI.Box(new Rect(drawingRect.x + 1, drawingRect.y, drawingRect.width + 15, 16), new GUIContent("", sceneLayer.Description));
            GUIContent labelName = new GUIContent(sceneLayer.name);
            GUI.Label(new Rect(drawingRect.x + 5, drawingRect.y + 1, drawingRect.width + 15, 17), labelName, labelStyle);
        }

        /// <summary>
        /// Draw the Freeze Button
        /// </summary>
        /// <param name="sceneLayer"></param>
        /// <param name="buttonPosition"></param>
        public static void DrawFrozenButton(SceneLayer sceneLayer, Rect buttonPosition)
        {
            if (sceneLayer.Frozen)
            {
                if (GUI.Button(buttonPosition, new GUIContent(SceneLayerLockedIcon, "Click to unlock the layer"), editorOverride))
                {
                    Undo.RegisterCompleteObjectUndo(sceneLayer, "UnfrozeLayer");
                    sceneLayer.UnfreezeLayer();
                }
            }

            else
            {
                if (GUI.Button(buttonPosition, new GUIContent(SceneLayerUnlockedIcon, "Click to lock the layer"), editorOverride))
                {
                    Undo.RegisterCompleteObjectUndo(sceneLayer, "Freeze Layer");
                    sceneLayer.FreezeLayer();
                }
            }
        }

        /// <summary>
        /// Draw Layer Isolation Button
        /// </summary>
        /// <param name="sceneLayer"></param>
        /// <param name="buttonPosition"></param>
        public static void DrawIsolationButton(SceneLayer sceneLayer, Rect buttonPosition)
        {
            if (sceneLayer.LayerIsIsolated == true)
            {
                if (GUI.Button(buttonPosition, new GUIContent(SceneLayerIsolatedIcon, "Click to remove layer isolation"), editorOverride))
                {
                    Undo.RegisterCompleteObjectUndo(sceneLayer, "Unisolate Layer");
                    sceneLayer.UnisolateLayer();
                }
            }
            else
            {
                if (GUI.Button(buttonPosition, new GUIContent(SceneLayerUnisolatedIcon, "Click to isolate layer"), editorOverride))
                {
                    Undo.RegisterCompleteObjectUndo(sceneLayer, "Isolate Layer");
                    sceneLayer.IsolateLayer();
                }
            }
        }

        /// <summary>
        /// Draw the Layer Visibility Icon
        /// </summary>
        /// <param name="sceneLayer"></param>
        /// <param name="buttonPosition"></param>
        public static void DrawVisibilityButton(SceneLayer sceneLayer, Rect buttonPosition)
        {
            if (sceneLayer.Hidden == true)
            {
                if (GUI.Button(buttonPosition, new GUIContent(SceneLayerInvisibleIcon, "Click to unhide Layer"), editorOverride))
                {
                    Undo.RegisterCompleteObjectUndo(sceneLayer, "Unhide Layer");
                    sceneLayer.UnhideLayer();
                }
            }
            else
            {
                if (GUI.Button(buttonPosition, new GUIContent(SceneLayerVisibleIcon, "Click to hide layer"), editorOverride))
                {
                    Undo.RegisterCompleteObjectUndo(sceneLayer, "Hide Layer");
                    sceneLayer.HideLayer();
                }
            }
        }

        /// <summary>
        /// Draw Layer Persistance Icon
        /// </summary>
        /// <param name="sceneLayer"></param>
        /// <param name="buttonPosition"></param>
        public static void DrawPersistantButton(SceneLayer sceneLayer, Rect buttonPosition)
        {
            if (sceneLayer.IsPersistant == true)
            {
                if (GUI.Button(buttonPosition, new GUIContent(SceneLayerPersistantOnIcon, "Click to turn off persistnace"), editorOverride))
                {
                    Undo.RegisterCompleteObjectUndo(sceneLayer, "Persistance Off");
                    sceneLayer.RemovePersistance();
                }
            }
            else
            {
                if (GUI.Button(buttonPosition, new GUIContent(SceneLayerPersistantOffIcon, "Click to turn on persistnace"), editorOverride))
                {
                    Undo.RegisterCompleteObjectUndo(sceneLayer, "Persistance On");
                    sceneLayer.MakePersistant();
                }
            }
        }

        /// <summary>
        /// Draw All of the Layers Buttons in the Hierarchy
        /// </summary>
        /// <param name="sceneLayer"></param>
        /// <param name="drawingRect"></param>
        public static void DrawButtons(SceneLayer sceneLayer, Rect drawingRect)
        {
            Rect frozenButtonPosition = new Rect(drawingRect.width, drawingRect.y, 16, 16);
            Rect isolationBUttonPosition = new Rect(drawingRect.width - 18, drawingRect.y, 16, 16);
            Rect visibilityButtonPosition = new Rect(drawingRect.width - 36, drawingRect.y, 16, 16);
            Rect persistantButtonPosition = new Rect(drawingRect.width - 54, drawingRect.y, 16, 16);

            DrawFrozenButton(sceneLayer, frozenButtonPosition);
            DrawIsolationButton(sceneLayer, isolationBUttonPosition);
            DrawVisibilityButton(sceneLayer, visibilityButtonPosition);
            DrawPersistantButton(sceneLayer, persistantButtonPosition);
        }

        /// <summary>
        /// Load The Icon Textures
        /// </summary>
        public static void LoadTextures()
        {		
		    SceneLayerVisibleIcon = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/SceneLayers/Editor/InternalResources/SceneLayerVisibleIcon.tif",typeof(Texture2D));
		    SceneLayerInvisibleIcon = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/SceneLayers/Editor/InternalResources/SceneLayerInvisibleIcon.tif",typeof(Texture2D));
		    SceneLayerIsolatedIcon = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/SceneLayers/Editor/InternalResources/SceneLayerIsolatedIcon.tif",typeof(Texture2D));
		    SceneLayerUnisolatedIcon = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/SceneLayers/Editor/InternalResources/SceneLayerUnisolatedIcon.tif",typeof(Texture2D));
		    SceneLayerLockedIcon = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/SceneLayers/Editor/InternalResources/SceneLayerLockedIcon.tif",typeof(Texture2D));
		    SceneLayerUnlockedIcon = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/SceneLayers/Editor/InternalResources/SceneLayerUnlockedIcon.tif",typeof(Texture2D));
		    SceneLayerPersistantOnIcon = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/SceneLayers/Editor/InternalResources/SceneLayerPersistantOnIcon.tif", typeof(Texture2D));
		    SceneLayerPersistantOffIcon = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/SceneLayers/Editor/InternalResources/SceneLayerPersistantOffIcon.tif", typeof(Texture2D));
	    }
    }	
}



