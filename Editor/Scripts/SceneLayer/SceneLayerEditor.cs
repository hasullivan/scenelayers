﻿// <copyright file="SceneLayerEditor.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>07/01/2016</date>
// <summary>Scene Layer Editor</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using UnityEngine;
using UnityEditor;

namespace SceneLayers {

    /// <summary>
    /// Pop Up Window to create a Scene Layer
    /// </summary>
    public class SceneLayerEditor : EditorWindow
    {
        private string newLayerName = "Scene Layer";
        private string newDescription = string.Empty;
        private string newNotes = string.Empty;
        private bool attachLayerPainter = true;

        /// <summary>
        /// Initialize
        /// </summary>
        [MenuItem("GameObject/Create Other/Scene Layer %l")]
        static void Init()
        {
            EditorWindow.GetWindow(typeof(SceneLayerEditor));
        }

        /// <summary>
        /// Editor GUI
        /// </summary>
        void OnGUI()
        {
            GUILayout.Label("Create Scene Layer", EditorStyles.boldLabel);
            newLayerName = EditorGUILayout.TextField("Scene Layer Name", newLayerName);
            newDescription = EditorGUILayout.TextField("Scene Layer Description", newDescription);
            newNotes = EditorGUILayout.TextField("Scene Layer notes", newNotes);
            attachLayerPainter = EditorGUILayout.Toggle("Attach Layer Painter", attachLayerPainter);
            EditorGUILayout.Space();
            EditorGUILayout.Space();

            if (GUILayout.Button("Create"))
            {              
                GameObject newSceneLayer = SceneLayerUtility.CreateLayerObject(newLayerName);
                Undo.RegisterCreatedObjectUndo(newSceneLayer, "Scene Layer Created");
                newSceneLayer.GetComponent<SceneLayer>().Description = newDescription;
                newSceneLayer.GetComponent<SceneLayer>().Notes = newNotes;
                if (attachLayerPainter == true)
                {
                    newSceneLayer.AddComponent<LayerPainter>();
                }
            }
        }
    }	
}
