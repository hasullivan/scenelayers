// <copyright file="SmartObject.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>07/01/2016  </date>
// <summary>Smart Object</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using UnityEngine;

using System.Collections.Generic;
using UnityEditor;

// Old and inefficient method. Create a new one that connects the the scene layer and manages its children
// Set up to use Filters and modifiers

[ExecuteInEditMode]
public class SmartObject : MonoBehaviour {
	
	public bool actAutonomously = true;
	// should the object snap to the object below it?
	public bool snapToSurface = true;
	public float surfaceSnapDistance = 5.0f;
	public Vector3 Center = Vector3.zero;
	// should the object confrom to the surface normal?
	public bool alignToSurface = false;
	// uses boundingbox to check for conflicts
	public bool noOverlappingBounds = true;
	public bool useMinProximity = false;
	// the minimum proximity between it and any other object
	public float minProximity = 3.0f;
	// if this object is colliding with another placed object should it destroy itself?
	public bool destroyIfConflicted = false;
	// if this object is colliding with another placed object should it move itself?
	public bool moveIfConflicted = true;
	// how many frames will pass before update
	public int updateRate = 10;
	private int tick = 0;
	
	
	// reuseables
	Vector3 rayOrigin = Vector3.zero;
	
	Vector3 rayOffset = new Vector3(0.0f,1.0f,0.0f);
	RaycastHit hit = new RaycastHit();
	Ray ray = new Ray();
	Collider[] conflicts = new Collider[0];
	private bool hitSurface = false;
	private int ignoreLayer = 0;
	private int currLayer = 0;
	// the surface we are currently attached to if any
	private GameObject surface;
	//
	private List<Transform> kids = new List<Transform>();
	private List<int> kidLayers = new List<int>();
	private List<Collider> nonChildrenCollisions = new List<Collider>();
	
	void OnEnable()
	{
		ignoreLayer = LayerMask.NameToLayer("Ignore Raycast");
		
		EditorApplication.update += SmartUpdate;	
		
		Transform[] sceneOBJs = (Transform[])FindObjectsOfType(typeof(Transform));
	}
	
	void SmartUpdate()
	{
		// use a ticker to skip updates
		tick += 1;
		if(tick >= updateRate)
		{
			tick = 0;	
			PerformChecks();
		}
	}
	void OnDisable()
	{
		EditorApplication.update -= SmartUpdate;	
	}
	
	public void PerformChecks()
	{
		CacheChildrenAndLayer();
		if(GetSurface() == true)
		{
			SnapToSurface();
		}
		if(alignToSurface == true)
		{
			AlignWithSurface();
		}
		if(useMinProximity == true)
		{
			ArrayUtility.Clear<Collider>(ref conflicts);
			GetConflicts();
			ResolveConflicts();
			
		}
		
	}
	
	// move in worldspace away from the point of the collider
	// untill minProximity distance is reached
	public void MoveOutOfWay(Vector3 opponentPosition)
	{
		Vector3 relativePosition = transform.position - opponentPosition;
		relativePosition.Normalize();
		relativePosition.Scale(new Vector3(0.2f,0.2f,0.2f));
		
		transform.position += relativePosition;
	}

    public void SnapToSurface()
	{
		if(snapToSurface)
		{
			if(hitSurface == true)
			{
				transform.position = hit.point + Center;
			}
		}
	}
	
	public bool GetSurface()
	{
		hitSurface = false;
		
		rayOrigin = transform.position + Center + rayOffset;
		
		ray.origin = rayOrigin;
		ray.direction = Vector3.down;

		if(surfaceSnapDistance > 0)
		{
		
			SwapLayerMask();
			if(Physics.Raycast(ray,out hit,surfaceSnapDistance) == true)
			{
				hitSurface = true;	
				surface = hit.transform.gameObject;
			}
			else
			{
				surface = null;
				
			}
			RevertLayerMask();
		}
		return hitSurface;
		
	}

    public void GetConflicts()
	{
		// need to take into account children and ignore tham
		ArrayUtility.Clear<Collider>(ref conflicts);
		nonChildrenCollisions.Clear();
		conflicts = Physics.OverlapSphere(transform.position + Center,minProximity);
		for(int i = 0; i < conflicts.Length; i++)
		{
			if(conflicts[i].transform.IsChildOf(transform) == false )
			{
				nonChildrenCollisions.Add(conflicts[i]);
				
			}
			
		}
		
	}
	
	public void SwapLayerMask()
	{
		currLayer = gameObject.layer;
		gameObject.layer = ignoreLayer;
		for(int i =0 ; i < kids.Count; i ++)
		{
			kids[i].gameObject.layer = ignoreLayer;
			
		}
	}

    public void RevertLayerMask()
	{
		gameObject.layer = currLayer;
		for(int i =0 ; i < kids.Count; i ++)
		{
			kids[i].gameObject.layer = kidLayers[i];
			
		}	
		
	}
	
	public void AlignWithSurface()
	{
		if(hitSurface == true)
		{	
			transform.rotation = Quaternion.FromToRotation(Vector3.up,hit.normal);	
		}
		
	}

	public void ResolveConflicts()
	{
		
		for(int i = 0; i < nonChildrenCollisions.Count; i++)
		{
		
			if(nonChildrenCollisions[i].transform.gameObject != surface && nonChildrenCollisions[i].transform.gameObject != gameObject)
			{
//					Debug.Log("I am a conflict " + nonChildrenCollisions[i].gameObject.name);
					MoveOutOfWay(nonChildrenCollisions[i].transform.position);				
			}
			
			
		}
		
	}

	public void CacheChildrenAndLayer()
	{
		kids.Clear();
		kidLayers.Clear();
		foreach(Transform child in transform)
		{
			kids.Add(child);
			//Debug.Log("I am a child named " + child.name);
			kidLayers.Add(child.gameObject.layer);
			
		}
		
	}
	
}
