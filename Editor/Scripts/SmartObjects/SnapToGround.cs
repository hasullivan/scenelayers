// <copyright file="SnapToGround.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>07/01/2016</date>
// <summary>Simple Script to force an object to stick to the ground when editing</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


using UnityEngine;
using System.Collections;


// Old, Get Rid of this
[ExecuteInEditMode]
public class SnapToGround : MonoBehaviour {


    public bool enableSnapping = false;
	public bool hasBeenMoved = true;
	public Vector3 currPos;
	public Vector3 oldPos;
	// Use this for initialization
	void Awake() 
	{
		currPos = transform.position;
		oldPos = currPos;
	}
	
	// Update is called once per frame
	void Update ()
	{
        if (enableSnapping == true)
        {
            currPos = transform.position;

            if (currPos != oldPos)
            {
                hasBeenMoved = true;
            }

            if (currPos == oldPos)
            {
                hasBeenMoved = false;
            }

            if (hasBeenMoved)
            {
                // find the ground and move the position there	
                RaycastHit hit;

                if (Physics.Raycast(new Vector3(transform.position.x, transform.position.y, transform.position.z), -Vector3.up, out hit))
                {
                    float distanceToGround = hit.distance;
                    transform.position = new Vector3(transform.position.x, (transform.position.y - distanceToGround), transform.position.z);
                }
            }

            oldPos = currPos;
        }
	}
}
