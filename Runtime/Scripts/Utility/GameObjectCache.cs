﻿// <copyright file="GameObejctCache.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>07/01/2016</date>
// <summary>Game Object Cache for Layer Painter</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using UnityEngine;
using System.Collections.Generic;

namespace SceneLayers
{
    /// <summary>
    /// Game Object Cache
    /// </summary>
	public sealed class GameObjectCache
	{
		static readonly GameObjectCache instance = new GameObjectCache();
		private HashSet<GameObject> cache = new HashSet<GameObject>();

        /// <summary>
        /// Default Constructor
        /// </summary>
        public GameObjectCache() { }

        /// <summary>
        /// Get the Instance
        /// </summary>
		public static GameObjectCache Instance
		{
			get
			{
				return instance;
			}
		}

        /// <summary>
        /// Gets or sets the Game Obejct Cache
        /// </summary>
		public HashSet<GameObject> Cache
		{
			get
			{
				return cache;
			}
			set
			{
				cache = value;
			}
		}

        /// <summary>
        /// Add a Game Object to the Cache
        /// </summary>
        /// <param name="gameObject"></param>
		public void AddGameObject(GameObject gameObject)
		{
			cache.Add (gameObject);
		}

        /// <summary>
        /// Remove a Game Obejct from the Cache
        /// </summary>
        /// <param name="gameObject">Game Object</param>
		public void RemoveGameObject(GameObject gameObject)
		{
			cache.Remove (gameObject);
		}
	}
}
