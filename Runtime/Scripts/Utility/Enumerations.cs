﻿// <copyright file="Enumerations.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>07/01/2016</date>
// <summary>Enumerations for Layer Painter</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

namespace SceneLayers
{
    /// <summary>
    /// Includion Mode, for including or excuding a reference from filtering
    /// </summary>
    public enum InclusionMode
    {
        Permitted,
        Excluded
    };

    /// <summary>
    /// Paint Mode for brushes
    /// </summary>
    public enum PaintMode
    {
        Place,
        Remove,
    };

    /// <summary>
    /// Placement Brush Method
    /// </summary>
    public enum PlacementMode
    {
        Scatter,
        Single,
        Stepped
    };

    /// <summary>
    /// Removal brush method
    /// </summary>
    public enum RemovalMode
	{
		Prune,
		Delete
	};

    /// <summary>
    /// Proximity Mode for Fitlering
    /// </summary>
    public enum ProximityMode
	{
		ExcludeIfInProximity,
		IncludeIfInProximity
	}

    /// <summary>
    /// General Brush Catagory Type
    /// </summary>
    public enum BrushType
    {
        Placement,
        Removal
    }
}
