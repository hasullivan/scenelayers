﻿// <copyright file="LayerPainterUtiltiy.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>07/01/2016</date>
// <summary>Utiltiy Methods for Layer Painter</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace SceneLayers
{

    /// <summary>
    /// Static Utility class for layer painter
    /// </summary>
    public static class LayerPainterUtility
    {
		#if UNITY_EDITOR
        /// <summary>
        /// Adds a thumbnail to the Scene Layer Cache
        /// </summary>
        /// <param name="thumbnail">Thumbnail to cache</param>
        /// <param name="fileName">Name to save as</param>
        public static void CacheThumbnail(Texture2D thumbnail, string fileName)
        {
			
            if (AssetDatabase.LoadAssetAtPath(@"/SceneLayers/Editor/InternalResources/Cache/" + fileName + " .png", typeof(Texture2D)) == null)
            {
                byte[] thumbPNG = thumbnail.EncodeToPNG();
                //AssetDatabase.CreateAsset(thumbnail, Application.dataPath + @"/SceneLayers/Editor/Resources/Cache/" + fileName + ".png");
                FileStream file = File.Open(Application.dataPath + @"/SceneLayers/Editor/InternalResources/Cache/" + fileName + " .png", FileMode.Create);
                BinaryWriter writer = new BinaryWriter(file);
                writer.Write(thumbPNG);
                file.Close();
                AssetDatabase.ImportAsset(@"Assets/SceneLayers/Editor/InternalResources/Cache/" + fileName + " .png", ImportAssetOptions.Default);
            }

        }
		#endif
        /// <summary>
        /// Gets a list of every childs transform position in this layer
        /// </summary>
        /// <returns>List of world positions as Vector3</returns>
        public static List<Vector3> GetChildPositionsList(LayerPainter layerPainter)
        {
            List<Vector3> childPositions = new List<Vector3>();
            Transform[] children = layerPainter.GetComponentsInChildren<Transform>();

            if (children.Length > 0)
            {
                for(int i = 0; i < children.Length; i++)
                {
                    childPositions.Add(children[i].position);
                }
            }

            return childPositions;
        }
	

        /// <summary>
        /// Gets the angles of the of the normal as a float
        /// </summary>
        /// <param name="normal">Normal</param>
        /// <returns>Float, 0 is up and 180 is down </returns>
        public static float GetSurfaceAngle(Vector3 normal)
        {
            float angle = 0.0f;
            angle = Vector3.Angle(normal, Vector3.up);
            return angle;
        }

		#if UNITY_EDITOR
        /// <summary>
        /// Gets a thumbnail for an asset, will return null if
        /// </summary>
        /// <param name="asset">Asset to try to create thumbnail from</param>
        /// <returns>Texture 3d or null if thumbnail failed</returns>
        public static Texture2D GetThumbnail(Object asset)
        {
            Texture2D thumbnail = LayerPainterUtility.LoadThumbnailFromCache(asset.name);

            if (thumbnail == null)
            {
                // Limit trys to prevent editor freeze, there is no Garauntee that Asset Preview will ever return a thumbnail
                int maxIterations = 10;
                int interations = 0;

                // Hack to deal with Get Asset Preview being async on the back end
                // Need to developer own renderer to create thumbs and get away from Asset Preview and improve thumb quality
                while (thumbnail == null)
                {
                    thumbnail = AssetPreview.GetAssetPreview(asset);
                    Thread.Sleep(45);

                    if (interations == maxIterations)
                    {
                        break;
                    }

                    interations += 1;
                }

				if (thumbnail != null)
				{
					LayerPainterUtility.CacheThumbnail (thumbnail, asset.name);
					//Now load again, but from cache, this unsures that the URL is cache URL
					thumbnail = LayerPainterUtility.LoadThumbnailFromCache(asset.name);
				} 
            }

            return thumbnail;
        }
		#endif	

		#if UNITY_EDITOR
        /// <summary>
        /// Loades a thumbnail from the cache
        /// </summary>
        /// <param name="name">Name of thumbnail to load</param>
        /// <returns>Texture2D , Null if file not in cache</returns>
        public static Texture2D LoadThumbnailFromCache(string name)
		{
            return (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/SceneLayers/Editor/InternalResources/Cache/" + name + " .png", typeof(Texture2D));
        }
		#endif	

		/// <summary>
		/// 
		/// </summary>
		/// <param name="point"></param>
		/// <param name="normal"></param>
		/// <param name="worldPosition"></param>
		/// <returns></returns>
		public static Vector3 AlignPoint(Vector3 randomPoint, Vector3 normal, Vector3 worldPosition)
		{
			Vector3 alignedPoint = randomPoint;
			//Debug.Log ("World Position: " + worldPosition);
			float mag = randomPoint.magnitude;
			// normalize point
			alignedPoint.Normalize();
			// rotate to match normal
			Quaternion rot = Quaternion.LookRotation(normal);
			alignedPoint = rot * alignedPoint;
			// scale point back up
			alignedPoint = alignedPoint * mag;
			// cast a ray back to the object
			alignedPoint = alignedPoint + worldPosition;

			return alignedPoint;
		}

		/// <summary>
		/// Get a random Point between min and max radius
		/// </summary>
		/// <param name="minRadius"></param>
		/// <param name="maxRadius"></param>
		/// <returns></returns>
		public static Vector3 GetRandomPoint(float minRadius, float maxRadius)
		{
			Vector3 randomPoint = new Vector3();

			float randX = 0.0f;
			float randZ = 0.0f;
			float radius = Random.Range(minRadius, maxRadius);
			float angle = Random.Range(0.0f, 360.0f);
			randX = radius * Mathf.Cos(angle);
			randZ = radius * Mathf.Sin(angle);
			randomPoint = new Vector3(randX, randZ, 3.0f);

			return randomPoint;
		}

		/// <summary>
		/// Gets the random sample point.
		/// </summary>
		/// <returns>The random sample point.</returns>
		/// <param name="direction">Direction.</param>
		/// <param name="worldPosition">World position.</param>
		/// <param name="minimumRadius">Minimum radius.</param>
		/// <param name="maximumRadius">Maximum radius.</param>
		public static RaycastHit GetRandomSamplePoint(Vector3 worldPosition, Vector3 normal, float minimumRadius, float maximumRadius)
		{
			RaycastHit instancePoint = new RaycastHit();
			Vector3 randomPoint = GetRandomPoint(minimumRadius, maximumRadius);
			randomPoint = LayerPainterUtility.AlignPoint(randomPoint, normal, worldPosition);
			Ray ray = new Ray(randomPoint, - normal);
			Physics.Raycast(ray, out instancePoint);

			return instancePoint;
		}

		#if UNITY_EDITOR
		/// <summary>
		/// Casta a ray into the Scene View and returns any hit information
		/// </summary>
		/// <returns>RaycastHit, null if no ray intersection</returns>
		public static bool CastMouseRay(out RaycastHit hit)
		{
			Event e = Event.current;
			Camera cam = SceneView.lastActiveSceneView.camera;
			Vector3 tmp = new Vector3(e.mousePosition.x, -e.mousePosition.y + cam.pixelHeight);
			Vector3 viewportPoint = new Vector3(tmp.x / cam.pixelWidth, tmp.y / cam.pixelHeight, 0.0f);

			// Is the mouse in the viewport?
			if (viewportPoint.x < 0.0f || viewportPoint.x > 1.0f || viewportPoint.y < 0.0f || viewportPoint.y > 1.0f)
			{
				hit = new RaycastHit();
				return false;
			}
			else
			{
				Ray ray = cam.ViewportPointToRay(viewportPoint);
				return Physics.Raycast(ray, out hit); ;
			}
		}
		#endif
			
        /// <summary>
        /// This method takes a list of frequencies of random sizes and selects the index based of random number.
        /// Frequencies can be in any range
        /// </summary>
        /// <returns>Index that was randomly selected</returns>
        public static int GetRandomIndexByFrequencies(List<float> frequencies)
        {
            int index = 0;
            float sum = 0;
            float currRangeMin = 0;
            float currRangeMax = 0;
            float randomNumber = 0;

            // sum frequecies to find range
            for (int i = 0; i < frequencies.Count; i++)
            {
                sum += frequencies[i];
            }

            Random.seed = Random.Range(0, 100000000);
            randomNumber = Random.Range(0.0f, sum);

            for (int i = 0; i < frequencies.Count; i++)
            {
                if (i == 0)
                {
                    currRangeMax = frequencies[i];
                }
                else
                {
                    currRangeMin = currRangeMin + frequencies[i - 1];
                    currRangeMax = currRangeMax + frequencies[i];
                }
                if (randomNumber > currRangeMin && randomNumber <= currRangeMax)
                {
                    index = i;
                    break;
                }
            }
            return index;
        }

		#if UNITY_EDITOR
        /// <summary>
        /// Saves a brush as a brush preset
        /// </summary>
		public static void SaveBrushPreset(IBrush brush, string path)
        {
			if (brush != null)
			{
				using (MemoryStream stream = new MemoryStream())
				{
					BinaryFormatter binaryFormatter = new BinaryFormatter();

					// Handle Surrogates for Unity's Unserializable Types
					SurrogateSelector surrogateSelector = new SurrogateSelector();
					surrogateSelector.AddSurrogate(typeof(Vector3), new StreamingContext(StreamingContextStates.All),new Vector3Surrogate());
					surrogateSelector.AddSurrogate(typeof(Vector2), new StreamingContext(StreamingContextStates.All), new Vector2Surrogate());
					surrogateSelector.AddSurrogate(typeof(Color), new StreamingContext(StreamingContextStates.All), new ColorSurrogate());

					binaryFormatter.SurrogateSelector = surrogateSelector;

					binaryFormatter.Serialize(stream, brush);
					File.WriteAllBytes (path, stream.ToArray ());
					AssetDatabase.Refresh ();
				}
			}
        }
		#endif

		/// <summary>
		/// Doeses the preset exist.
		/// </summary>
		/// <returns><c>true</c>, if preset exist was doesed, <c>false</c> otherwise.</returns>
		/// <param name="name">Name.</param>
		public static bool DoesPresetExist(string path, string name)
		{
			DirectoryInfo directoryInfo = new DirectoryInfo (path);
			FileInfo[] files = directoryInfo.GetFiles ();
			string fullName = name + ".bps";
			bool doesExist = false;

			foreach (FileInfo fileInfo in files)
			{
				if(fileInfo.Name == fullName)
				{
					
					doesExist = true;
				}
			}
			return doesExist;
		}

        /// <summary>
        /// Loads a brush preset
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
		public static IBrush LoadBrushPreset(string path)
		{
			try
			{
				byte[] brush = File.ReadAllBytes (path);
			
				using (MemoryStream stream = new MemoryStream (brush))
				{
					BinaryFormatter binaryFormatter = new BinaryFormatter ();
				
					SurrogateSelector surrogateSelector = new SurrogateSelector ();
					surrogateSelector.AddSurrogate (typeof(Vector3), new StreamingContext (StreamingContextStates.All), new Vector3Surrogate ());
					surrogateSelector.AddSurrogate (typeof(Vector2), new StreamingContext (StreamingContextStates.All), new Vector2Surrogate ());
					surrogateSelector.AddSurrogate (typeof(Color), new StreamingContext (StreamingContextStates.All), new ColorSurrogate ());
					//surrogateSelector.AddSurrogate (typeof(Quaternion), new StreamingContext (StreamingContextStates.All), new QuaternionSurrogate ());
					//surrogateSelector.AddSurrogate (typeof(Transform), new StreamingContext (StreamingContextStates.All), new TransformSurrogate ());
					//surrogateSelector.AddSurrogate (typeof(Scene), new StreamingContext (StreamingContextStates.All), new SceneSurrogate ());
			
					binaryFormatter.SurrogateSelector = surrogateSelector;
				
					return (IBrush)binaryFormatter.Deserialize (stream);
				
				}
			}
			catch(FileNotFoundException e)
			{
				throw e;
			}

		}

        /// <summary>
        /// Serializes a list of brushes. Used for in editor serialization to work around Unitys inablity to serialize interfaces
        /// </summary>
        /// <param name="brushes"></param>
        /// <returns>Byte Array</returns>
        public static byte[] SerializeBrushes(List<IBrush> brushes)
        {
			if (brushes != null && brushes.Count > 0)
            {
                using (MemoryStream stream = new MemoryStream())
                {
                    BinaryFormatter binaryFormatter = new BinaryFormatter();

                    // Handle Surrogates for Unity's Unserializable Types
                    SurrogateSelector surrogateSelector = new SurrogateSelector();
                    surrogateSelector.AddSurrogate(typeof(Vector3), new StreamingContext(StreamingContextStates.All),new Vector3Surrogate());
                    surrogateSelector.AddSurrogate(typeof(Vector2), new StreamingContext(StreamingContextStates.All), new Vector2Surrogate());
                    surrogateSelector.AddSurrogate(typeof(Color), new StreamingContext(StreamingContextStates.All), new ColorSurrogate());
                    //surrogateSelector.AddSurrogate(typeof(GameObject), new StreamingContext(StreamingContextStates.All), new GameObjectSurrogate());
                    //surrogateSelector.AddSurrogate(typeof(Quaternion), new StreamingContext(StreamingContextStates.All), new QuaternionSurrogate());
                    //surrogateSelector.AddSurrogate(typeof(Transform), new StreamingContext(StreamingContextStates.All), new TransformSurrogate());
                    //surrogateSelector.AddSurrogate(typeof(Scene), new StreamingContext(StreamingContextStates.All), new SceneSurrogate());
                    
                    binaryFormatter.SurrogateSelector = surrogateSelector;
                    binaryFormatter.Serialize(stream, brushes);
                     
                    return stream.ToArray();
                }
            }
            else
            {
                return null;
            }
            
        }

        /// <summary>
        /// Deserialize brush list from byte array
        /// </summary>
        /// <param name="serializedData"></param>
        /// <returns></returns>
        public static List<IBrush> DeserializeBrushes(byte[] serializedData)
        {
            if(serializedData != null && serializedData.Length > 0)
            {
                using (MemoryStream stream = new MemoryStream(serializedData))
                {
                    BinaryFormatter binaryFormatter = new BinaryFormatter();

					SurrogateSelector surrogateSelector = new SurrogateSelector();
					surrogateSelector.AddSurrogate(typeof(Vector3), new StreamingContext(StreamingContextStates.All),new Vector3Surrogate());
					surrogateSelector.AddSurrogate(typeof(Vector2), new StreamingContext(StreamingContextStates.All), new Vector2Surrogate());
					surrogateSelector.AddSurrogate(typeof(Color), new StreamingContext(StreamingContextStates.All), new ColorSurrogate());
					//surrogateSelector.AddSurrogate(typeof(GameObject), new StreamingContext(StreamingContextStates.All), new GameObjectSurrogate());
					//surrogateSelector.AddSurrogate(typeof(Quaternion), new StreamingContext(StreamingContextStates.All), new QuaternionSurrogate());
					//surrogateSelector.AddSurrogate(typeof(Transform), new StreamingContext(StreamingContextStates.All), new TransformSurrogate());
					//surrogateSelector.AddSurrogate(typeof(Scene), new StreamingContext(StreamingContextStates.All), new SceneSurrogate());

					binaryFormatter.SurrogateSelector = surrogateSelector;
                    List<IBrush> brushes = (List<IBrush>)binaryFormatter.Deserialize(stream);

                    return brushes;
                }

            }
            else
            {
                return null;
            }  
        }
    
		#if UNITY_EDITOR
        /// <summary>
        /// Get the Editor Window Reference to Unity Project Browser
        /// </summary>
        /// <returns></returns>
		public static EditorWindow GetProjectBrowser()
		{
			EditorWindow projectBrowser = null;
			EditorWindow[] windows = (EditorWindow[])Resources.FindObjectsOfTypeAll(typeof(EditorWindow));

			foreach(EditorWindow window in windows)
			{
				if (window.GetType().ToString() == "UnityEditor.ProjectBrowser")
				{
					projectBrowser = window;
					break;
				}
			}

			return projectBrowser;
		}
		#endif

        /// <summary>
        /// Gets the Bounds for a game object that includes all of its children
        /// </summary>
        /// <param name="gameObject">Game Object</param>
        /// <returns>Bounds</returns>
		public static Bounds GetBounds(GameObject gameObject)
		{
			Bounds bounds = new Bounds();
			Renderer[] renderers = gameObject.GetComponentsInChildren<Renderer> ();
			Vector3 newBoundsCenter = Vector3.zero;

			if (renderers.Length > 0)
			{
                for(int i = 0; i < renderers.Length; i++)
                {
                    newBoundsCenter += renderers[i].bounds.center;
                }

				bounds.center = newBoundsCenter / renderers.Length;

                for (int i = 0; i < renderers.Length; i++)
                {
                    bounds.Encapsulate (renderers[i].bounds);
				}
			}

			return bounds;
		}

        /// <summary>
        /// Disable all colliders in the game object. In Single mode the instances collider wil cause chaos.
        /// </summary>
        /// <param name="gameObject">Game Obejct</param>
        /// <returns></returns>
		public static Collider[] DisableActiveColliders(GameObject gameObject)
		{
			Collider[] colliders = gameObject.GetComponentsInChildren<Collider> (false);

			for (int i = 0; i <  colliders.Length; i++)
			{
				colliders[i].enabled = false;
			}

			return colliders;
		}

        /// <summary>
        /// Enable All of a Game Object Colliders. Once done placing and object, turn its colliders back on.
        /// </summary>
        /// <param name="colliders">Array of Colliders</param>
		public static void EnableColliders(Collider[] colliders)
		{
			for (int i = 0; i < colliders.Length; i++)
			{
				if (colliders[i] != null)
				{
					colliders[i].enabled = true;
				}
			}
		}

        /// <summary>
        /// Gets all of the transforms from the scene transform cache
        /// </summary>
        /// <returns></returns>
		public static List<Transform> GetSceneTransforms()
		{
			SceneTransformCache sceneRendererCache = SceneTransformCache.Instance;
			return sceneRendererCache.Cache;
		}

        /// <summary>
        /// Cache all of the scenes transforms into the Transform cache
        /// </summary>
		public static void CacheSceneTransforms()
		{
			SceneTransformCache sceneRendererCache = SceneTransformCache.Instance;
			List<Transform> sceneRenderers = new List<Transform>(GameObject.FindObjectsOfType<Transform> ());
			sceneRendererCache.Cache = sceneRenderers;
		}

        /// <summary>
        /// Cache a game objects children into the Game Object Cache
        /// </summary>
        /// <param name="gameObject">Game Object</param>
		public static void CacheChildGameObjects(GameObject gameObject)
		{
			GameObjectCache gameObjectCache = GameObjectCache.Instance;
			Transform[] childTransforms = gameObject.GetComponentsInChildren<Transform> ();
			gameObjectCache.Cache.Clear ();

			for(int i = 0; i < childTransforms.Length; i++)
			{
				if (childTransforms [i].gameObject.name != gameObject.name)
				{	
					#if UNITY_EDITOR
					gameObjectCache.AddGameObject (PrefabUtility.FindPrefabRoot (childTransforms [i].gameObject));
					#else
					gameObjectCache.AddGameObject(childTransforms[i].gameObject);
					#endif
				}
			}
		}

        /// <summary>
        /// Get Game Obejcts from the Game Object Chache
        /// </summary>
        /// <returns></returns>
		public static HashSet<GameObject> GetGameObjectsFromCache()
		{
			GameObjectCache gameObjectCache = GameObjectCache.Instance;
			return gameObjectCache.Cache;
		}

        /// <summary>
        /// Remove a Game object from he Game Object Cache
        /// </summary>
        /// <param name="gameObject">Game Object</param>
		public static void RemoveGameObjectFromCache(GameObject gameObject)
		{
			GameObjectCache gameObjectCache = GameObjectCache.Instance;
			gameObjectCache.RemoveGameObject (gameObject);
		}

		// TODO make this work at runtime 

		/// <summary>
        /// Adds an Instance to the Scene Transform Cache, not sure why its called "Paint Instance".
        /// </summary>
        /// <param name="gameObject">Game Obejct</param>
        /// <returns></returns>
		public static GameObject PaintInstance(GameObject gameObject)
		{
			#if UNITY_EDITOR
			GameObject instance = (GameObject)PrefabUtility.InstantiatePrefab(gameObject);
			#else
			GameObject instance = GameObject.Instantiate (gameObject);
			#endif

			SceneTransformCache sceneTransformsCache = SceneTransformCache.Instance;
			sceneTransformsCache.AddTransform(instance.transform);

			foreach (Transform transform in instance.transform)
			{
				sceneTransformsCache.AddTransform(transform);
			}

			return instance;
		}

		//TODO need a runtime version that does not rely on Prefab Utility

        /// <summary>
        /// Gets all transforms from the root transform of the provided transform.
        /// Unitys roots are a bit wonky. Since everything in the scene has a transform the "Root" it the first transform node at scene root.
        /// This is not what is wanted for a game objects root. We want the Root of the instance, not the scene graph root.
        /// This methods gets the "prefab root" (Editor Only) then gets all child transforms. This is needed to deal with compound objects
        /// </summary>
        /// <param name="transform"></param>
        /// <returns></returns>
		public static List<Transform> GetCompoundObjectsTransforms(Transform transform)
		{
			#if UNITY_EDITOR
			GameObject root = PrefabUtility.FindPrefabRoot (transform.gameObject);
			#else
			GameObject root = transform.gameObject;
			#endif
			List<Transform> transforms = new List<Transform> (root.GetComponentsInChildren<Transform> ());
			return transforms;
		}

	}
}
