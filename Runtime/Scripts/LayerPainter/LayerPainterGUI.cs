﻿// <copyright file="LayerPainterGUI.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>07/01/2016</date>
// <summary>GUI Items for Layer Painter</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SceneLayers
{
    /// <summary>
    /// Static Class for Layer Painter GUI Methods
    /// </summary>
    public static class LayerPainterGUI
    {
		public static readonly Color noModeColor = new Color(0.4f, 0.4f, 0.4f,0.4f);
		public static readonly Color paintModeColor = new Color(0.0f, 1.0f, 0.0f, 0.1f);
		public static readonly Color singleModeColor = new Color (0.0f, 0.0f, 1.0f, 0.1f);
		public static readonly Color steppedModeColor = new Color(1.0f, 0.0f, 1.0f, 0.1f);
		public static readonly Color steppedInfoColor = new Color (1.0f, 0.0f, 1.0f, 1.0f);
		public static readonly Color steppedToolColor = new Color (1.0f, 0.0f, 1.0f, 0.7f);
		public static readonly Color deleteModeColor = new Color(1.0f, 0.0f, 0.0f, 0.2f);
		public static readonly Color pruneModeColor = new Color(1.0f, 0.5f, 0.3137f, .2f);
    	
		/// <summary>
		/// Draws the brush handle.
		/// </summary>
		/// <param name="worldPoint">World point.</param>
		/// <param name="worldNormal">World normal.</param>
		/// <param name="radius">Radius.</param>
		/// <param name="color">Color.</param>
		public static void DrawBrushHandle(Vector3 worldPoint, Vector3 worldNormal, float radius, Color color)
		{
			#if UNITY_EDITOR
			HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
			Handles.color = color;
			Handles.DrawSolidDisc (worldPoint, worldNormal, radius);
			Color borderColor = new Color(color.r, color.g, color.b, color.a + 0.3f);
			Handles.color = borderColor;
			Handles.DrawWireDisc (worldPoint, worldNormal, radius);
			HandleUtility.Repaint();
			#endif
		}

		/// <summary>
		/// Draws the double brush tool handle.
		/// </summary>
		/// <param name="worldPoint">World point.</param>
		/// <param name="worldNormal">World normal.</param>
		/// <param name="innerRadius">Inner radius.</param>
		/// <param name="outerRadius">Outer radius.</param>
		/// <param name="innerColor">Inner color.</param>
		/// <param name="outerColor">Outer color.</param>
		public static void DrawDoubleBrushToolHandle(Vector3 worldPoint, Vector3 worldNormal, float innerRadius, float outerRadius, Color innerColor, Color outerColor)
		{
			#if UNITY_EDITOR
			HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
			Handles.color = innerColor;
			Handles.DrawSolidDisc(worldPoint, worldNormal, innerRadius);
			Handles.color = outerColor;;
			Handles.DrawSolidDisc(worldPoint, worldNormal, outerRadius);
			Color borderColor = new Color(outerColor.r, outerColor.g, outerColor.b, outerColor.a + 0.3f);
			Handles.color = borderColor;
			Handles.DrawWireDisc (worldPoint, worldNormal, outerRadius);
			HandleUtility.Repaint();
			#endif
		}
			
		/// <summary>
		/// Draws the direction tool handle.
		/// </summary>
		/// <param name="startPosition">Start position.</param>
		/// <param name="direction">Direction.</param>
		/// <param name="distance">Distance.</param>
		/// <param name="infoColor">Info color.</param>
		/// <param name="toolColor">Tool color.</param>
		public static void DrawDirectionToolHandle(Vector3 startPosition, Vector3 direction, float distance, Color infoColor, Color toolColor)
		{
			#if UNITY_EDITOR
			Vector3 endPosition = startPosition + (direction.normalized * distance);
			HandleUtility.AddDefaultControl (GUIUtility.GetControlID (FocusType.Passive));
			Handles.color = infoColor;
			Handles.DrawDottedLine (startPosition, endPosition, 2.0f);
			Handles.color = toolColor;
			Handles.SphereCap (0, startPosition, Quaternion.identity, 0.3f);
			Handles.ConeCap(0,endPosition,Quaternion.LookRotation(direction), 0.3f);
			#endif
		}

        /// <summary>
        /// Draws the Directional Handle Tool
        /// </summary>
        /// <param name="worldPoint">World Point</param>
        /// <param name="worldNormal">World Normal</param>
        /// <param name="currentDragPosition">Current Drag Position</param>
        /// <param name="handleColor">Handle Color</param>
        /// <param name="lineColor">Line Color</param>
		public static void DrawDirectionalToolHandle(Vector3 worldPoint, Vector3 worldNormal, Vector3 currentDragPosition, Color handleColor, Color lineColor)
		{
			#if UNITY_EDITOR
			HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
			float diskRadius = Vector3.Distance(worldPoint, currentDragPosition);
			Handles.color = handleColor;
			Handles.DrawSolidDisc(worldPoint, worldNormal, diskRadius);
			Handles.color = lineColor;
			Handles.DrawLine(worldPoint, currentDragPosition);
			Color borderColor = new Color(handleColor.r, handleColor.g, handleColor.b, handleColor.a + 0.3f);
			Handles.color = borderColor;
			Handles.DrawWireDisc (worldPoint, worldNormal, diskRadius);
			HandleUtility.Repaint();
			#endif
		}
		
        /// <summary>
        /// Hack: Draw a really small box to create a line effect
        /// </summary>
        public static void DrawHorizontalSeparator()
        {
			#if UNITY_EDITOR
            GUILayout.Space(10.0f);
            GUILayout.Box("", GUILayout.Height(1.0f), GUILayout.ExpandWidth(true));
            GUILayout.Space(10.0f);
			#endif
        }

        /// <summary>
        /// HACK: creates an invisible label, its dirty but for the life of me can not find a horizontal space
        /// </summary>
        public static void HorizontalPadding()
        {
			#if UNITY_EDITOR
            GUILayout.Label("");
			#endif
        }
    }
}
