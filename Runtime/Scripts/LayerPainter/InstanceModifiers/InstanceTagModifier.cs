﻿// <copyright file="InstanceTagModifier.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>07/01/2016</date>
// <summary>Instance Tag Modifier</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Runtime.Serialization;


namespace SceneLayers
{
    /// <summary>
    /// Instance Tag Modifier. Will set the tag of an instanced object.
    /// </summary>
	[System.Serializable]
	public class InstanceTagModifier : IModifier, ISerializable
	{
		private bool enabled = true;
		private string tag = string.Empty;

        /// <summary>
        /// Default Constructor
        /// </summary>
        public InstanceTagModifier() { }

        /// <summary>
        /// Serialization Constructor
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected InstanceTagModifier(SerializationInfo info, StreamingContext context)
        {
            this.enabled = (bool)info.GetValue("enabled", typeof(bool));
            this.tag = (string)info.GetValue("tag", typeof(string));
        }

        /// <summary>
        /// Gets or sets enebled
        /// </summary>
        public bool Enabled
        {
            get
            {
                return this.enabled;
            }
            set
            {
                this.enabled = value;
            }
        }

        /// <summary>
        /// Serialize
        /// </summary>
        /// <param name="info">Serialization Info</param>
        /// <param name="context">Streaming COntext</param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("enabled", enabled, typeof(bool));
            info.AddValue("tag", tag, typeof(string));
        }

        /// <summary>
        /// Modify the instance
        /// </summary>
        /// <param name="gameObject"></param>
        public void Modify(GameObject gameObject)
        {
			if (this.tag != string.Empty)
			{
				gameObject.tag = tag;
			}
		}

        /// <summary>
        /// OnDisable Callback
        /// </summary>
        public void OnDisable()
        {

        }

        /// <summary>
        /// OnDraw Gizmos Callback
        /// </summary>
        public void OnDrawGizmos()
        {

        }

        /// <summary>
        /// OnEnabled Callback
        /// </summary>
        public void OnEnable()
        {

        }

        /// <summary>
        /// One Inspector GUI callback
        /// </summary>
        public void OnInspectorGUI()
        {
			#if UNITY_EDITOR
			EditorGUILayout.BeginVertical();

			EditorGUILayout.LabelField("Layer Modifier:");

			this.enabled = EditorGUILayout.Toggle("Enabled:", this.enabled);

			EditorGUILayout.BeginHorizontal ();

			EditorGUILayout.LabelField ("Tag: ");
			this.tag = EditorGUILayout.TagField (this.tag);

			EditorGUILayout.EndHorizontal ();

			EditorGUILayout.EndVertical();
			#endif
		}

        /// <summary>
        /// On Scene GUI Callbback
        /// </summary>
		public void OnSceneGUI ()
		{
			
		}
	}
}
