﻿// <copyright file="InstanceRotationModifier.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>07/01/2016</date>
// <summary>Instance Rotation Modifier for Layer Painter</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Runtime.Serialization;

namespace SceneLayers
{

    /// <summary>
    /// Rotation Filter, values are clamped from 0 to 360 degrees
    /// </summary>
    [System.Serializable]
	public class InstanceRotationModifier : IModifier, ISerializable
    {
        private bool enabled = true;
        private Vector3 maximumRotation = new Vector3(0.0f, 360.0f, 0.0f);
        private Vector3 minimumRotation = Vector3.zero;

        /// <summary>
        /// Default Constructor
        /// </summary>
        public InstanceRotationModifier() { }

        /// <summary>
        /// Serialization Cosntructor
        /// </summary>
        /// <param name="info">Serialization Info</param>
        /// <param name="context">Streaming Context</param>
        protected InstanceRotationModifier(SerializationInfo info, StreamingContext context)
        {
            this.enabled = (bool)info.GetValue("enabled", typeof(bool));
            this.minimumRotation = (Vector3)info.GetValue("minimumRotation", typeof(Vector3));
            this.maximumRotation = (Vector3)info.GetValue("maximumRotation", typeof(Vector3));
        }

        /// <summary>
        /// Gets or sets Enabled
        /// </summary>
        public bool Enabled
        {
            get
            {
                return this.enabled;
            }

            set
            {
                this.enabled = value;
            }
        }

        /// <summary>
        /// Gets or sets the Maximum Rotation, clamped and greter than or equal to minimum rotation per axis.
        /// </summary>
        public Vector3 MaximumRotation
        {
            get
            {
                return this.maximumRotation;
            }

            set
            {
                float x = 0.0f;
                float y = 0.0f;
                float z = 0.0f;

                if (value.x >= 0.0f && value.x <= 360.0f)
                {
                    if (value.x >= this.minimumRotation.x)
                    {
                        x = value.x;
                    }
                    else
                    {
                        x = this.minimumRotation.x;
                    }
                }
                else if (value.x < 0.0f)
                {
                    x = 0.0f;
                }
                else if (value.x > 360.0f)
                {
                    x = 360.0f;
                }

                if (value.y >= 0.0f && value.y <= 360.0f)
                {
                    if (value.y >= this.minimumRotation.y)
                    {
                        y = value.y;
                    }
                    else
                    {
                        y = this.minimumRotation.y;
                    }
                }
                else if (value.y < 0.0f)
                {
                    y = 0.0f;
                }
                else if (value.y > 360.0f)
                {
                    y = 360.0f;
                }

                if (value.z >= 0.0f && value.z <= 360.0f)
                {
                    if (value.z >= this.minimumRotation.z)
                    {
                        z = value.z;
                    }
                    else
                    {
                        z = this.minimumRotation.z;
                    }
                }
                else if (value.z < 0.0f)
                {
                    z = 0.0f;
                }
                else if (value.z > 360.0f)
                {
                    z = 360.0f;
                }

                this.maximumRotation = new Vector3(x, y, z);
            }
        }

        /// <summary>
        /// Gets or sets the Minimum Rotation, clamped and less than or equal to maximum rotation per axis.
        /// </summary>
        public Vector3 MinimumRotation
        {
            get
            {
                return this.minimumRotation;
            }

            set
            {
                float x = 0.0f;
                float y = 0.0f;
                float z = 0.0f;

                if (value.x >= 0.0f && value.x <= 360.0f)
                {
                    if (value.x <= this.maximumRotation.x)
                    {
                        x = value.x;
                    }
                    else
                    {
                        x = this.maximumRotation.x;
                    }
                }
                else if (value.x < 0.0f)
                {
                    x = 0.0f;
                }
                else if (value.x > 360.0f)
                {
                    x = 360.0f;
                }

                if (value.y >= 0.0f && value.y <= 360.0f)
                {
                    if (value.y <= this.maximumRotation.y)
                    {
                        y = value.y;
                    }
                    else
                    {
                        y = this.maximumRotation.y;
                    }
                }
                else if (value.y < 0.0f)
                {
                    y = 0.0f;
                }
                else if (value.y > 360.0f)
                {
                    y = 360.0f;
                }

                if (value.z >= 0.0f && value.z <= 360.0f)
                {
                    if (value.z <= this.maximumRotation.z)
                    {
                        z = value.z;
                    }
                    else
                    {
                        z = this.maximumRotation.z;
                    }
                }
                else if (value.z < 0.0f)
                {
                    z = 0.0f;
                }
                else if (value.z > 360.0f)
                {
                    z = 360.0f;
                }

                this.minimumRotation = new Vector3(x, y, z);
            }
        }

        /// <summary>
        /// Serialize
        /// </summary>
        /// <param name="info">Serialization Info</param>
        /// <param name="context">Streaming Context</param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("enabled", enabled, typeof(bool));
            info.AddValue("minimumRotation", minimumRotation, typeof(Vector3));
            info.AddValue("maximumRotation", maximumRotation, typeof(Vector3));
        }

        /// <summary>
        /// Modify the passed gameobject
        /// </summary>
        /// <param name="gameObject">game object to modify</param>
		public void Modify(GameObject gameObject)
		{
			Vector3 rotation = new Vector3(Random.Range(minimumRotation.x, maximumRotation.x), Random.Range(minimumRotation.y, maximumRotation.y), Random.Range(minimumRotation.z, maximumRotation.z));
			gameObject.transform.Rotate(rotation, Space.Self);
		}

        /// <summary>
        /// On Disable Callback
        /// </summary>
		public void OnDisable()
        {

        }

        /// <summary>
        /// On Draw Gizmos Callback
        /// </summary>
		public void OnDrawGizmos()
        {

        }

        /// <summary>
        /// On Enable Callback
        /// </summary>
		public void OnEnable()
        {

        }

        /// <summary>
        /// On Inspector GUI Callback
        /// </summary>
		public void OnInspectorGUI()
		{
			#if UNITY_EDITOR
            EditorGUILayout.BeginVertical();

            EditorGUILayout.LabelField("Rotation Modifier:");

            this.enabled = EditorGUILayout.Toggle("Enabled:", this.enabled);
       
            this.MinimumRotation = EditorGUILayout.Vector3Field("Minimum Rotation:", this.MinimumRotation);
            this.MaximumRotation = EditorGUILayout.Vector3Field("Maximum Rotation:", this.MaximumRotation);

            EditorGUILayout.EndVertical();
			#endif
        }

        /// <summary>
        /// On Scene GUI Callback
        /// </summary>
		public void OnSceneGUI()
		{

		}
    }
}
