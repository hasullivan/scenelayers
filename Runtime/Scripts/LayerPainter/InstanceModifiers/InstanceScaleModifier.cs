﻿// <copyright file="InstanceScaleModifier.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>07/01/2016</date>
// <summary>Instance Scale Modifier for Layer Painter</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Runtime.Serialization;

namespace SceneLayers
{
    /// <summary>
    /// Modifies the scale of an instance in a uniform way.
    /// </summary>
    [System.Serializable]
	public class InstanceScaleModifier : IModifier, ISerializable
    { 
        private bool enabled = true;
        private float maximumScale = 2.0f;
        private float minimumScale = .5f;

        /// <summary>
        /// Default Constructor
        /// </summary>
        public InstanceScaleModifier() { }

        /// <summary>
        /// Serialization Constructor
        /// </summary>
        /// <param name="info">Serialization Info</param>
        /// <param name="context">Streaming Costext</param>
        protected InstanceScaleModifier(SerializationInfo info, StreamingContext context)
        {
            this.enabled = (bool)info.GetValue("enabled", typeof(bool));
            this.minimumScale = (float)info.GetValue("minimumScale", typeof(float));
            this.maximumScale = (float)info.GetValue("maximumScale", typeof(float));
        }

        /// <summary>
        /// Gets or sets Random Scale, is true random scale will be used
        /// </summary>
        public bool Enabled
        {
            get
            {
                return this.enabled;
            }

            set
            {
                this.enabled = value;
            }
        }

        /// <summary>
        /// Gets or sets the Maximum Scale
        /// </summary>
        public float MaximumScale
        {
            get
            {
                return this.maximumScale;
            }

            set
            {
                if (value > .0001f)
                {
                    if (value >= this.minimumScale)
                    {
                        this.maximumScale = value;
                    }
                    else
                    {
                        this.maximumScale = this.minimumScale;
                    }
                }
                else
                {
                    this.maximumScale = .0001f;
                }
            }
        }

        /// <summary>
        /// Gets or sets the Minimum Scale
        /// </summary>
        public float MinimumScale
        {
            get
            {
                return this.minimumScale;
            }

            set
            {
                if (value > .0001f)
                {
                    if (value <= this.maximumScale)
                    {
                        this.minimumScale = value;
                    }
                    else
                    {
                        this.minimumScale = this.maximumScale;
                    }
                }
                else
                {
                    this.minimumScale = .0001f;
                }
            }
        }

        /// <summary>
        /// Serialize
        /// </summary>
        /// <param name="info">Serialization Info</param>
        /// <param name="context">Streaming Context</param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("enabled", enabled, typeof(bool));
            info.AddValue("minimumScale", minimumScale, typeof(float));
            info.AddValue("maximumScale", maximumScale, typeof(float));
        }

        /// <summary>
        /// Applys the Modify settings the the provided Game Object
        /// </summary>
        /// <param name="gameObject">Game Object to Modify</param>
		public void Modify(GameObject gameObject)
		{
			float randomScale = Random.Range(minimumScale, maximumScale);
			Vector3 scale = new Vector3 (
				gameObject.transform.localScale.x * randomScale,
				gameObject.transform.localScale.y * randomScale,
				gameObject.transform.localScale.z * randomScale);
			gameObject.transform.localScale = scale;
		}

        /// <summary>
        /// On Disable Callback
        /// </summary>
        public void OnDisable()
        {

        }

        /// <summary>
        /// On Draw Gizmos Callback
        /// </summary>
        public void OnDrawGizmos()
        {

        }

        /// <summary>
        /// On Enable Callback
        /// </summary>
        public void OnEnable()
        {

        }

        /// <summary>
        /// On Inspector GUI Callback, for rendering in inspector
        /// </summary>
		public void OnInspectorGUI()
		{
			#if UNITY_EDITOR
            EditorGUILayout.BeginVertical();

            EditorGUILayout.LabelField("Scale Modifier:");

            this.enabled = EditorGUILayout.Toggle("Enabled:", this.enabled);
    
            this.MinimumScale = EditorGUILayout.DelayedFloatField("Minimum Scale:", this.MinimumScale);
            this.MaximumScale = EditorGUILayout.DelayedFloatField("Maximum Scale:", this.MaximumScale);

            EditorGUILayout.MinMaxSlider(ref minimumScale, ref maximumScale, 0.001f, 50.0f);

            EditorGUILayout.EndVertical();
			#endif
        }

        /// <summary>
        /// On Scene GUI callback
        /// </summary>
		public void OnSceneGUI()
		{

		}
    }
}
