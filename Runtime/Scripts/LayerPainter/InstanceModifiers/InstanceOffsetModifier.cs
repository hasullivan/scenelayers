﻿// <copyright file="InstanceOffsetModifier.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>07/01/2016</date>
// <summary>Instance Offset Modifier for Layer Painter</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Runtime.Serialization;

namespace SceneLayers
{
    /// <summary>
    /// Modifies the position offset off the instance
    /// </summary>
    [System.Serializable]
	public class InstanceOffsetModifier : IModifier, ISerializable
    {    
        private bool enabled = true;
        private Vector3 offset = Vector3.zero;

        /// <summary>
        /// Default Constructor
        /// </summary>
        public InstanceOffsetModifier() { }

        /// <summary>
        /// Serialization COnstructor
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected InstanceOffsetModifier(SerializationInfo info, StreamingContext context)
        {
            this.enabled = (bool)info.GetValue("enabled", typeof(bool));
            this.offset = (Vector3)info.GetValue("offset", typeof(Vector3));
        }

        /// <summary>
        /// Gets or sets enabled
        /// </summary>
        public bool Enabled
        {
            get
            {
                return this.enabled;
            }

            set
            {
                this.enabled = value;
            }
        }

        /// <summary>
        /// Gets or set the offset
        /// </summary>
        public Vector3 Offset
        {
            get
            {
                return this.offset;
            }

            set
            {
                this.offset = value;
            }
        }

        /// <summary>
        /// Serialize
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("enabled", enabled, typeof(bool));
            info.AddValue("offset", offset, typeof(Vector3));
        }

        /// <summary>
        /// Apply the offset to the provided game object
        /// </summary>
        /// <param name="gameObject"></param>
		public void Modify(GameObject gameObject)
		{
			gameObject.transform.position += offset;
		}

        /// <summary>
        /// On Disable Callback
        /// </summary>
        public void OnDisable() { }

        /// <summary>
        /// On Draw Gizmos Callback
        /// </summary>
        public void OnDrawGizmos() { }

        /// <summary>
        /// On Enable Callback
        /// </summary>
        public void OnEnable() { }

        /// <summary>
        /// On Inspector GUI Callback
        /// </summary>
		public void OnInspectorGUI()
		{
			#if UNITY_EDITOR
            EditorGUILayout.BeginVertical();

            EditorGUILayout.LabelField("Offset Modifier:");

            this.enabled = EditorGUILayout.Toggle("Enabled:", this.enabled);
            this.offset = EditorGUILayout.Vector3Field("Offset:", this.offset);
            
            EditorGUILayout.EndVertical();
			#endif
        }

        /// <summary>
        /// On Scene GUI
        /// </summary>
		public void OnSceneGUI() { }
    }
}
