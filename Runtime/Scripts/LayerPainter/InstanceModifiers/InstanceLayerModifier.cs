﻿// <copyright file="InstanceLayerModifier.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>04/11/2016  </date>
// <summary>Instance Layer Modifier</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Runtime.Serialization;

namespace SceneLayers
{
	/// <summary>
	/// Instance layer modifier. Sets the instance to the selected unity layer
	/// </summary>
	[System.Serializable]
	public class InstanceLayerModifier : IModifier, ISerializable
	{
		private bool enabled = true;
		private int layer = 0;

        /// <summary>
        /// Default Constructor
        /// </summary>
        public InstanceLayerModifier() { }

        /// <summary>
        /// Serialization constructor
        /// </summary>
        /// <param name="info">Info.</param>
        /// <param name="context">Context.</param>
        public InstanceLayerModifier(SerializationInfo info, StreamingContext context)
        {
            this.enabled = (bool)info.GetValue("enabled", typeof(bool));
            this.layer = (int)info.GetValue("layer", typeof(int));
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="SceneLayers.InstanceLayerModifier"/> is enabled.
        /// </summary>
        /// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
        public bool Enabled
        {
            get
            {
                return this.enabled;
            }
            set
            {
                this.enabled = value;
            }
        }

        /// <summary>
        /// Serialization
        /// </summary>
        /// <param name="info">Info.</param>
        /// <param name="context">Context.</param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("enabled", enabled, typeof(bool));
            info.AddValue("layer", layer, typeof(int));
        }

        /// <summary>
        /// Modify the specified gameObject.
        /// </summary>
        /// <param name="gameObject">Game object.</param>
        public void Modify(GameObject gameObject)
        {
            gameObject.layer = layer;
        }

        /// <summary>
        /// On Disable
        /// </summary>
        public void OnDisable()
        {
        }

        /// <summary>
        /// OnDrawGizmos
        /// </summary>
        public void OnDrawGizmos()
        {

        }

        /// <summary>
        /// On Enable
        /// </summary>
        public void OnEnable()
		{
		}
		/// <summary>
		/// Inspector GUI for this Modifier
		/// </summary>
		public void OnInspectorGUI ()
		{
			#if UNITY_EDITOR
			EditorGUILayout.BeginVertical();

			EditorGUILayout.LabelField("Layer Modifier:");

			this.enabled = EditorGUILayout.Toggle("Enabled:", this.enabled);

			EditorGUILayout.BeginHorizontal ();

			EditorGUILayout.LabelField ("Layer: ");
			layer = EditorGUILayout.LayerField (layer);

			EditorGUILayout.EndHorizontal ();


			EditorGUILayout.EndVertical();
			#endif
		}
			
		/// <summary>
		/// On Scene GUI
		/// </summary>
		public void OnSceneGUI ()
		{	#if UNITY_EDITOR
			
			#endif
		}
	}
}
