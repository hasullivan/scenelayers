﻿// <copyright file="InstanceFixedRotationModifier.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>07/01/2016</date>
// <summary>Instance Fixed Rotation Modifier for Layer Painter</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Runtime.Serialization;

namespace SceneLayers
{

	/// <summary>
	/// Rotation Filter, values are clamped from 0 to 360 degrees
	/// </summary>
	[System.Serializable]
	public class InstanceFixedRotationModifier : IModifier, ISerializable
	{
		private bool enabled = true;
		private Vector3 rotation = Vector3.zero;

		/// <summary>
		/// Default Constructor
		/// </summary>
		public InstanceFixedRotationModifier() { }

		/// <summary>
		/// Serialization Cosntructor
		/// </summary>
		/// <param name="info">Serialization Info</param>
		/// <param name="context">Streaming Context</param>
		protected InstanceFixedRotationModifier(SerializationInfo info, StreamingContext context)
		{
			this.enabled = (bool)info.GetValue("enabled", typeof(bool));
			this.rotation = (Vector3)info.GetValue("rotation", typeof(Vector3));
		}

		/// <summary>
		/// Gets or sets Enabled
		/// </summary>
		public bool Enabled
		{
			get
			{
				return this.enabled;
			}

			set
			{
				this.enabled = value;
			}
		}

		/// <summary>
		/// Gets or sets the Maximum Rotation, clamped and greter than or equal to minimum rotation per axis.
		/// </summary>
		public Vector3 Rotation
		{
			get
			{
				return this.rotation;
			}

			set
			{
				float x = 0.0f;
				float y = 0.0f;
				float z = 0.0f;

				if (value.x >= 0.0f && value.x <= 360.0f)
				{
					x = this.rotation.x;
				}
				else if (value.x < 0.0f)
				{
					x = 0.0f;
				}
				else if (value.x > 360.0f)
				{
					x = 360.0f;
				}

				if (value.y >= 0.0f && value.y <= 360.0f)
				{
					y = this.rotation.y;
				}
				else if (value.y < 0.0f)
				{
					y = 0.0f;
				}
				else if (value.y > 360.0f)
				{
					y = 360.0f;
				}

				if (value.z >= 0.0f && value.z <= 360.0f)
				{
					z = this.rotation.z;
				}
				else if (value.z < 0.0f)
				{
					z = 0.0f;
				}
				else if (value.z > 360.0f)
				{
					z = 360.0f;
				}

				this.rotation = new Vector3(x, y, z);
			}
		}



		/// <summary>
		/// Serialize
		/// </summary>
		/// <param name="info">Serialization Info</param>
		/// <param name="context">Streaming Context</param>
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("enabled", enabled, typeof(bool));
			info.AddValue("minimumRotation", rotation, typeof(Vector3));
		}

		/// <summary>
		/// Modify the passed gameobject
		/// </summary>
		/// <param name="gameObject">game object to modify</param>
		public void Modify(GameObject gameObject)
		{
			gameObject.transform.Rotate(this.rotation, Space.Self);
		}

		/// <summary>
		/// On Disable Callback
		/// </summary>
		public void OnDisable()
		{

		}

		/// <summary>
		/// On Draw Gizmos Callback
		/// </summary>
		public void OnDrawGizmos()
		{

		}

		/// <summary>
		/// On Enable Callback
		/// </summary>
		public void OnEnable()
		{

		}

		/// <summary>
		/// On Inspector GUI Callback
		/// </summary>
		public void OnInspectorGUI()
		{
			#if UNITY_EDITOR
			EditorGUILayout.BeginVertical();

			EditorGUILayout.LabelField("Fixed Rotation Modifier:");

			this.enabled = EditorGUILayout.Toggle("Enabled:", this.enabled);

			this.rotation = EditorGUILayout.Vector3Field("Rotation:", this.Rotation);

			EditorGUILayout.EndVertical();
			#endif
		}

		/// <summary>
		/// On Scene GUI Callback
		/// </summary>
		public void OnSceneGUI()
		{

		}
	}
}


