﻿// <copyright file="InstanceRules.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>07/01/2016</date>
// <summary>Instance Rules for Layer Painter</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SceneLayers {

    /// <summary>
    /// 
    /// </summary>
    [System.Serializable]
    [KnownType(typeof(InstanceModifierStack))]
	public class InstanceRules : IInstanceRule, ISerializable
    {
       
        private bool enabled = false;

      
        private InstanceModifierStack instanceModifierStack = new InstanceModifierStack();

      
        private bool showModifierSelection = false;

        /// <summary>
        /// 
        /// </summary>
        public InstanceRules()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        public bool Enabled
        {
            get
            {
                return this.enabled;
            }

            set
            {
                this.enabled = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public InstanceModifierStack InstanceModifierStack
        {
            get
            {
                return this.instanceModifierStack;
            }

            set
            {
                this.instanceModifierStack = value;
            }
        }

		/// <summary>
		/// On Enable Callback
		/// </summary>
		public void OnEnable()
		{
			this.instanceModifierStack.OnEnable ();
		}

		/// <summary>
		/// On Disable Callback
		/// </summary>
		public void OnDisable()
		{
			this.instanceModifierStack.OnDisable ();
		}


        /// <summary>
        /// On Inspector GUI Callback
        /// </summary>
        public void OnInspectorGUI()
        {
			#if UNITY_EDITOR
            EditorGUILayout.LabelField("Modifier Rules:");
            EditorGUILayout.BeginVertical();

            this.instanceModifierStack.OnInspectorGUI();


            EditorGUILayout.EndVertical();


            if (GUILayout.Button("+", GUILayout.Width(20.0f)) == true)
            {
                if (showModifierSelection == false)
                {
                    this.showModifierSelection = true;
                }
                else
                {
                    this.showModifierSelection = false;
                }
            }

            EditorGUILayout.Separator();

            if (showModifierSelection == true)
            {
                this.DrawModifierSelection();
            }
			#endif
        }

		/// <summary>
		/// On Scene GUI Callback
		/// </summary>
		public void OnSceneGUI()
		{
			this.instanceModifierStack.OnSceneGUI ();
		}

		public void OnDrawGizmos()
		{
			this.instanceModifierStack.OnDrawGizmos ();
		}

        /// <summary>
        /// Draw Modifier Selection in the inspector
		/// TODO Come up with better way before this gets too long
        /// </summary>
        private void DrawModifierSelection()
        {
			#if UNITY_EDITOR
            if (GUILayout.Button("Offset Modifier") == true)
            {
                this.instanceModifierStack.InstanceModifiers.Add(new InstanceOffsetModifier());
                this.showModifierSelection = false;
            }

            if (GUILayout.Button("Rotation Modifier") == true)
            {
                this.instanceModifierStack.InstanceModifiers.Add(new InstanceRotationModifier());
                this.showModifierSelection = false;
            }

            if (GUILayout.Button("Scale Modifier") == true)
            {
                this.instanceModifierStack.InstanceModifiers.Add(new InstanceScaleModifier());
                this.showModifierSelection = false;
            }

			if (GUILayout.Button ("Unity Layer Modifier") == true)
			{
				this.instanceModifierStack.InstanceModifiers.Add (new InstanceLayerModifier ());
				this.showModifierSelection = false;
			}

			if (GUILayout.Button ("Tag Modifier") == true)
			{
				this.instanceModifierStack.InstanceModifiers.Add (new InstanceTagModifier ());
				this.showModifierSelection = false;
			}

			//if (GUILayout.Button ("Parent Modifier") == true)
			//{
			//	this.instanceModifierStack.InstanceModifiers.Add (new InstanceParentModifier ());
			//	this.showModifierSelection = false;
			//}

			if (GUILayout.Button ("Fixed Rotation Modifier") == true)
			{
				this.instanceModifierStack.InstanceModifiers.Add (new InstanceFixedRotationModifier ());
				this.showModifierSelection = false;
			}
			#endif
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        public void ApplyModifiers(List<GameObject> list)
        {
			for (int i = 0; i < list.Count; i++)
			{
				this.ApplyModifiers (list [i]);
			}
        }

		public void ApplyModifiers (GameObject gameObject)
		{
			this.instanceModifierStack.ApplyModifiers (gameObject);
		}
			
		public void GetObjectData (SerializationInfo info, StreamingContext context)
		{
			info.AddValue ("enabled", enabled, typeof(bool));
			info.AddValue ("instanceModifierStack", instanceModifierStack, typeof(InstanceModifierStack));

		}

		protected InstanceRules(SerializationInfo info, StreamingContext context)
		{
			this.enabled = (bool)info.GetValue ("enabled", typeof(bool));
			this.instanceModifierStack = (InstanceModifierStack)info.GetValue ("instanceModifierStack", typeof(InstanceModifierStack));

		}
			
    }
}
