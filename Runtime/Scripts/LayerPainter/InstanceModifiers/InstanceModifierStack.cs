﻿// <copyright file="InstanceModifierStack.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>04/11/2016  </date>
// <summary>Instance Modifier Stack for Layer Painter</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


using UnityEngine;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SceneLayers
{
    /// <summary>
    /// Instance Modifier Stack stores and draws the list of instance modifiers to the GUI
    /// </summary>
	[System.Serializable]
    [KnownType(typeof(InstanceScaleModifier))]
    [KnownType(typeof(InstanceOffsetModifier))]
    [KnownType(typeof(InstanceRotationModifier))]
	[KnownType(typeof(InstanceLayerModifier))]
	[KnownType(typeof(InstanceTagModifier))]
	[KnownType(typeof(InstanceFixedRotationModifier))]
	public class InstanceModifierStack : ISerializable
	{     
        private List<IModifier> instanceModifiers = new List<IModifier>();

        /// <summary>
        /// Default Constructor
        /// </summary>
        public InstanceModifierStack() { }

        /// <summary>
        /// Serialization Constructor
        /// </summary>
        /// <param name="info">Serialization Info</param>
        /// <param name="context">Streaming Context</param>
        protected InstanceModifierStack(SerializationInfo info, StreamingContext context)
        {
            this.instanceModifiers = (List<IModifier>)info.GetValue("instanceModifiers", typeof(List<IModifier>));
        }

        /// <summary>
        /// Gets or sets the Instance Modifiers
        /// </summary>
		public List<IModifier> InstanceModifiers
		{
			get
			{
				return this.instanceModifiers;
			}
			set
			{
				this.instanceModifiers = value;
			}

		}

        /// <summary>
        /// Apply the modifier stack to the game obejct
        /// </summary>
        /// <param name="gameObject"></param>
        public void ApplyModifiers(GameObject gameObject)
        {
            for(int i = 0; i < this.instanceModifiers.Count; i++)
            {
                if(this.instanceModifiers[i].Enabled == true)
                {
                    this.instanceModifiers[i].Modify(gameObject);
                }
            }
        }

        /// <summary>
        /// Serialize
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("instanceModifiers", instanceModifiers, typeof(List<IModifier>));
        }

        /// <summary>
        /// On Disable Callback
        /// </summary>
        public void OnDisable()
        {
            for (int i = 0; i < instanceModifiers.Count; i++)
            {
                this.instanceModifiers[i].OnDisable();
            }
        }

        /// <summary>
        /// On Draw Gizmos Callback
        /// </summary>
        public void OnDrawGizmos()
        {
            for (int i = 0; i < this.instanceModifiers.Count; i++)
            {
                this.instanceModifiers[i].OnDrawGizmos();
            }
        }

        /// <summary>
        /// On Enable Callback
        /// </summary>
        public void OnEnable()
        {
			for (int i = 0; i < instanceModifiers.Count; i++)
			{
				this.instanceModifiers [i].OnEnable ();
			}
		}

        /// <summary>
        /// On Inspector GUI Callback
        /// </summary>
        public void OnInspectorGUI()
		{
			#if UNITY_EDITOR
            for (int i = 0; i < instanceModifiers.Count; i++)
            {
                LayerPainterGUI.DrawHorizontalSeparator();

                GUILayout.BeginHorizontal();

                    GUILayout.BeginVertical();

                    if (GUILayout.Button("-", GUILayout.Width(20), GUILayout.Height(20)) == true)
                    {
                        instanceModifiers[i] = null;
                    }

                    GUILayout.EndVertical();
                
                    if (instanceModifiers[i] != null)
                    {
                        instanceModifiers[i].OnInspectorGUI();
                    }

                GUILayout.EndHorizontal();

                LayerPainterGUI.DrawHorizontalSeparator();
            }

            instanceModifiers.RemoveAll(item => item == null);
			#endif
        }

        /// <summary>
        /// On Screne GUI Callback
        /// </summary>
		public void OnSceneGUI()
		{
			#if UNITY_EDITOR
			for (int i = 0; i < instanceModifiers.Count; i++)
			{
				this.instanceModifiers [i].OnSceneGUI ();
			}
			#endif
		}
	}
}

