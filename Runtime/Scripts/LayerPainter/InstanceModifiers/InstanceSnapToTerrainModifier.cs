﻿// <copyright file="InstanceSnapToTerrainModifier.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>07/01/2016</date>
// <summary>Instance Snap To Terrain Modifier for Layer Painter</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Runtime.Serialization;

namespace SceneLayers
{
    /// <summary>
    /// Instance Snap To Terrain Modifier. Will snap the instance to a terrain object below it.
    /// </summary>
    [System.Serializable]
    public class InstanceSnapToTerrainModifier : IModifier, ISerializable
	{    
		private bool enabled = false;  

        /// <summary>
        /// Default Constructor
        /// </summary>
		public InstanceSnapToTerrainModifier(){}

        /// <summary>
        /// Serialization Cosntructor
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected InstanceSnapToTerrainModifier(SerializationInfo info, StreamingContext context)
        {
            this.enabled = (bool)info.GetValue("enabled", typeof(bool));
        }

        /// <summary>
        /// Gets or sets enabled
        /// </summary>
        public bool Enabled
        {
			get
			{
				return this.enabled;
			}
			set
			{
				this.enabled = value;
			}
		}

        /// <summary>
        /// Serialize
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("enabled", enabled, typeof(bool));
        }

        /// <summary>
        /// Apply the modifier to the gameObject
        /// </summary>
        /// <param name="gameObjject"></param>
        public void Modify(GameObject gameObjject)
        {

        }

        /// <summary>
        /// OnDisable Callback
        /// </summary>
        public void OnDisable()
        {

        }

        /// <summary>
        /// On Draw Gizmos Callback
        /// </summary>
        public void OnDrawGizmos()
        {

        }

        /// <summary>
        /// On Enable Callback
        /// </summary>
        public void OnEnable()
        {

        }

        /// <summary>
        /// On Inspector GUI Callback
        /// </summary>
        public void OnInspectorGUI()
        {
		}

        /// <summary>
        /// On Scene GUI Callback
        /// </summary>
		public void OnSceneGUI()
		{

		}
	}
}
