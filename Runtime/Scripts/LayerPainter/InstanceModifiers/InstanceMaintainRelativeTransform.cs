﻿// <copyright file="InstanceMaintainRelativeTransform.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>04/11/2016  </date>
// <summary>Instance Maintain Relative Transform for Layer Painter</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace SceneLayers
{
    /// <summary>
    /// Maintains the Relative position, rotation, and scale to the transform of the sample that this object was instanced at.
    /// </summary>
    [System.Serializable]
	public class InstanceMaintainRelativeTransform : IModifier, ISerializable
    { 
        private bool enabled = true;
        private Transform relativeTransform;

        /// <summary>
        /// Default Constructor
        /// </summary>
        public InstanceMaintainRelativeTransform() { }

        /// <summary>
        /// Serialization Constructor
        /// </summary>
        /// <param name="info">Srialization Info</param>
        /// <param name="context">Streaming Context</param>
        public InstanceMaintainRelativeTransform(SerializationInfo info, StreamingContext context)
        {

        }

        /// <summary>
        /// Gets or sets enabled
        /// </summary>
        public bool Enabled
        {
            get
            {
                return enabled;
            }

            set
            {
                enabled = value;
            }
        }

        /// <summary>
        /// Serialize
        /// </summary>
        /// <param name="info">Serialization info</param>
        /// <param name="context">Streaming Context</param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {

        }

        /// <summary>
        /// Modify the provided instance
        /// </summary>
        /// <param name="instance"></param>
        public void Modify(GameObject gameObejct)
        {


        }

        /// <summary>
        /// On Disable Callback
        /// </summary>
        public void OnDisable()
        {

        }

        /// <summary>
        /// On Draw Gizmos Callback
        /// </summary>
        public void OnDrawGizmos()
        {

        }

        /// <summary>
        /// On Enable Callback
        /// </summary>
        public void OnEnable()
        {

        }

        /// <summary>
        /// On Inspector GUI Callback
        /// </summary>
        public void OnInspectorGUI()
        {
			#if UNITY_EDITOR
			#endif
        }

        /// <summary>
        /// On Scene GUI Callback
        /// </summary>
		public void OnSceneGUI()
		{
			#if UNITY_EDITOR
			#endif
		}
    }
}
