﻿// <copyright file="InstanceObject.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>07/01/2016</date>
// <summary>Instance Object for Layer Painter</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Runtime.Serialization;


namespace SceneLayers
{
    /// <summary>
    /// Instance object
    /// </summary>
    [System.Serializable]
    [KnownType(typeof(SampleRules))]
    [KnownType(typeof(InstanceRules))]
	public class InstanceObject : ISerializable
	{
        private bool enabled = true;
		private GameObject gameObject;
		private string gameObjectURL;
        private InstanceRules instanceRules = new InstanceRules();
        private GameObject objectFilterTemp;
        private float placementFrequency = 1.0f;
        private SampleRules samplingRules = new SampleRules();
        private bool showFilterSelection = false;
        private bool showFiltersFoldout = false;
        private bool showModifiersFoldout = false;
		#if UNITY_EDITOR
        private Texture2D thumbnail;
        private string thumbnailURL;
		#endif

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="gameObject">Game Object to instance</param>
		public InstanceObject(GameObject gameObject)
		{
			this.enabled = true;
			this.GameObject = gameObject;
			#if UNITY_EDITOR
			thumbnail = LayerPainterUtility.GetThumbnail (gameObject);

			this.gameObjectURL = AssetDatabase.GetAssetPath (gameObject.GetInstanceID ());

			if (thumbnail == null)
			{
				thumbnail = (Texture2D)AssetDatabase.LoadAssetAtPath(@"Assets/SceneLayers/Editor/InternalResources/NoPreviewAvailable.tif", typeof(Texture2D));
			}

			thumbnailURL = AssetDatabase.GetAssetPath (thumbnail.GetInstanceID ());
			#endif
		}

        /// <summary>
        /// Serialization COnstructor
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected InstanceObject(SerializationInfo info, StreamingContext context)
        {
            this.enabled = (bool)info.GetValue("enabled", typeof(bool));
            this.gameObjectURL = (string)info.GetValue("gameObjectURL", typeof(string));
			#if UNITY_EDITOR
            if (this.gameObjectURL != null)
            {
                this.GameObject = (GameObject)AssetDatabase.LoadAssetAtPath(this.gameObjectURL, typeof(GameObject));
            }

            this.thumbnailURL = (string)info.GetValue("thumbnailURL", typeof(string));

            if (this.thumbnailURL != null)
            {
                this.thumbnail = (Texture2D)AssetDatabase.LoadAssetAtPath(this.thumbnailURL, typeof(Texture2D));
            }
			#endif
            this.placementFrequency = (float)info.GetValue("placementFrequency", typeof(float));
            this.samplingRules = (SampleRules)info.GetValue("samplingRules", typeof(SampleRules));
            this.instanceRules = (InstanceRules)info.GetValue("instanceRules", typeof(InstanceRules));
        }

        /// <summary>
        /// Gets or sets enabled
        /// </summary>
        public bool Enabled
        {
            get
            {
                return enabled;
            }
            set
            {
                enabled = value;
            }
        }

        /// <summary>
        /// Gets the game object
        /// </summary>
        public GameObject GameObject
        {
            get
            {
                return this.gameObject;
            }

            set
            {
                this.gameObject = value;
            }
        }

        /// <summary>
        /// Gets the instance rules.
        /// </summary>
        /// <value>The instance rules.</value>
        public InstanceRules InstanceRules
        {
            get
            {
                return this.instanceRules;
            }
        }

        /// <summary>
        /// Gets or sets the placement frequency
        /// </summary>
        public float PlacementFrequency
        {
            get
            {
                return this.placementFrequency;
            }
            set
            {
                if (value <= 0.0f)
                {
                    placementFrequency = .001f;
                }
                else
                {
                    placementFrequency = value;
                }
            }
        }

        /// <summary>
        /// Gets the sampling rules.
        /// </summary>
        /// <value>The sampling rules.</value>
        public SampleRules SamplingRules
        {
            get
            {
                return this.samplingRules;
            }
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("enabled", enabled, typeof(bool));
            info.AddValue("gameObjectURL", gameObjectURL, typeof(string));
			#if UNITY_EDITOR
            info.AddValue("thumbnailURL", thumbnailURL, typeof(string));
			#endif
            info.AddValue("placementFrequency", placementFrequency, typeof(float));
            info.AddValue("samplingRules", samplingRules, typeof(SampleRules));
            info.AddValue("instanceRules", instanceRules, typeof(InstanceRules));
        }

        /// <summary>
        /// On Disable Callback
        /// </summary>
        public void OnDisable() { }

        /// <summary>
        /// On Draw Gizmos Callback
        /// </summary>
        public void OnDrawGizmos() { }

        /// <summary>
        /// On Enable Callback
        /// </summary>
        public void OnEnable() { }

        /// <summary>
        /// On Inspector GUI Callback
        /// </summary>
		public void OnInspectorGUI()
		{
			#if UNITY_EDITOR
            GUILayout.Label(this.GameObject.name, EditorStyles.boldLabel);

            GUILayout.BeginHorizontal (GUILayout.ExpandWidth (true));

            GUILayout.BeginVertical();

			this.DrawInstance ();

            GUILayout.EndVertical();

            GUILayout.BeginVertical();
 
            this.enabled = EditorGUILayout.Toggle("Enabled:", this.enabled);

            EditorGUILayout.LabelField("Placement Frequency:");
            this.placementFrequency = EditorGUILayout.Slider("", this.placementFrequency, 0.0f, 100.0f);

            GUILayout.EndVertical();

			GUILayout.EndHorizontal ();

            GUILayout.BeginVertical();

            this.DrawOptions();

            GUILayout.EndVertical();
			#endif
        }

        /// <summary>
        /// On Scene GUI Callback
        /// </summary>
        public void OnSceneGUI() { }


        /// <summary>
        /// Draw the Instance Thumbnail
        /// </summary>
        private void DrawInstance()
		{
			#if UNITY_EDITOR
            GUILayout.BeginVertical();

			if (this.thumbnail != null)
			{
				
				GUILayout.Box(this.thumbnail, GUILayout.Width(110), GUILayout.Height(110));
			}
			else
			{
				GUILayout.Box(this.GameObject.name, GUILayout.Width(128), GUILayout.Height(128));
			}
				
			GUILayout.EndVertical();
			#endif
		}

        /// <summary>
        /// Draw the Instance Object Options
        /// </summary>
	    private void DrawOptions()
	    {
			#if UNITY_EDITOR
            GUILayout.Space(20.0f);
            GUILayout.BeginVertical();
            

	    	this.showFiltersFoldout = EditorGUILayout.Foldout (this.showFiltersFoldout, "Filters", EditorStyles.foldout);
        
	    	if (this.showFiltersFoldout == true)
	    	{
                this.samplingRules.OnInspectorGUI();
            }
            
	    	this.showModifiersFoldout = EditorGUILayout.Foldout (this.showModifiersFoldout, "Modifiers", EditorStyles.foldout);
        
	    	if (this.showModifiersFoldout == true)
	    	{
                this.instanceRules.OnInspectorGUI();
	    	}
        
	    	GUILayout.EndVertical ();
			#endif
	    }	
    }
}
