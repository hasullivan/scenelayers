﻿// <copyright file="ObjectFilterStack.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>07/01/2016</date>
// <summary>Brush Interface for Layer Painter</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

namespace SceneLayers
{
    /// <summary>
    /// Object fitler stack stores object fitlers and performs GUI funtionality
    /// </summary>
	[System.Serializable]
	[KnownType(typeof(ObjectGameObjectFilter))]
	public class ObjectFilterStack : ISerializable
	{
		List<IObjectFilter> objectFilters = new List<IObjectFilter>();

        /// <summary>
        /// Default Constructor
        /// </summary>
		public ObjectFilterStack() { }

        /// <summary>
        /// Serialization Constructor
        /// </summary>
        /// <param name="info">Serializtion Info</param>
        /// <param name="context">Streaming Context</param>
        protected ObjectFilterStack(SerializationInfo info, StreamingContext context)
        {
            this.objectFilters = (List<IObjectFilter>)info.GetValue("objectFilters", typeof(List<IObjectFilter>));
        }

        /// <summary>
        /// Get or set Object Fitlers
        /// </summary>
        public List<IObjectFilter> ObjectFilters
        {
			get
			{
				return this.objectFilters;
			}
			set
			{
				this.objectFilters = value;
			}
		}

        /// <summary>
        /// Apply Filters to provided Object
        /// </summary>
        /// <param name="gameObject">Game Object</param>
        /// <returns>Bool</returns>
        public bool ApplyFilter(GameObject gameObject)
        {
            bool result = true;

            for(int i = 0; i < this.objectFilters.Count; i++)
            {
                if (objectFilters[i].Enabled == true)
                {
                    result = objectFilters[i].ApplyFilter(gameObject);
                    if (result == false)
                    {
                        return false;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Serialize
        /// </summary>
        /// <param name="info">Serialization Info</param>
        /// <param name="context">Streaming Context</param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("objectFilters", objectFilters, typeof(List<IObjectFilter>));
        }

        /// <summary>
        /// On Disable Callback
        /// </summary>
        public void OnDisable()
        {
            for (int i = 0; i < this.objectFilters.Count; i++)
            {
                this.objectFilters[i].OnDisable();
            }
        }

        /// <summary>
        /// On Draw Gizmo Callback
        /// </summary>
        public void OnDrawGizmos()
        {
            for (int i = 0; i < this.objectFilters.Count; i++)
            {
                this.objectFilters[i].OnDrawGizmos();
            }
        }

        /// <summary>
        /// On Enabled Callback
        /// </summary>
        public void OnEnable()
        {
			for (int i = 0; i < this.objectFilters.Count; i++)
			{
				this.objectFilters [i].OnEnable ();
			}
		}

        /// <summary>
        /// On Inspector GUI Callback
        /// </summary>
		public void OnInspectorGUI()
		{
			#if UNITY_EDITOR
			for (int i = 0; i < objectFilters.Count; i++)
			{
				LayerPainterGUI.DrawHorizontalSeparator();

				GUILayout.BeginHorizontal();
				GUILayout.BeginVertical();

				if (GUILayout.Button("-", GUILayout.Width(20), GUILayout.Height(20)) == true)
				{
					objectFilters[i] = null;
				}

				GUILayout.EndVertical();
				LayerPainterGUI.HorizontalPadding();

				if (objectFilters[i] != null)
				{
					objectFilters[i].OnInspectorGUI();
				}


				GUILayout.EndHorizontal();

				LayerPainterGUI.DrawHorizontalSeparator();

			}

			objectFilters.RemoveAll (item => item == null);
			#endif
		}

        /// <summary>
        /// On Scene GUI
        /// </summary>
		public void OnSceneGUI()
		{
			#if UNITY_EDITOR
			for (int i = 0; i < this.objectFilters.Count; i++)
			{
				this.objectFilters [i].OnSceneGUI ();
			}
			#endif
		}
	}
}

