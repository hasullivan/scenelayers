﻿// <copyright file="ObjectRules.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>07/01/2016</date>
// <summary>Brush Interface for Layer Painter</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


using System.Runtime.Serialization;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace SceneLayers
{
    /// <summary>
    /// 
    /// </summary>
	[System.Serializable]
	[KnownType(typeof(ObjectFilterStack))]
	public class ObjectRules : ISerializable
	{
		private bool enabled = false;
		private ObjectFilterStack objectFilterStack = new ObjectFilterStack();
        private GameObject objectFilterTemp;
        private bool showObjectSelection = false;

        /// <summary>
        /// Default Constructor
        /// </summary>
        public ObjectRules() { }

        /// <summary>
        /// Serialization Constructor
        /// </summary>
        /// <param name="info">Serialization Info</param>
        /// <param name="context">Streaming COntext</param>
        protected ObjectRules(SerializationInfo info, StreamingContext context)
        {
            this.enabled = (bool)info.GetValue("enabled", typeof(bool));
            this.objectFilterStack = (ObjectFilterStack)info.GetValue("objectFilterStack", typeof(ObjectFilterStack));
        }

        /// <summary>
        /// Gets or set enabled
        /// </summary>
        public bool Enabled
        {
            get
            {
                return this.enabled;
            }
            set
            {
                this.enabled = value;
            }
        }

        /// <summary>
        /// Get or set Object Filter Stack
        /// </summary>
        public ObjectFilterStack ObjectFilterStack
        {
			get
			{
				return this.objectFilterStack;
			}
			set 
			{
				this.objectFilterStack = value;
			}
		}

        /// <summary>
        /// Apply Filters int the sack to the provided gameObject
        /// </summary>
        /// <param name="gameObject"></param>
        /// <returns></returns>
        public bool ApplyFilters(GameObject gameObject)
        {
            return this.objectFilterStack.ApplyFilter(gameObject);
        }

        /// <summary>
        /// Serialize
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("enabled", enabled, typeof(bool));
            info.AddValue("objectFilterStack", objectFilterStack, typeof(ObjectFilterStack));
        }

        /// <summary>
        /// On Disable Callback
        /// </summary>
        public void OnDisable()
        {
            this.objectFilterStack.OnDisable();
        }

        /// <summary>
        /// On Draw Gizmos Callback
        /// </summary>
        public void OnDrawGizmos()
        {
            this.objectFilterStack.OnDrawGizmos();
        }

        /// <summary>
        /// On Enable Callback
        /// </summary>
        public void OnEnable()
        {
			this.objectFilterStack.OnEnable ();
		}

		/// <summary>
		/// On Inspector GUI Callback
		/// </summary>
		public void OnInspectorGUI()
		{
			#if UNITY_EDITOR
			EditorGUILayout.LabelField("Object Rules:");
			EditorGUILayout.BeginVertical();

			this.objectFilterStack.OnInspectorGUI();


			EditorGUILayout.EndVertical();


			if (GUILayout.Button("+", GUILayout.Width(20.0f)) == true)
			{
				if (showObjectSelection == false)
				{
					this.showObjectSelection = true;
				}
				else
				{
					this.showObjectSelection = false;
				}
			}

			EditorGUILayout.Separator();

			if(showObjectSelection == true)
			{
				this.DrawFilterSelection();
			}
			#endif
		}

        /// <summary>
        /// On Scene GUI Callback
        /// </summary>
		public void OnSceneGUI()
		{
			#if UNITY_EDITOR
			this.objectFilterStack.OnSceneGUI ();
			#endif
		}

		/// <summary>
		/// Draw the Filter selection List to the GUI
		/// </summary>
		private void DrawFilterSelection()
		{
			#if UNITY_EDITOR
			EditorGUILayout.BeginHorizontal();

			this.objectFilterTemp = (GameObject)EditorGUILayout.ObjectField("Object to add", this.objectFilterTemp, typeof(GameObject), true, GUILayout.ExpandWidth(true));

			if (GUILayout.Button("Add Object Fitler"))
			{
				if(this.objectFilterTemp != null)
				{
					this.objectFilterStack.ObjectFilters.Add(new ObjectGameObjectFilter(this.objectFilterTemp));
					this.showObjectSelection = false;
				}
				this.objectFilterTemp = null;
			}

			EditorGUILayout.EndHorizontal();
			#endif
		}
	}
}

