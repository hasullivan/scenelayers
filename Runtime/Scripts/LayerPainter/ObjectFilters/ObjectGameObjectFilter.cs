﻿// <copyright file="ObjectGameObjectFilter.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>07/01/2016</date>
// <summary>Brush Interface for Layer Painter</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SceneLayers
{
    /// <summary>
    /// Filter For Game Objects
    /// </summary>
	[System.Serializable]
	public class ObjectGameObjectFilter : IObjectFilter, ISerializable
	{
		private bool enabled = true;
        private string filterObjectTransformName;
        private InclusionMode inclusionMode = InclusionMode.Excluded;
		#if UNITY_EDITOR
        private Texture2D thumbnail;
		private string thumbnailURL = string.Empty;
		#endif

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="filterObject">Game Obejct to filter</param>
		public ObjectGameObjectFilter(GameObject filterObject)
		{
			this.filterObjectTransformName = filterObject.transform.name;

			#if UNITY_EDITOR
			thumbnail = LayerPainterUtility.GetThumbnail(filterObject);

			if (thumbnail == null)
			{
				thumbnail = (Texture2D)AssetDatabase.LoadAssetAtPath (@"Assets/SceneLayers/Editor/InternalResources/NoPreviewAvailable.tif", typeof(Texture2D));
			} 

			thumbnailURL = AssetDatabase.GetAssetPath (thumbnail.GetInstanceID ());
			#endif
		}

        /// <summary>
        /// Serializzation Constructor
        /// </summary>
        /// <param name="info">Serialization Info</param>
        /// <param name="context">Streaming Context</param>
        protected ObjectGameObjectFilter(SerializationInfo info, StreamingContext context)
        {
            this.enabled = (bool)info.GetValue("enabled", typeof(bool));
            this.inclusionMode = (InclusionMode)info.GetValue("inclusionMode", typeof(InclusionMode));
			this.filterObjectTransformName = (string)info.GetValue("filterObjectTransformName", typeof(string));
			#if UNITY_EDITOR
            this.thumbnailURL = (string)info.GetValue("thumbnailURL", typeof(string));
            this.thumbnail = (Texture2D)AssetDatabase.LoadAssetAtPath(thumbnailURL, typeof(Texture2D));
			#endif
            
        }

        /// <summary>
        /// Gets or sets enabled
        /// </summary>
        public bool Enabled
        {
            get
            {
                return this.enabled;
            }
            set
            {
                this.enabled = value;
            }
        }

        /// <summary>
        /// Apply the filter to a list
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public List<GameObject> ApplyFilter(List<GameObject> list)
        {
            List<GameObject> filteredList = new List<GameObject>();

            for(int i = 0; i < list.Count; i++)
            {
                if(this.ApplyFilter(list[i]) == true)
                {
                    filteredList.Add(list[i]);
                }

            }

            return filteredList;
        }

        /// <summary>
        /// Apply Filter to provided Game Object
        /// </summary>
        /// <param name="gameObject">Game Object to filter</param>
        /// <returns>Bool</returns>
        public bool ApplyFilter(GameObject gameObject)
        {
            List<Transform> transforms = LayerPainterUtility.GetCompoundObjectsTransforms(gameObject.transform);

            switch (this.inclusionMode)
            {
                case InclusionMode.Permitted:

                    if (gameObject.transform == null)
                    {
                        return false;
                    }
                    else
                    {
                        for(int i = 0; i < transforms.Count; i++)
                        {
                            if (transforms[i].name == filterObjectTransformName)
                            {
                                return true;
                            }
                        }

                        return false;
                    }

                case InclusionMode.Excluded:
                    if (gameObject.transform == null)
                    {
                        return false;
                    }
                    else
                    {
                        for(int i = 0; i < transforms.Count; i++)
                        {
                            if (transforms[i].name == filterObjectTransformName)
                            {
                                return false;
                            }
                        }

                        return true;
                    }

                default:
                    return true;
            }
        }

        /// <summary>
        /// Serialize
        /// </summary>
        /// <param name="info">Serializaiton Info</param>
        /// <param name="context">Streaming Context</param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("enabled", enabled, typeof(bool));
            info.AddValue("inclusionMode", inclusionMode, typeof(InclusionMode));
			info.AddValue("filterObjectTransformName", filterObjectTransformName, typeof(string));
			#if UNITY_EDITOR
            info.AddValue("thumbnailURL", thumbnailURL, typeof(string));
			#endif
            
        }

        /// <summary>
        /// On Disable Callback
        /// </summary>
        public void OnDisable()
        {

        }

        /// <summary>
        /// On Draw Gizmos callback
        /// </summary>
        public void OnDrawGizmos()
        {

        }

        /// <summary>
        /// On Enable Callback
        /// </summary>
        public void OnEnable()
        {

		}

        /// <summary>
        /// On Inspector GUI Callback
        /// </summary>
		public void OnInspectorGUI ()
		{
			#if UNITY_EDITOR
			GUILayout.BeginVertical();

			if (filterObjectTransformName != null)
			{
				GUILayout.Label ("Object Filter: " + filterObjectTransformName);
			}
			else
			{
				GUILayout.Label ("Object Filter: " + "Error Game Object is null");
			}

			GUILayout.BeginHorizontal();

			GUILayout.BeginVertical();

			if (this.thumbnail != null)
			{
				GUILayout.Box (thumbnail, GUILayout.Width (72), GUILayout.Height (72));
			}
			else
			{
				if (filterObjectTransformName != null)
				{
					GUILayout.Box (filterObjectTransformName, GUILayout.Width (72), GUILayout.Height (72));
				}
				else
				{
					GUILayout.Box ("Error Game Object is null", GUILayout.Width (72), GUILayout.Height (72));
				}
			}

			GUILayout.EndVertical();

			GUILayout.BeginVertical();

			this.enabled = EditorGUILayout.Toggle("Enabled:", this.enabled);
			this.inclusionMode = (InclusionMode)EditorGUILayout.EnumPopup("Inclusion Mode:", this.inclusionMode);

			GUILayout.EndVertical();
			GUILayout.EndHorizontal();
			GUILayout.EndVertical();
			#endif
		}

        /// <summary>
        /// On Scene GUI Callback
        /// </summary>
		public void OnSceneGUI ()
		{
			
		}
	}
}
