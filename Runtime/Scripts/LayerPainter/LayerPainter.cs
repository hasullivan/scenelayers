// <copyright file="LayerPainter.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>07/01/2016  </date>
// <summary>Layer Painter</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using UnityEngine;

// TODO: Need to add in code to detect if we are not in the unity editor (in a playable build) and clear the serialized info
// This will have to do until I find a way to hook into unitys build process, As this is of no use in player the component should be removed.

namespace SceneLayers {

    /// <summary>
    /// Layer Painter. All the work is done in the inspector. This only exists so that each layer can use a unique set of brushes and the 
    /// brushes can be serialized.
    /// </summary>
    [ExecuteInEditMode]
    [System.Serializable]
	public class LayerPainter : MonoBehaviour, ISerializationCallbackReceiver
    {

        /// <summary>
        /// Delegate to hook inspector and brush serialization into MonoBehaviour OnDraw Gizmos Callback
        /// </summary>
        public DelegateOnDrawGizmos onDrawGizmos;

        /// <summary>
        /// Delegate to hook inspector and brush serialization into MonoBehaviour Serialization Callback
        /// </summary>
        public DelegateOnSerializeLayerPainter onSerializeLayerPainter;
        
        /// <summary>
        /// Scroll view for current brush in inspector
        /// </summary>
        [SerializeField]
        private Vector2 brushScrollView = new Vector2(0.0f, 0.0f);

        /// <summary>
        /// Index of the current brush selected in the Inspector
        /// </summary>
        [SerializeField]
        private int currentBrush = 0;

		#if UNITY_EDITOR
        /// <summary>
        /// Bit of a hack. Brushes are not derived from MonoBehaviour or from ScriptableObject and they use interfaces so Unity can not serialize the brushes.
        /// To work around this limitation that will reset the brushes every time the layer painter loses focus or the user enters play mode, the entire brushlist
        /// in the inspector is serialized in the OnBeforeSerialize hook and stored here as a byte array, which Unity can serialize.
        /// </summary>
        [SerializeField]
        private byte[] serializedBrushes = new byte[0];
		#endif

        /// <summary>
        /// Delegate called by Unity's OnDrawGizmos Callback. Used to allow the inspector, or filter, or brush... to hook into the callback.
        /// On Draw Gizmos is only available to MonoBehaviours
        /// </summary>
		public delegate void DelegateOnDrawGizmos();

        /// <summary>
        /// Delegate called by Unity's OnBeforeSerialize Callback. Used to allow the inspector (or anything else) to hook into the callback.
        /// </summary>
        public delegate void DelegateOnSerializeLayerPainter();
        
        /// <summary>
        /// Gets or sets the Brush Scroll View
        /// </summary>
        public Vector2 BrushScrollView
        {
            get
            {
                return brushScrollView;
            }

            set
            {
                brushScrollView = value;
            }
        }

        /// <summary>
        /// Gets or sets the Current Brush index
        /// </summary>
        public int CurrentBrush
        {
            get
            {
                return currentBrush;
            }
            set
            {
                currentBrush = value;
            }
        }

		#if UNITY_EDITOR
        /// <summary>
        /// The Bytes Array that stores the Serialized Brush List. Used for editor serialization when entering play mode or deselecting layer painter.
        /// The brushes, Filters, and modifiers are interfaces so unity cannot serialize them.
        /// </summary>
        public byte[] SerializedBrushes
        {
            get
            {
                return serializedBrushes;
            }

            set
            {
                serializedBrushes = value;
            }
        }
		#endif
 
        /// <summary>
        /// On AFter Deserialize callback
        /// </summary>
		public void OnAfterDeserialize ()
		{
			// Brush deserialization is handled by the inspectors On Enable, not here
		}
 
        /// <summary>
        /// Inspector do not call the Iserialize callback reciever
        /// since this does get called, this is used to then serialize the inspector
        /// </summary>
        public void OnBeforeSerialize()
        {
            if(onSerializeLayerPainter != null)
            {
                this.onSerializeLayerPainter();
            }
        }

        /// <summary>
        /// On Draw Gizmos Callback
        /// </summary>
		public void OnDrawGizmos()
		{
			if (onDrawGizmos != null)
			{
				this.onDrawGizmos ();
			}
		}
    }
}

