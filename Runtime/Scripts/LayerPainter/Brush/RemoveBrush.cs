﻿// <copyright file="RemovalBrush.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>07/01/2016</date>
// <summary>Removal Brush for Layer Painter</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Runtime.Serialization;
using System.Collections.Generic;


namespace SceneLayers{

	/// <summary>
	/// Remove brush for removing instances in the game object it is attached to. Modes are Delete and Prune.
	/// </summary>
    [System.Serializable]
    [KnownType(typeof(InstanceRules))]
    [KnownType(typeof(SampleRules))]
	public class RemoveBrush : IBrush, ISerializable
	{
        private RemovalMode brushMode = RemovalMode.Delete;
        private Color deleteModeColor = new Color(1.0f, 0.0f, 0.0f, 0.2f);
        private bool isActive = false;
        private bool isMouseDown = false;
        private RemovalMode lastBrushMode = RemovalMode.Delete;
        private string name = string.Empty;
        private Color noModeColor = new Color(0.4f, 0.4f, 0.4f);
        private ObjectRules objectRules = new ObjectRules();
        private Transform parent;
        private Color pruneModeColor = new Color(1.0f, 0.5f, 0.3137f, .2f);
		private float radius = 3.0f;
        private int sampleRate = 1;
        private bool settingsFoldout = false;
        private Stroke stroke = new Stroke(100);

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="name">Name.</param>
        public RemoveBrush(string name)
		{
            this.Name = name;
		}

        /// <summary>
        /// Serializtion Constructor
        /// </summary>
        /// <param name="info">Serialization Info.</param>
        /// <param name="context">Streaming Context.</param>
        protected RemoveBrush(SerializationInfo info, StreamingContext context)
        {
            this.brushMode = (RemovalMode)info.GetValue("brushMode", typeof(RemovalMode));
            this.name = (string)info.GetValue("name", typeof(string));
            this.radius = (float)info.GetValue("radius", typeof(float));
            this.objectRules = (ObjectRules)info.GetValue("objectRules", typeof(ObjectRules));
        }

        /// <summary>
        /// Gets or sets the brush mode
        /// </summary>
        public RemovalMode BrushMode
        {
            get
            {
                return this.brushMode;
            }
            set
            {
                this.brushMode = value;
            }
        }

        /// <summary>
        /// Gets or sets the brushes name.
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                this.name = value;
            }
        }

        /// <summary>
        /// Gets or sets the parent of the brush.
        /// </summary>
        /// <value>The parent.</value>
        public Transform Parent
		{
			get
			{
				return this.parent;
			}
			set
			{
				this.parent = value;
			}
		}
       
        /// <summary>
        /// Gets or sets the brush radius
        /// </summary>
        public float Radius
        {
            get
            {
                return this.radius;
            }
            set
            {
                if (value < .001f)
                {
                    this.radius = 0.001f;
                }
                else
                {
                    this.radius = value;
                }
            }
        }

		/// <summary>
		/// Gets or sets the sample rate.
		/// </summary>
		/// <value>The sample rate.</value>
		public int SampleRate
		{
			get
			{
				return this.sampleRate;
			}
			set
			{
				if (value < 1)
				{
					this.sampleRate = 1;
				}
				else if (value > 50)
				{
					this.sampleRate = 50;
				} 
				else
				{
					this.sampleRate = value;
				}
			}
		}

        /// <summary>
        /// Handles User Input
        /// </summary>
        public bool GetInput()
        {

            // Create internal events for this stuff
            // Actually, Create a small helper class with events
            // left mouse down and left mouse up, etc
            bool remove = false;
	
            Event e = Event.current;

            if (e.isMouse == true && e.button == 0 && e.type == EventType.MouseDown)
            {
                if (e.alt == false)
                {
                    remove = true;
                    return remove;
                }
            }
            //Does this even work ? Find out why.
            else if (e.isMouse == true && e.button == 0 && e.type == EventType.MouseUp)
            {

            }
            else if (isMouseDown == true && remove == false)
            {
                remove = true;
            }

            return remove;
        }

        /// <summary>
        /// Serialization
        /// </summary>
        /// <param name="info">Serialization Info.</param>
        /// <param name="context">Streaming Context.</param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("brushMode", brushMode, typeof(RemovalMode));
            info.AddValue("name", name, typeof(string));
            info.AddValue("radius", radius, typeof(float));
            info.AddValue("objectRules", objectRules, typeof(ObjectRules));
        }

        /// <summary>
        /// On Disable Callback
        /// </summary>
        public void OnDisable()
        {
            this.objectRules.OnDisable();
        }

        /// <summary>
        /// On Draw Gizmos Callback
        /// </summary>
        public void OnDrawGizmos()
        {
            this.objectRules.OnDrawGizmos();
        }

        /// <summary>
        /// On Enable Callback
        /// </summary>
        public void OnEnable()
        {
			this.objectRules.OnEnable ();
		}
       
        /// <summary>
        /// On Inspector GUI
        /// </summary>
        public void OnInspectorGUI()
        {
			#if UNITY_EDITOR
            EditorGUILayout.Separator();
            this.DrawSettings();
			#endif
        }

        /// <summary>
        /// OnSceneGUI
        /// </summary>
        public void OnSceneGUI()
        {
			#if UNITY_EDITOR
            Tools.hidden = true;
            bool didHit = false;
            RaycastHit ray = new RaycastHit();

            if (this.isActive == true)
            {
                isMouseDown = GetInput();

                didHit = LayerPainterUtility.CastMouseRay(out ray);

                if (didHit == true && isMouseDown == true)
                {
                    stroke.Add(new StrokePoint(ray.point, ray.normal));
                }
            }

            switch (this.brushMode)
            {
                case RemovalMode.Delete:
                    if (isActive == true && didHit == true)
                    {
                        LayerPainterGUI.DrawBrushHandle(
                            ray.point,
                            ray.normal,
                            this.radius,
                            LayerPainterGUI.deleteModeColor);
                    }
                    break;

                case RemovalMode.Prune:
                    if (isActive == true)
                    {
                        LayerPainterGUI.DrawBrushHandle(
                            ray.point,
                            ray.normal,
                            this.radius,
                            LayerPainterGUI.pruneModeColor);
                    }
                    break;
            }
			#endif
        }

        /// <summary>
        /// Update this instance.
        /// </summary>
        public void Update()
		{
			#if UNITY_EDITOR
			if (this.isMouseDown == true && this.isActive == true)
			{
				switch (brushMode)
				{
					case RemovalMode.Delete:
						this.DeleteInstances ();
						break;

					case RemovalMode.Prune:
						this.PruneInstances ();
						break;	
				}
			} 
			#endif
		}

		/// <summary>
		/// Deletes the instances.
		/// </summary>
		private void DeleteInstances()
		{
			List<GameObject> layerGameObjects = this.SampleGameObjectsInProximity (stroke.LastPoint.Position, this.radius);

			for (int i = 0; i < layerGameObjects.Count; i++)
			{
				if (layerGameObjects [i] != null)
				{
					bool filterResult = objectRules.ApplyFilters (layerGameObjects [i]);

					if (filterResult == true)
					{
						LayerPainterUtility.RemoveGameObjectFromCache (layerGameObjects [i]);
						GameObject.DestroyImmediate (layerGameObjects [i]);
					}
				}
			}
		}

        /// <summary>
        /// Draws the settings.
        /// </summary>
        private void DrawSettings()
        {
			#if UNITY_EDITOR
            Color bgColor = GUI.backgroundColor;

            switch (brushMode)
            {
                case RemovalMode.Delete:
                    if (isActive == false)
                    {
                        if (GUILayout.Button("Start Deleting Instances", GUILayout.Height(30)) == true)
                        {
                            isActive = true;
                            LayerPainterUtility.CacheChildGameObjects(this.parent.gameObject);
                        }
                    }
                    else
                    {
                        GUI.backgroundColor = Color.green;

                        if (GUILayout.Button("Stop Deleting Instances", GUILayout.Height(30)) == true)
                        {
                            isActive = false;
                        }

                        GUI.backgroundColor = bgColor;
                    }
                    break;

                case RemovalMode.Prune:
                    if (isActive == false)
                    {
                        if (GUILayout.Button("Start Pruning Instances", GUILayout.Height(30)) == true)
                        {
                            isActive = true;
                            LayerPainterUtility.CacheChildGameObjects(this.parent.gameObject);
                        }
                    }
                    else
                    {
                        GUI.backgroundColor = Color.green;

                        if (GUILayout.Button("Stop Pruming Instances", GUILayout.Height(30)) == true)
                        {
                            isActive = false;
                        }

                        GUI.backgroundColor = bgColor;
                    }

                    break;
            }
            EditorGUILayout.Separator();

            this.SwitchBrushMode((RemovalMode)EditorGUILayout.EnumPopup("Placement Mode:", this.brushMode));

            settingsFoldout = EditorGUILayout.Foldout(settingsFoldout, "Brush Settings:");

            if (settingsFoldout == true)
            {
                switch (this.brushMode)
                {
                    case RemovalMode.Delete:
                        EditorGUILayout.BeginHorizontal();

                        this.radius = EditorGUILayout.DelayedFloatField("Radius:", this.radius);

                        EditorGUILayout.EndHorizontal();

                        this.radius = EditorGUILayout.Slider(radius, 0.001f, 100.0f);

                        break;

                    case RemovalMode.Prune:
                        EditorGUILayout.BeginHorizontal();

                        this.radius = EditorGUILayout.DelayedFloatField("Radius:", this.radius);

                        EditorGUILayout.EndHorizontal();

                        this.radius = EditorGUILayout.Slider(radius, 0.001f, 100.0f);
                        EditorGUILayout.LabelField("Sample Rate:");
                        this.sampleRate = EditorGUILayout.IntSlider(this.sampleRate, 1, 50);

                        break;
                }

                LayerPainterGUI.DrawHorizontalSeparator();
                this.objectRules.OnInspectorGUI();
            }
			#endif
        }

        /// <summary>
        /// Prunes the instances. Pruning this out the desity of the obejcts. It is the opposite
        /// of the placement brush in scatter mode
        /// </summary>
        private void PruneInstances()
		{
			if(this.stroke.StrokePoints.Count > 0)
			{
				List<RaycastHit> samples = new List<RaycastHit>();
				List<GameObject> prunableGameObjects = new List<GameObject> ();

				for (int i = 0; i < sampleRate; i++)
				{
					samples.Add (LayerPainterUtility.GetRandomSamplePoint (
						this.stroke.LastPoint.Position,
						this.stroke.LastPoint.Normal,
						0.0f,
						this.radius));
				}

				for (int i = 0; i < samples.Count; i++)
				{
					List<GameObject> inProximityToSample = this.SampleGameObjectsInProximity (samples [i].point, .3f);
					prunableGameObjects.AddRange (inProximityToSample);
				}

				for (int i = 0; i < prunableGameObjects.Count; i++)
				{
					if (prunableGameObjects [i] != null)
					{
						bool filterResult = objectRules.ApplyFilters (prunableGameObjects [i]);
						//Debug.Log (filterResult);
						if (filterResult == true)
						{
							LayerPainterUtility.RemoveGameObjectFromCache (prunableGameObjects [i]);
							GameObject.DestroyImmediate (prunableGameObjects [i]);

						}
					}
				}
			}
		}
        
        /// <summary>
        /// Samples the game objects in the cache within the radius of the provided world position
        /// </summary>
        /// <param name="worldPoint">World Position</param>
        /// <param name="radius">Radius</param>
        private List<GameObject> SampleGameObjectsInProximity(Vector3 worldPoint, float radius)
        {
            HashSet<GameObject> childGameObjects = LayerPainterUtility.GetGameObjectsFromCache();
            List<GameObject> prefabRoots = new List<GameObject>();

            foreach (GameObject gameObject in childGameObjects)
            {
                if (Vector3.Distance(worldPoint, gameObject.transform.position) <= radius)
                {
                    prefabRoots.Add(gameObject);
                }
            }

            return prefabRoots;
        }

        /// <summary>
        /// Switchs the brush mode.
        /// </summary>
        /// <param name="removalMode">Removal mode.</param>
        private void SwitchBrushMode(RemovalMode removalMode)
		{
			this.lastBrushMode = this.brushMode;
			this.brushMode = removalMode;

			if(this.lastBrushMode != this.brushMode)
			{
				this.isActive = false;
			}
		}
    }
}
