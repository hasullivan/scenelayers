﻿// <copyright file="StrokePoint.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>07/01/2016</date>
// <summary>Stroke Point</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using UnityEngine;
using System.Runtime.Serialization;

namespace SceneLayers
{
	/// <summary>
	/// Stroke point stores the inforation for each point in a stroke.
	/// </summary>
	[System.Serializable]
	public class StrokePoint : ISerializable
	{
        private Vector3 normal;
        private Vector3 position;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="position">Position.</param>
        /// <param name="normal">Normal.</param>
        public StrokePoint(Vector3 position, Vector3 normal)
		{
			this.position = position;
			this.normal = normal;
		}

        /// <summary>
        /// Serialization Constructor
        /// </summary>
        /// <param name="info">Info.</param>
        /// <param name="context">Context.</param>
        protected StrokePoint(SerializationInfo info, StreamingContext context)
        {
            this.position = (Vector3)info.GetValue("position", typeof(Vector3));
            this.normal = (Vector3)info.GetValue("normal", typeof(Vector3));
        }

        /// <summary>
        /// Gets the normal.
        /// </summary>
        /// <value>The normal.</value>
        public Vector3 Normal
        {
            get
            {
                return this.normal;
            }
        }

        /// <summary>
        /// Gets the position.
        /// </summary>
        /// <value>The position.</value>
        public Vector3 Position
		{
			get
			{
				return this.position;
			}
		}

		/// <summary>
		/// Serialization
		/// </summary>
		/// <param name="info">Info.</param>
		/// <param name="context">Context.</param>
		public void GetObjectData (SerializationInfo info, StreamingContext context)
		{
			info.AddValue ("position", position, typeof(Vector3));
			info.AddValue ("normal", normal, typeof(Vector3));
		}
	}
}
