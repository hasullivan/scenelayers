﻿// <copyright file="PlacementBrush.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>07/01/2016</date>
// <summary>Placement Brush for Layer Painter</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SceneLayers
{
	/// <summary>
	/// Placement brush adds instances to the scene based on brush mode (Scatter, Single, or Stepped).
	/// </summary>
    [System.Serializable]
    [KnownType(typeof(InstanceRules))]
    [KnownType(typeof(SampleRules))]
	[KnownType(typeof(InstanceObject))]
    public class PlacementBrush : IBrush, ISerializable
    { 
		private readonly string[] toolbarText = new string[] { "Brush", "Instance Objects" };
        private PlacementMode brushMode = PlacementMode.Scatter;
        private float innerRadius = .5f;
        private List<InstanceObject> instanceObjects = new List<InstanceObject>();
        private Vector2 instanceObjectsScrollView = new Vector2(0.0f, 0.0f);
        private InstanceRules instanceRules = new InstanceRules();
        private bool isActive = false;
        private bool isMouseDown = false;
        private PlacementMode lastBrushMode = PlacementMode.Scatter;
        private Vector3 lastSteppedNormal;
        private Vector3 lastSteppedPosition;
        private string name = "";
        private GameObject objectTemp;
        private float outerRadius = 3.0f;
        private Transform parent;
        private int sampleRate = 3;
        private SampleRules samplingRules = new SampleRules();
        private bool settingsFoldout = true;
        private float singleBrushRadius = 1.0f;
        private Bounds singleInstanceBounds;
        private Collider[] singleInstanceColliders;
        private float singleInstanceSize = 1.0f;
        private GameObject singlePlacementObject;
        private Vector3 singlePlacementOrigin;
        private Vector3 singlePlacementOriginalScale;
        private Vector3 singlePlacementRotation;
        private float stepLength = 2.0f;
        private List<Collider> steppedInstanceColliders = new List<Collider>();
        private Vector3 steppedStartPosition;
        private Stroke stroke = new Stroke(200);
        private int toolbar = 0;
        
        /// <summary>
        /// Default Constructor
        /// </summary>
        /// <param name="name"></param>
		public PlacementBrush(string name)
        {
            this.Name = name;
        }

        /// <summary>
        /// Serialization Constructor, used to deserialize the brush
        /// </summary>
        /// <param name="info">Info.</param>
        /// <param name="context">Context.</param>
        protected PlacementBrush(SerializationInfo info, StreamingContext context)
        {
            this.brushMode = (PlacementMode)info.GetValue("brushMode", typeof(PlacementMode));
            this.innerRadius = (float)info.GetValue("innerRadius", typeof(float));
            this.instanceObjects = (List<InstanceObject>)info.GetValue("instanceObjects", typeof(List<InstanceObject>));
            this.instanceObjectsScrollView = (Vector2)info.GetValue("instanceObjectsScrollView", typeof(Vector2));
            this.instanceRules = (InstanceRules)info.GetValue("instanceRules", typeof(InstanceRules));
            this.name = (string)info.GetValue("name", typeof(string));
            this.outerRadius = (float)info.GetValue("outerRadius", typeof(float));
            this.samplingRules = (SampleRules)info.GetValue("samplingRules", typeof(SampleRules));
            this.settingsFoldout = (bool)info.GetValue("settingsFoldout", typeof(bool));
            this.singleBrushRadius = (float)info.GetValue("singleBrushRadius", typeof(float));
            this.stepLength = (float)info.GetValue("stepLength", typeof(float));
            this.sampleRate = (int)info.GetValue("sampleRate", typeof(int));
        }

        /// <summary>
        /// Gets or sets the inner brush radius, used by the scatter brush mode
        /// </summary>
        public float InnerRadius
        {
            get
            {
                return this.innerRadius;
            }

            set
            {
                if (value >= 0.0f)
                {
                    if (value <= outerRadius)
                    {
                        this.innerRadius = value;
                    }
                    else
                    {
                        this.innerRadius = outerRadius;
                    }

                }
                else
                {
                    this.innerRadius = 0.0f;
                }
            }
        }

        /// <summary>
        /// Gets or set the Instance Objects. Instance objects hold the references and instance specific filters
		/// and modifiers used when placing an instance
        /// </summary>
        public List<InstanceObject> InstanceObjects
        {
            get
            {
                return this.instanceObjects;
            }

            set
            {
                this.instanceObjects = value;
            }
        }

        /// <summary>
        /// Gets or sets the instance rules. Brush wide isntance rules. These Rules are only applied if the 
        /// Instance Object does not have Overrides.
        /// </summary>
        /// <value>The instance rules.</value>
        public InstanceRules InstanceRules
        {
            get
            {
                return this.instanceRules;
            }

            set
            {
                this.instanceRules = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of this brush
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                this.name = value;
            }
        }

        /// <summary>
        /// Gets or sets the Outer Radius. Use by the Scatter mode
        /// </summary>
        public float OuterRadius
        {
            get
            {
                return this.outerRadius;
            }

            set
            {
                if (value >= outerRadius)
                {
                    if (value >= innerRadius)
                    {
                        this.outerRadius = value;
                    }
                    else
                    {
                        this.outerRadius = innerRadius;
                    }
                }
                else
                {
                    this.outerRadius = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the parent. The parent is used by default as parent for instances.
        /// </summary>
        /// <value>The parent.</value>
        public Transform Parent
		{
			get
			{
				return this.parent;
			}
			set
			{
				this.parent = value;
			}
		}
		/// <summary>
		/// Gets or sets the sample rate. Sample Rate is the number of scene samples to take per update
		/// </summary>
		/// <value>The sample rate.</value>
		public int SampleRate
		{
			get
			{
				return this.sampleRate;
			}
			set
			{
				if (value < 1)
				{
					this.sampleRate = 1;
				}
				else if (value > 50)
				{
					this.sampleRate = 50;
				} 
				else
				{
					this.sampleRate = value;
				}
			}
		}
			
        /// <summary>
        /// Gets or sets the Sample Rules. These will be used if an instance does not have overrides.
        /// </summary>
		public SampleRules SamplingRules
        {
            get
            {
                return this.samplingRules;
            }

            set
            {
                this.samplingRules = value;
            }
        }
			
        /// <summary>
        /// Gets or sets the Step Length
        /// </summary>
        public float StepLength
        {
            get
            {
                return this.stepLength;
            }

            set
            {
                this.stepLength = value;
            }
        }


        /// <summary>
        /// Gets or sets the Brush Stroke
        /// </summary>
		public Stroke Stroke
        {
            get
            {
                return this.stroke;
            }

            set
            {
                this.stroke = value;
            }
        }

        /// <summary>
        /// Draw the instance objects in the inspector gui
        /// </summary>
        public void DrawInstanceObjects()
        {
			#if UNITY_EDITOR
            this.objectTemp = (GameObject)EditorGUILayout.ObjectField("Instance to Add", this.objectTemp, typeof(GameObject), false, GUILayout.ExpandWidth(true), GUILayout.Height(25));

            if (GUILayout.Button("Add Instance Object", GUILayout.Height(25)) == true)
            {
                if (this.objectTemp != null)
                {
                    this.InstanceObjects.Add(new InstanceObject(objectTemp));
                }

                this.objectTemp = null;
            }

            this.instanceObjectsScrollView = EditorGUILayout.BeginScrollView(this.instanceObjectsScrollView, GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));

            for (int i = 0; i < this.instanceObjects.Count; i++)
            {
                GUILayout.BeginHorizontal();
                GUILayout.BeginVertical();

                if (GUILayout.Button("-", GUILayout.Width(20), GUILayout.Height(20)) == true)
                {
                    this.instanceObjects[i] = null;
                }

                GUILayout.EndVertical();
                LayerPainterGUI.HorizontalPadding();
                GUILayout.BeginVertical();

                if (instanceObjects[i] != null)
                {
                    this.instanceObjects[i].OnInspectorGUI();
                }

                GUILayout.EndVertical();

                GUILayout.EndHorizontal();
                LayerPainterGUI.DrawHorizontalSeparator();
            }

            this.instanceObjects.RemoveAll(item => item == null);

            EditorGUILayout.EndScrollView();
			#endif
        }

        /// <summary>
        /// Handles User Input
        /// </summary>
        public bool GetInput()
        {
            bool place = false;
            Event e = Event.current;

            if (e.isMouse == true && e.button == 0 && e.type == EventType.MouseDown)
            {
                if (e.alt == false)
                {
                    place = true;
                    return place;

                }
            }
            // This makes no difference ??? Find out why this does not work and fix up proper
            else if (e.isMouse == true && e.button == 0 && e.type == EventType.MouseUp)
            {

            }
            else if (isMouseDown == true && place == false)
            {
                place = true;
            }

            if (place == false)
            {
                this.stroke.Clear();

                if (this.singleInstanceColliders != null)
                {
                    LayerPainterUtility.EnableColliders(this.singleInstanceColliders);
                }

                if (this.steppedInstanceColliders.Count > 0)
                {
                    LayerPainterUtility.EnableColliders(this.steppedInstanceColliders.ToArray());
                    this.steppedInstanceColliders.Clear();
                }

                this.singleInstanceColliders = null;
            }

            return place;
        }

        /// <summary>
        /// Serialize the Brush
        /// </summary>
        /// <param name="info">Serialization Info</param>
        /// <param name="context">Streaming Context</param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("brushMode", this.brushMode, typeof(PlacementMode));
            info.AddValue("innerRadius", this.innerRadius, typeof(float));
            info.AddValue("instanceObjects", this.instanceObjects, typeof(List<InstanceObject>));
            info.AddValue("instanceObjectsScrollView", this.instanceObjectsScrollView, typeof(Vector2));
            info.AddValue("instanceRules", this.instanceRules, typeof(InstanceRules));
            info.AddValue("name", this.name, typeof(string));
            info.AddValue("outerRadius", this.outerRadius, typeof(float));
            info.AddValue("samplingRules", this.samplingRules, typeof(SampleRules));
            info.AddValue("settingsFoldout", this.settingsFoldout, typeof(bool));
            info.AddValue("singleBrushRadius", this.singleBrushRadius, typeof(float));
            info.AddValue("stepLength", this.stepLength, typeof(float));
            info.AddValue("sampleRate", this.sampleRate, typeof(int));
        }

        /// <summary>
        /// On Disable Callback
        /// </summary>
        public void OnDisable()
        {
            for (int i = 0; i < this.instanceObjects.Count; i++)
            {
                this.instanceObjects[i].OnDisable();
            }

            this.samplingRules.OnDisable();
            this.instanceRules.OnDisable();
        }

        /// <summary>
        /// Used for Drawing Gizmos in the scene
        /// </summary>
        public void OnDrawGizmos()
        {
            this.samplingRules.OnDrawGizmos();
            this.instanceRules.OnDrawGizmos();
            for (int i = 0; i < this.instanceObjects.Count; i++)
            {
                this.instanceObjects[i].OnDrawGizmos();
            }
        }

        /// <summary>
        /// On Enable Callback
        /// </summary>
        public void OnEnable()
        {
			for (int i = 0; i < this.instanceObjects.Count; i++)
			{
				this.instanceObjects [i].OnEnable ();
			}

			this.samplingRules.OnEnable ();
			this.instanceRules.OnEnable ();
		}
        
        /// <summary>
        /// Called when Drawing this brush to the inspector
        /// </summary>
        public void OnInspectorGUI()
        {
			#if UNITY_EDITOR
			LayerPainterGUI.DrawHorizontalSeparator();
			this.toolbar = GUILayout.Toolbar(this.toolbar, this.toolbarText);
			switch (this.toolbar)
			{
				case 0:
					EditorGUILayout.Separator ();
					this.DrawSettings ();
					break;

				case 1:
					EditorGUILayout.Separator ();
					this.DrawInstanceObjects();
					break;
			}
			#endif
        }

        /// <summary>
        /// Once Scene GUI, used to draw brushes to the scene
        /// </summary>
        public void OnSceneGUI()
        {
			#if UNITY_EDITOR
            Tools.hidden = true;

            bool didHit = false;
            bool doesComply = false;
            RaycastHit ray = new RaycastHit();

            if (this.isActive == true)
            {
                this.isMouseDown = GetInput();

                didHit = LayerPainterUtility.CastMouseRay(out ray);

                if (didHit == true)
                {
                    doesComply = DoesComplyToHostFilters(ray);
                }

                if (didHit == true && doesComply == true && this.isMouseDown == true)
                {
                    this.stroke.Add(new StrokePoint(ray.point, ray.normal));
                }
            }

            switch (this.brushMode)
            {
                case PlacementMode.Scatter:
                    if (this.isActive == true && didHit == true)
                    {
                        if (doesComply == true)
                        {
                            LayerPainterGUI.DrawDoubleBrushToolHandle(
                                ray.point,
                                ray.normal,
                                this.innerRadius,
                                this.outerRadius,
                                LayerPainterGUI.paintModeColor,
                                LayerPainterGUI.paintModeColor);
                        }
                        else
                        {
                            LayerPainterGUI.DrawBrushHandle(
                                ray.point,
                                ray.normal,
                                this.outerRadius,
                                LayerPainterGUI.noModeColor);
                        }
                    }
                    break;

                case PlacementMode.Single:
                    if (this.isActive == true)
                    {
                        if (this.stroke.StrokePoints.Count > 1)
                        {
                            LayerPainterGUI.DrawDirectionalToolHandle(
                                this.stroke.FirstPoint.Position,
                                this.stroke.FirstPoint.Normal,
                                ray.point,
                                LayerPainterGUI.singleModeColor,
                                Color.white);
                        }
                        else if (this.stroke.StrokePoints.Count <= 1)
                        {
                            if (doesComply == true)
                            {
                                LayerPainterGUI.DrawDirectionalToolHandle(
                                    ray.point,
                                    ray.normal,
                                    ray.point,
                                    LayerPainterGUI.singleModeColor,
                                    Color.white);
                            }
                            else
                            {
                                LayerPainterGUI.DrawBrushHandle(
                                    ray.point,
                                    ray.normal,
                                    singleBrushRadius,
                                    LayerPainterGUI.noModeColor);
                            }
                        }
                    }
                    break;

                case PlacementMode.Stepped:
                    if (this.isActive == true && didHit == true)
                    {
                        if (doesComply == true)
                        {
                            LayerPainterGUI.DrawBrushHandle(
                                ray.point,
                                ray.normal,
                                0.3f,
                                LayerPainterGUI.steppedModeColor);
                        }
                        else
                        {
                            LayerPainterGUI.DrawBrushHandle(
                                ray.point,
                                ray.normal,
                                0.3f,
                                LayerPainterGUI.noModeColor);


                        }

                        if (this.stroke.StrokePoints.Count > 2)
                        {
                            LayerPainterGUI.DrawDirectionToolHandle(
                                this.steppedStartPosition,
                                this.stroke.LastPoint.Position - this.steppedStartPosition,
                                this.stepLength,
                                LayerPainterGUI.steppedInfoColor,
                                LayerPainterGUI.steppedToolColor);
                        }
                    }
                    break;
            }
			#endif
        }

        /// <summary>
        /// Update this instance. This is Called 
        /// </summary>
        public void Update()
        {
			#if UNITY_EDITOR
            if (this.isMouseDown == true && this.isActive == true)
            {
                switch (brushMode)
                {
                    case PlacementMode.Scatter:
                        this.ScatterInstances();
                        break;
                    case PlacementMode.Single:
                        this.SingleInstance();
                        break;
                    case PlacementMode.Stepped:
                        this.SteppedInstances();
                        break;
                }
            }
            else
            {
                this.singlePlacementObject = null;
            }
			#endif
        }

        /// <summary>
        /// Special Filter for handle brush center raycast. THhs ray is cast into the scene everyupdate. It takes into account the permitted and exluded
        /// object filters. Permitted is a speccial mode that is exclusive, so normally only one will run at a time.
		/// This method Runs all of them so that the brush can give feedback to the user.
        /// </summary>
        /// <returns>Does the ray hit a a permitted object</returns>
        private bool DoesComplyToHostFilters(RaycastHit ray)
        {
            int filterCount = 0;

            foreach (ISampleFilter sampleFilter in this.samplingRules.SampleFilterStack.SampleFilters)
            {
                if (sampleFilter.GetType() == typeof(SampleObjectFilter))
                {

                    SampleObjectFilter filter = (SampleObjectFilter)sampleFilter;

                    if (filter.Enabled == true)
                    {
                        filterCount += 1;
                        bool result = filter.ApplyFilter(ray);

                        if (filter.InclusionMode == InclusionMode.Permitted)
                        {
                            if (result == true)
                            {
                                return true;
                            }
                        }
                        else
                        {
                            if (result == false)
                            {
                                return false;
                            }
                            else
                            {
                                return true;
                            }

                        }
                    }
                }
            }

            if (filterCount == 0)
            {
                return true;
            }

            // If the tests did not early out then return false
            return false;
        }

        /// <summary>
        /// Draws the brush settings in the inspector
        /// </summary>
        private void DrawSettings ()
		{
			#if UNITY_EDITOR
			Color bgColor = GUI.backgroundColor;

			switch (brushMode)
			{
				case PlacementMode.Scatter:
					if (this.isActive == false)
					{
						if (GUILayout.Button("Start Scattering Instances",GUILayout.Height(30)) == true)
						{
							LayerPainterUtility.CacheSceneTransforms ();
							this.isActive = true;
						}
					}
					else
					{
						GUI.backgroundColor = Color.green;

						if (GUILayout.Button("Stop Scattering Instances",GUILayout.Height(30)) == true)
						{
							this.isActive = false;
						}

						GUI.backgroundColor = bgColor;
					}
					break;

				case PlacementMode.Single:
					if (this.isActive == false)
					{
						if (GUILayout.Button("Place an Instance",GUILayout.Height(30)) == true)
						{
							LayerPainterUtility.CacheSceneTransforms ();
							this.isActive = true;
						}
					}
					else
					{	
						GUI.backgroundColor = Color.green;
						if (GUILayout.Button("Stop Placing an Instance",GUILayout.Height(30)) == true)
						{
							this.isActive = false;
						}
						GUI.backgroundColor = bgColor;
					}
					break;

				case PlacementMode.Stepped:
					if (this.isActive == false)
					{
						if (GUILayout.Button("Start Placing Instances",GUILayout.Height(30)) == true)
						{
							LayerPainterUtility.CacheSceneTransforms ();
							this.isActive = true;
						}
					}
					else
					{
						GUI.backgroundColor = Color.green;
						if (GUILayout.Button("Stop Placing Instances",GUILayout.Height(30)) == true)
						{
							this.isActive = false;
						}
						GUI.backgroundColor = bgColor;
					}
					break;

			}

			EditorGUILayout.Separator();

			this.SwitchBrushMode((PlacementMode)EditorGUILayout.EnumPopup("Placement Mode:", this.brushMode));

			if (this.brushMode == PlacementMode.Scatter)
			{
				this.settingsFoldout = EditorGUILayout.Foldout (this.settingsFoldout, "Brush Settings:");
			}

			if (settingsFoldout == true)
			{
				switch (this.brushMode)
				{
					case PlacementMode.Scatter:
						EditorGUILayout.LabelField ("Sample Rate:");
						this.sampleRate = EditorGUILayout.IntSlider (this.sampleRate, 1, 50);

						EditorGUILayout.BeginHorizontal();

						this.innerRadius = EditorGUILayout.DelayedFloatField("Inner Radius:", this.InnerRadius);
						this.outerRadius = EditorGUILayout.DelayedFloatField("Outer Radius:", this.OuterRadius);

						EditorGUILayout.EndHorizontal();

						EditorGUILayout.MinMaxSlider(ref this.innerRadius, ref this.outerRadius, 0.0f, 100.0f);
						break;

					case PlacementMode.Single:
						break;

					case PlacementMode.Stepped:
						this.stepLength = EditorGUILayout.Slider("Step Length", this.stepLength, 0.0f, 100.0f);
						break;
				}

				LayerPainterGUI.DrawHorizontalSeparator();

				switch (this.brushMode)
				{
					case PlacementMode.Scatter:
						this.samplingRules.OnInspectorGUI ();
						this.instanceRules.OnInspectorGUI ();
						break;

					case PlacementMode.Stepped:
						this.instanceRules.OnInspectorGUI ();
						break;
				}
			}
			#endif
		}

		/// <summary>
		/// Scatters instances in the scene.
		/// </summary>
		public void ScatterInstances()
		{
			if(this.stroke.StrokePoints.Count > 0)
			{
				List<RaycastHit> samples = new List<RaycastHit>();
				List<InstanceObject> enabledInstances = new List<InstanceObject> ();
				List<InstanceObject> instances = new List<InstanceObject> ();
				List<float> frequencies = new List<float> ();

                for(int i = 0; i < this.instanceObjects.Count; i++)
                {
                    if (instanceObjects[i].Enabled == true)
                    {
                        enabledInstances.Add(instanceObjects[i]);
                        frequencies.Add(instanceObjects[i].PlacementFrequency);
                    }
                }

				for (int i = 0; i < this.sampleRate; i++)
				{
					RaycastHit ray = LayerPainterUtility.GetRandomSamplePoint (
						                 this.stroke.LastPoint.Position,
						                 this.stroke.LastPoint.Normal,
						                 this.innerRadius,
						                 this.outerRadius);
					if (ray.transform != null)
					{
						samples.Add (ray);
						instances.Add (enabledInstances [LayerPainterUtility.GetRandomIndexByFrequencies (frequencies)]);
					}
				}

				//For each instance try to place it with its respective sample
				for(int i = 0; i < instances.Count; i++)
				{
					bool result = true;
				
					if (instances [i].SamplingRules.SampleFilterStack.SampleFilters.Count > 0)
					{
						result = instances [i].SamplingRules.ApplyFilters (samples [i]);

						if (result == false)
						{
							continue;
						}
					}

					else if (this.SamplingRules.SampleFilterStack.SampleFilters.Count > 0)
					{
						result = this.SamplingRules.ApplyFilters (samples [i]);

						if (result == false)
						{
							continue;
						} 
					}


					if (result == true)
					{
						GameObject newInstance = LayerPainterUtility.PaintInstance(instances[i].GameObject);
						newInstance.transform.parent = this.parent;
						newInstance.transform.position = samples [i].point;

						if (instances [i].InstanceRules.InstanceModifierStack.InstanceModifiers.Count > 0)
						{
							instances [i].InstanceRules.ApplyModifiers (newInstance);
						}
						else if (this.instanceRules.InstanceModifierStack.InstanceModifiers.Count > 0)
						{
							this.instanceRules.ApplyModifiers (newInstance);
						}
					}
				}
			}
		}
			
		/// <summary>
		/// Places a single instance in the scene
		/// </summary>
		public void SingleInstance()
		{
			if (this.stroke.StrokePoints.Count > 1 && this.singlePlacementObject == null)
			{
				List<InstanceObject> enabledInstances = new List<InstanceObject> ();
				List<float> frequencies = new List<float> ();

                for(int i = 0; i < instanceObjects.Count; i++)
                {
                    if (instanceObjects[i].Enabled == true)
                    {
                        enabledInstances.Add(instanceObjects[i]);
                        frequencies.Add(instanceObjects[i].PlacementFrequency);
                    }
                }

				if (enabledInstances.Count > 0)
				{
					this.singlePlacementObject = LayerPainterUtility.PaintInstance( enabledInstances [LayerPainterUtility.GetRandomIndexByFrequencies (frequencies)].GameObject);
					this.singlePlacementObject.transform.parent = this.parent;
					this.singlePlacementObject.transform.position = this.stroke.FirstPoint.Position;
					this.singlePlacementOriginalScale = this.singlePlacementObject.transform.localScale;
					this.singleInstanceBounds = LayerPainterUtility.GetBounds (this.singlePlacementObject);
					this.singleInstanceSize = ((singleInstanceBounds.extents.x * 2.0f) + (singleInstanceBounds.extents.z * 2.0f)) * .5f;
					this.singleInstanceColliders = LayerPainterUtility.DisableActiveColliders(singlePlacementObject);
				}
			}

			if (singlePlacementObject != null)
			{
				
				float brushDiameter = Vector3.Distance (this.stroke.FirstPoint.Position, this.stroke.LastPoint.Position) * 2.0f;
				float instanceScaleRatio = brushDiameter / this.singleInstanceSize;

				if (instanceScaleRatio < 0.01f)
				{
					instanceScaleRatio = 0.01f;
				}

				Vector3 instanceScale = this.singlePlacementOriginalScale * instanceScaleRatio;
				this.singlePlacementObject.transform.localScale = instanceScale;
				this.singlePlacementObject.transform.LookAt (this.stroke.LastPoint.Position,Vector3.up);
			}
		}

		/// <summary>
		/// Spawns instances along stepped intervals.
		/// </summary>
		private void SteppedInstances()
		{
			if (this.stroke.StrokePoints.Count == 1)
			{
				this.steppedStartPosition = this.stroke.FirstPoint.Position;
			}
			// Check if it should spawn an instance
			if (this.stroke.StrokePoints.Count >= 2)
			{
				if (Vector3.Distance (this.steppedStartPosition, this.stroke.LastPoint.Position) >= this.stepLength)
				{
					Vector3 nextInstancePosition = this.steppedStartPosition + ((this.stroke.LastPoint.Position - this.steppedStartPosition).normalized * this.stepLength);

					// Sort through enabled objects
					List<InstanceObject> enabledInstances = new List<InstanceObject> ();
					List<float> frequencies = new List<float> ();

                    for(int i = 0; i < this.instanceObjects.Count; i++)
                    {
                        if (instanceObjects[i].Enabled == true)
                        {
                            enabledInstances.Add(instanceObjects[i]);
                            frequencies.Add(instanceObjects[i].PlacementFrequency);
                        }
                    }


					if (enabledInstances.Count > 0)
					{
						int instanceIndex = LayerPainterUtility.GetRandomIndexByFrequencies (frequencies);
						GameObject instance = LayerPainterUtility.PaintInstance (enabledInstances[instanceIndex].GameObject);
						instance.transform.parent = this.parent;
						instance.transform.position = this.steppedStartPosition;
						instance.transform.LookAt (nextInstancePosition,Vector3.up);

						// Apply Modifiers
						if (enabledInstances[instanceIndex].InstanceRules.InstanceModifierStack.InstanceModifiers.Count > 0)
						{
							enabledInstances[instanceIndex].InstanceRules.ApplyModifiers (instance);
						}
						else if (this.instanceRules.InstanceModifierStack.InstanceModifiers.Count > 0)
						{
							this.instanceRules.ApplyModifiers (instance);
						}

						this.steppedInstanceColliders.AddRange (LayerPainterUtility.DisableActiveColliders (instance));
					}

					this.steppedStartPosition = nextInstancePosition;
				}
			}
		}

		/// <summary>
		/// Switchs the brush mode. Resets active state when mode is changed.
		/// </summary>
		/// <param name="placementMode">Placement mode.</param>
		private void SwitchBrushMode(PlacementMode placementMode)
		{
			this.lastBrushMode = this.brushMode;
			this.brushMode = placementMode;

			if(this.lastBrushMode != this.brushMode)
			{
				this.isActive = false;
			}

		}

    }
}
