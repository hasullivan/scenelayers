﻿// <copyright file="Stroke.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>04/11/2016  </date>
// <summary>Brush Stroke</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using UnityEngine;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace SceneLayers
{
	/// <summary>
	/// Stroke stores the raycast hit position and normal information as a stroke
	/// </summary>
	[System.Serializable]
	public class Stroke : ISerializable
	{
		private int maximumSamples = 100;
		private List<StrokePoint> strokePoints = new List<StrokePoint> ();

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="maximumSamples">Maximum samples.</param>
		public Stroke(int maximumSamples)
		{
			this.maximumSamples = maximumSamples;
		}

        /// <summary>
        /// Serialization Constructor
        /// </summary>
        /// <param name="info">Info.</param>
        /// <param name="context">Context.</param>
        protected Stroke(SerializationInfo info, StreamingContext context)
        {
            this.maximumSamples = (int)info.GetValue("maximumSamples", typeof(int));
            this.strokePoints = (List<StrokePoint>)info.GetValue("strokePoints", typeof(List<StrokePoint>));
        }

        /// <summary>
        /// Gets the first point.
        /// </summary>
        /// <value>The first point.</value>
        public StrokePoint FirstPoint
        {
            get
            {
                if (this.strokePoints.Count > 0)
                {
                    return this.strokePoints[0];
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Gets the last point.
        /// </summary>
        /// <value>The last point.</value>
        public StrokePoint LastPoint
        {
            get
            {
                if (this.strokePoints.Count > 0)
                {
                    return this.strokePoints[this.strokePoints.Count - 1];
                }
                else
                {
                    return null;
                }
            }

        }

        /// <summary>
        /// Gets the stroke points.
        /// </summary>
        /// <value>The stroke points.</value>
        public List<StrokePoint> StrokePoints
		{
			get
			{
				return this.strokePoints;
			}
		}

		/// <summary>
		/// Add the specified strokePoint.
		/// </summary>
		/// <param name="strokePoint">Stroke point.</param>
		public void Add(StrokePoint strokePoint)
		{
			this.strokePoints.Add (strokePoint);
			ReduceExtraPoints ();
		}
		/// <summary>
		/// Clear the Stroke.
		/// </summary>
		public void Clear()
		{	
			this.strokePoints.Clear();
		}

        /// <summary>
        /// Serialization
        /// </summary>
        /// <param name="info">Info.</param>
        /// <param name="context">Context.</param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("maximumSamples", maximumSamples, typeof(int));
            info.AddValue("strokePoints", strokePoints, typeof(List<StrokePoint>));
        }

        /// <summary>
        /// Gets the length of the stroke.
        /// </summary>
        /// <returns>The stroke length.</returns>
        private float GetStrokeLength()
        {
            float length = 0.0f;

            for (int i = 0; i < (strokePoints.Count / 2); i++)
            {
                length += Vector3.Distance(strokePoints[i].Position, strokePoints[i + 1].Position);
            }

            length += Vector3.Distance(strokePoints[strokePoints.Count / 2].Position, this.LastPoint.Position);

            return length;
        }

        /// <summary>
        /// reduces stroke count, roughly distributed
        /// Otherwise it hogs requrces, its coarse reduction. Have to make sure that points
        /// stored do not have their positions modified as that could lead to the stroke piercing 
        /// an object and the world point would no longer represent a point on a surface
        /// </summary>
        private void ReduceExtraPoints()
		{
			if (strokePoints.Count > maximumSamples)
			{
				float strokeLength = GetStrokeLength ();
				int reduceTo = this.maximumSamples / 2;
				float stepLength = strokeLength / reduceTo;
				int skip = 0;

				for (int i = skip; i < strokePoints.Count - 2; i++)
				{
					for (int j = skip + 1; j < strokePoints.Count - 2; j++)
					{
						if(strokePoints[i] != null && strokePoints[j] != null)
						{
							float distance = Vector3.Distance (strokePoints[i].Position, strokePoints[j].Position);
					
							if (distance < stepLength)
							{
								strokePoints [j] = null;
							}
							else if(distance >= stepLength)
							{
								skip = j;
								i = j - 1;
								break;
							}
						}
					}
				}
				strokePoints.RemoveAll (item => item == null);
			}
		}
	}
}