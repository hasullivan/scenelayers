﻿// <copyright file="SampleSlopeFilter.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>07/01/2016</date>
// <summary>Slope Filter for Layer Painter</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SceneLayers
{
    /// <summary>
    /// Slope Filter. Slope is from 0 to 180, with 0 being up, and 180 being down
    /// </summary>
    [System.Serializable]
	public class SampleSlopeFilter : ISampleFilter, ISerializable
    {  
        private bool enabled = true;
        private float maximumSlope = 180.0f;
        private float minimumSlope = 0.0f;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="enabled">Should Limit Slope</param>
        /// <param name="minimumSlope">The minimum Slope</param>
        /// <param name="maximumSlope">The Maximum Slope</param>
        public SampleSlopeFilter() { }

        /// <summary>
        /// Serialization Constructor
        /// </summary>
        /// <param name="info">Serialization Info</param>
        /// <param name="context">Streaming Context</param>
        protected SampleSlopeFilter(SerializationInfo info, StreamingContext context)
        {
            this.enabled = (bool)info.GetValue("enabled", typeof(bool));
            this.minimumSlope = (float)info.GetValue("minimumSlope", typeof(float));
            this.maximumSlope = (float)info.GetValue("maximumSlope", typeof(float));
        }

        /// <summary>
        /// Gets or sets Limit Slope, Should slope be a factor in placement.
        /// </summary>
        public bool Enabled
        {
            get
            {
                return this.enabled;
            }

            set
            {
                this.enabled = value;
            }
        }

        /// <summary>
        /// Gets or set the Maximum Slope for the filter
        /// </summary>
        public float MaximumSlope
        {
            get
            {
                return this.maximumSlope;
            }

            set
            {
                if (value >= 0.0f && value <= 180.0f)
                {
                    if (value >= minimumSlope)
                    {
                        this.maximumSlope = value;
                    }
                    else
                    {
                        this.maximumSlope = this.minimumSlope;
                    }
                }
                else if (value < 0.0f)
                {
                    this.maximumSlope = 0.0f;
                }
                else if (value > 180.0f)
                {
                    this.maximumSlope = 180.0f;
                }
            }
        }

        /// <summary>
        /// Gets or sets the Minimum Slope For the Filter
        /// </summary>
        public float MinimumSlope
        {
            get
            {
                return this.minimumSlope;
            }

            set
            {
                if (value >= 0.0f && value <= 180.0f)
                {
                    if (value <= this.maximumSlope)
                    {
                        this.minimumSlope = value;
                    }
                    else
                    {
                        this.minimumSlope = this.maximumSlope;
                    }
                }
                else if(value < 0)
                {
                    this.minimumSlope = 0.0f;
                }
                else if(value > 180.0f)
                {
                    this.minimumSlope = 180.0f;
                }
            }
        }
        /// <summary>
        /// Creates a new Filtered List from provided List
        /// </summary>
        /// <param name="list">List to Filter</param>
        /// <returns>Filterered List</returns>
		public List<RaycastHit> ApplyFilter(List<RaycastHit> list)
		{
			List<RaycastHit> newList = new List<RaycastHit> ();
		
			for (int i = 0; i < list.Count; i++)
			{
				float angle = Vector3.Angle (list[i].normal, Vector3.up);

				if (angle >= minimumSlope && angle <= maximumSlope)
				{
					newList.Add (list[i]);
				}
			}
				
			return newList;
		}

        /// <summary>
        /// Aplly the filter to the provided Ray
        /// </summary>
        /// <param name="ray">RaycastHit</param>
        /// <returns>bool</returns>
		public bool ApplyFilter (RaycastHit ray)
		{
			float angle = Vector3.Angle (ray.normal, Vector3.up);

			if (angle >= minimumSlope && angle <= maximumSlope)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

        /// <summary>
        /// Serialize
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("enabled", enabled, typeof(bool));
            info.AddValue("minimumSlope", minimumSlope, typeof(float));
            info.AddValue("maximumSlope", maximumSlope, typeof(float));
        }

        /// <summary>
        /// On Disable Callback
        /// </summary>
        public void OnDisable()
        {

        }

        /// <summary>
        /// On Draw Gizmos Callback
        /// </summary>
        public void OnDrawGizmos()
        {

        }

        /// <summary>
        /// On Enable Callback
        /// </summary>
        public void OnEnable()
        {

        }

        /// <summary>
        /// On Inspector GUi callback
        /// </summary>
		public void OnInspectorGUI()
		{
			#if UNITY_EDITOR
            EditorGUILayout.BeginVertical();

            EditorGUILayout.LabelField("Slope Filter:");
            
            this.enabled = EditorGUILayout.Toggle("Enabled:", this.enabled);

            this.minimumSlope = EditorGUILayout.DelayedFloatField("Minimum Slope:", this.minimumSlope);
            this.maximumSlope = EditorGUILayout.DelayedFloatField("Maximum Slope:", this.maximumSlope);
 
            EditorGUILayout.MinMaxSlider(ref minimumSlope, ref maximumSlope, 0.0f, 180.0f);

            EditorGUILayout.EndVertical();
			#endif
		}

        /// <summary>
        /// On Scene GUI Callback
        /// </summary>
		public void OnSceneGUI()
		{

		}
    }
}
