﻿// <copyright file="SampleFilterStack.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>07/01/2016</date>
// <summary>Sample Filter Stack for Layer Painter</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using UnityEngine;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace SceneLayers
{

    /// <summary>
    /// Sample Filter stack, perofrms gui functions and stores lsit of filters.
    /// </summary>
    [System.Serializable]
    [KnownType(typeof(SampleSlopeFilter))]
    [KnownType(typeof(SampleObjectFilter))]
	[KnownType(typeof(SampleMinimumProximityFilter))]
	public class SampleFilterStack: ISerializable
	{
        private List<ISampleFilter> sampleFilters = new List<ISampleFilter>();

        /// <summary>
        /// Default Constructor
        /// </summary>
		public SampleFilterStack() { }

        /// <summary>
        /// Serialization COnstructor
        /// </summary>
        /// <param name="info">Serialization Info</param>
        /// <param name="context">Streaming Context</param>
		protected SampleFilterStack(SerializationInfo info, StreamingContext context)
        {
            this.sampleFilters = (List<ISampleFilter>)info.GetValue("sampleFilters", typeof(List<ISampleFilter>));
        }

        /// <summary>
        /// Gets or sets Sample Filters
        /// </summary>
        public List<ISampleFilter> SampleFilters
		{
			get
			{
				return this.sampleFilters;
			}
			set
			{
				this.sampleFilters = value;
			}
		}

        /// <summary>
        /// Apply Fitlers
        /// </summary>
        /// <param name="ray">Ray</param>
        /// <returns></returns>
		public bool ApplyFilter(RaycastHit ray)
        {
            bool result = true;
            foreach (ISampleFilter sampleFilter in this.sampleFilters)
            {
                result = sampleFilter.ApplyFilter(ray);
                if (result == false)
                {
                    return false;
                }
            }
            return result;
        }

        /// <summary>
        /// Serialize
        /// </summary>
        /// <param name="info">Serialization Info</param>
        /// <param name="context">Streaming Context</param>
		public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("sampleFilters", sampleFilters, typeof(List<ISampleFilter>));
        }

        /// <summary>
        /// On Disable Callback
        /// </summary>
		public void OnDisable()
        {
            for (int i = 0; i < this.sampleFilters.Count; i++)
            {
                this.sampleFilters[i].OnDisable();
            }
        }

        /// <summary>
        /// On Draw Gizmos Callback
        /// </summary>
		public void OnDrawGizmos()
        {
            for (int i = 0; i < this.sampleFilters.Count; i++)
            {
                this.sampleFilters[i].OnDrawGizmos();
            }
        }

        /// <summary>
        /// On Enalbe Callback
        /// </summary>
		public void OnEnable()
        {
            for (int i = 0; i < this.sampleFilters.Count; i++)
            {
                this.sampleFilters[i].OnEnable();
            }
        }

        /// <summary>
        /// On Inspector GUI Callback
        /// </summary>
        public void OnInspectorGUI()
		{
			#if UNITY_EDITOR
			for (int i = 0; i < sampleFilters.Count; i++)
			{
                LayerPainterGUI.DrawHorizontalSeparator();

                GUILayout.BeginHorizontal();
                    GUILayout.BeginVertical();

                        if (GUILayout.Button("-", GUILayout.Width(20), GUILayout.Height(20)) == true)
                        {
                            sampleFilters[i] = null;
                        }

                    GUILayout.EndVertical();
                LayerPainterGUI.HorizontalPadding();

                    if (sampleFilters[i] != null)
                    {
                        sampleFilters[i].OnInspectorGUI();
                    }


                GUILayout.EndHorizontal();

                LayerPainterGUI.DrawHorizontalSeparator();
                
			}

			sampleFilters.RemoveAll (item => item == null);
			#endif
		}

        /// <summary>
        /// On Scene GUI Callback
        /// </summary>
		public void OnSceneGUI()
		{
			#if UNITY_EDITOR
			for (int i = 0; i < this.sampleFilters.Count; i++)
			{
				this.sampleFilters [i].OnSceneGUI ();
			}
			#endif
		}
	}
}