﻿// <copyright file="SampleObjectFilter.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>07/01/2016</date>
// <summary>Sample Object Filter for Layer Painter</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SceneLayers
{
	/// <summary>
	/// Sample minimum proximity filter.
	/// </summary>
	[System.Serializable]
	public class SampleMinimumProximityFilter : ISampleFilter, ISerializable
	{
		private bool enabled = true;
		private float minimumProximity = 1.0f;

        /// <summary>
        /// Default Constructor
        /// </summary>
        public SampleMinimumProximityFilter() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SceneLayers.SampleMinimumProximityFilter"/> class.
        /// </summary>
        /// <param name="info">Info.</param>
        /// <param name="context">Context.</param>
        protected SampleMinimumProximityFilter(SerializationInfo info, StreamingContext context)
        {
            this.minimumProximity = (float)info.GetValue("minimumProximity", typeof(float));
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="SceneLayers.SampleMinimumProximityFilter"/> is enabled.
        /// </summary>
        /// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
        public bool Enabled
        {
            get
            {
                return this.Enabled;
            }
            set
            {
                this.Enabled = value;
            }
        }

        /// <summary>
        /// Applies the filter.
        /// </summary>
        /// <returns>The filter.</returns>
        /// <param name="list">List.</param>
        public List<RaycastHit> ApplyFilter (List<RaycastHit> list)
		{
            List<RaycastHit> filteredList = new List<RaycastHit>();

            for(int i = 0; i < list.Count; i++)
            {
                if(this.ApplyFilter(list[i]) == true)
                {
                    filteredList.Add(list[i]);
                }

            }

            return filteredList;
		}

		/// <summary>
		/// Applies the filter.
		/// </summary>
		/// <returns>The filter.</returns>
		/// <param name="ray">Ray.</param>
		public bool ApplyFilter (RaycastHit ray)
		{
			// TODO Need to find a more efficient way to do this. Can not use colliders as
			// the objects being painted might not have a collider
			// Possible Solution...
			// Create a cache in the brush that stores all scene renderers when a stroke starts
			// Then in the filter compare to the cache, then compair to all
			// transforms in the brushes parent. This will reduce the need to get all renderers 
			// for every ray. It will just have to get the components in the parent every ray
			// as the user can not etid the scene and brush at the same time this should be "safe"

			bool result = true;
			if (ray.transform != null)
			{
				List<Transform> sceneTransforms = LayerPainterUtility.GetSceneTransforms();

				// Go through the Cache First
				for (int i = 0; i < sceneTransforms.Count; i++)
				{
					if (sceneTransforms [i] != ray.transform)
					{
						float distance = Vector3.Distance (ray.point, sceneTransforms [i].position);

						if (distance <= minimumProximity)
						{
							
							result = false;
							break;
						}
					}
				}
			}
			else
			{
				return false;
			}
			return result;
		}

        /// <summary>
        /// Serialize
        /// </summary>
        /// <param name="info">Info.</param>
        /// <param name="context">Context.</param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("minimumProximity", minimumProximity, typeof(float));
        }

        /// <summary>
        /// On Disable Callback
        /// </summary>
        public void OnDisable()
        {

        }

        /// <summary>
        /// On Draw Gizmos Callback
        /// </summary>
        public void OnDrawGizmos()
        {

        }

        /// <summary>
        /// On Enable Callback
        /// </summary>
        public void OnEnable()
        {

        }

        /// <summary>
        /// On Inspector GUI Callback
        /// </summary>
        public void OnInspectorGUI ()
		{
			#if UNITY_EDITOR
			EditorGUILayout.BeginVertical();

			EditorGUILayout.LabelField("Minimum Proximity Filter:");

			this.enabled = EditorGUILayout.Toggle("Enabled:", this.enabled);

			EditorGUILayout.LabelField ("Minimum Proximity:");

			this.minimumProximity = EditorGUILayout.Slider (this.minimumProximity, 0.0f, 100.0f);

			EditorGUILayout.EndVertical();
			#endif
		}

		/// <summary>
		/// On Scene GUI Callback
		/// </summary>
		public void OnSceneGUI ()
		{
			
		}
	}
}

