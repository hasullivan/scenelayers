﻿// <copyright file="SampleRules.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>07/01/2016</date>
// <summary>A Rule is a set of filters</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SceneLayers
{
    /// <summary>
    /// Structure for placement rules for layer painter brush
    /// </summary>
    [System.Serializable]
    [KnownType(typeof(SampleFilterStack))]
	public class SampleRules : ISampleRule, ISerializable
    {
      
        private bool enabled = false;


        private GameObject objectFilterTemp;
        private SampleFilterStack sampleFilterStack = new SampleFilterStack();


        private bool showFilterSelection = false;
		public SampleRules()
		{
		}

        protected SampleRules(SerializationInfo info, StreamingContext context)
        {
            this.enabled = (bool)info.GetValue("enabled", typeof(bool));
            this.sampleFilterStack = (SampleFilterStack)info.GetValue("sampleFilterStack", typeof(SampleFilterStack));
        }

        /// <summary>
        /// 
        /// </summary>
		public bool Enabled
        {
            get
            {
                return this.enabled;
            }
            set
            {
                this.enabled = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
		public SampleFilterStack SampleFilterStack
		{
			get
			{
				return this.sampleFilterStack;
			}
			set 
			{
				this.sampleFilterStack = value;
			}
		}
        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public List<RaycastHit> ApplyFilters(List<RaycastHit> list)
        {
            List<RaycastHit> newList = new List<RaycastHit>();




            return newList;
        }

        public bool ApplyFilters(RaycastHit ray)
        {
            return this.sampleFilterStack.ApplyFilter(ray);
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("enabled", enabled, typeof(bool));
            info.AddValue("sampleFilterStack", sampleFilterStack, typeof(SampleFilterStack));
        }

        public void OnDisable()
        {
            this.sampleFilterStack.OnDisable();
        }

        public void OnDrawGizmos()
        {
            this.sampleFilterStack.OnDrawGizmos();
        }

        public void OnEnable()
        {
			this.sampleFilterStack.OnEnable ();
		}
        /// <summary>
        /// 
        /// </summary>
		public void OnInspectorGUI()
		{
			#if UNITY_EDITOR
            EditorGUILayout.LabelField("Sampling Rules:");
            EditorGUILayout.BeginVertical();

            this.sampleFilterStack.OnInspectorGUI();


            EditorGUILayout.EndVertical();
            

            if (GUILayout.Button("+", GUILayout.Width(20.0f)) == true)
            {
                if (showFilterSelection == false)
                {
                    this.showFilterSelection = true;
                }
                else
                {
                    this.showFilterSelection = false;
                }
            }
 
            EditorGUILayout.Separator();

            if(showFilterSelection == true)
            {
                this.DrawFilterSelection();
            }
			#endif
		}

		public void OnSceneGUI()
		{
			#if UNITY_EDITOR
			this.sampleFilterStack.OnSceneGUI ();
			#endif
		}
        /// <summary>
        /// 
        /// </summary>
        private void DrawFilterSelection()
        {
			#if UNITY_EDITOR
            EditorGUILayout.BeginHorizontal();

            this.objectFilterTemp = (GameObject)EditorGUILayout.ObjectField("Host object to add", this.objectFilterTemp, typeof(GameObject), true, GUILayout.ExpandWidth(true));

            if (GUILayout.Button("Add Object Fitler"))
            {
                if(this.objectFilterTemp != null)
                {
                    this.sampleFilterStack.SampleFilters.Add(new SampleObjectFilter(this.objectFilterTemp));
                    this.showFilterSelection = false;
                }
                this.objectFilterTemp = null;
            }
           
            EditorGUILayout.EndHorizontal();

            if (GUILayout.Button("Slope Fitler"))
            {
                this.sampleFilterStack.SampleFilters.Add(new SampleSlopeFilter());
                this.showFilterSelection = false;
            }       

			if (GUILayout.Button ("MinimumProximityFilter"))
			{
				this.sampleFilterStack.SampleFilters.Add (new SampleMinimumProximityFilter ());
				this.showFilterSelection = false;
			}
			#endif
        }
    }
}
