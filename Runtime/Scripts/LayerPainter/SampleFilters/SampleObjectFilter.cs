﻿// <copyright file="SampleObjectFilter.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>07/01/2016</date>
// <summary>Sample Object Filter for Layer Painter</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace SceneLayers
{
    /// <summary>
    /// Filters RaycastHit list by the objects the rays intersected with
    /// </summary>
    [System.Serializable]
	public class SampleObjectFilter : ISampleFilter, ISerializable
    {
        private bool enabled = true;
        private string filterObjectTransformName;
        private InclusionMode inclusionMode = InclusionMode.Excluded;
		#if UNITY_EDITOR
        private Texture2D thumbnail;
		private string thumbnailURL = string.Empty;
		#endif

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="enabled"></param>
        /// <param name="gameObjects"></param>
        public SampleObjectFilter(GameObject filterObject)
        {
			this.filterObjectTransformName = filterObject.transform.name;

			#if UNITY_EDITOR
            thumbnail = LayerPainterUtility.GetThumbnail(filterObject);

			if (thumbnail == null)
			{
				thumbnail = (Texture2D)AssetDatabase.LoadAssetAtPath (@"Assets/SceneLayers/Editor/InternalResources/NoPreviewAvailable.tif", typeof(Texture2D));
			} 

			thumbnailURL = AssetDatabase.GetAssetPath (thumbnail.GetInstanceID ());
			#endif
        }

        /// <summary>
        /// Serialization Constructor
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected SampleObjectFilter(SerializationInfo info, StreamingContext context)
        {
            this.enabled = (bool)info.GetValue("enabled", typeof(bool));
            this.inclusionMode = (InclusionMode)info.GetValue("inclusionMode", typeof(InclusionMode));
			this.filterObjectTransformName = (string)info.GetValue("filterObjectTransformName", typeof(string));
			#if UNITY_EDITOR
            this.thumbnailURL = (string)info.GetValue("thumbnailURL", typeof(string));
            this.thumbnail = (Texture2D)AssetDatabase.LoadAssetAtPath(thumbnailURL, typeof(Texture2D));
			#endif
            
        }

        /// <summary>
        /// Gets or sets enabled
        /// </summary>
        public bool Enabled
        {
            get
            {
                return this.enabled;
            }

            set
            {
                this.enabled = value;
            }
        }

        /// <summary>
        /// Gets or sets Inclusion Mode
        /// </summary>
        public InclusionMode InclusionMode
        {
            get
            {
                return inclusionMode;
            }

            set
            {
                inclusionMode = value;
            }
        }

        // Logic is all broken
        // Switch this to use other apply fitler as base
        /// <summary>
        /// Apply filters to list
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public List<RaycastHit> ApplyFilter(List<RaycastHit> list)
		{
			List<RaycastHit> newList = new List<RaycastHit> ();

			switch (InclusionMode)
			{
				case InclusionMode.Permitted:
					
					for (int i = 0; i < list.Count; i++)
					{
						if (list[i].transform != null)
						{
							if (filterObjectTransformName == list [i].transform.name)
							{
								newList.Add (list [i]);

							}	
						}
					}
					break;

				case InclusionMode.Excluded:

                    for (int i = 0; i < list.Count; i++)
                    {
						if (filterObjectTransformName != list[i].transform.name)
                        {
                            newList.Add(list[i]);
                            
                        }
                    }
                    break;
			}
				
			return newList;
		}

        /// <summary>
        /// Apply Filter
        /// </summary>
        /// <param name="ray">ray</param>
        /// <returns>bool</returns>
		public bool ApplyFilter (RaycastHit ray)
		{
			List<Transform> transforms = LayerPainterUtility.GetCompoundObjectsTransforms (ray.transform);

			//bool result = false;

			switch (InclusionMode)
			{
				case InclusionMode.Permitted:

					if (ray.transform == null)
					{
						return false;
					}
					else
					{
						foreach (Transform transform in transforms)
						{
							if (transform.name == filterObjectTransformName)
							{
								return true;
							}
						}
						return false;
					}
					break;

				case InclusionMode.Excluded:
					if (ray.transform == null)
					{
						return false;
					}
					else
					{
						
						foreach (Transform transform in transforms)
						{
							//Debug.Log (transform.name);
							if (transform.name == filterObjectTransformName)
							{
								return false;
							}
						}
						return true;
					}
					break;

				default:
					return true;

			}
			//return result;
		}

        /// <summary>
        /// Serialization
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("enabled", enabled, typeof(bool));
            info.AddValue("inclusionMode", inclusionMode, typeof(InclusionMode));
			info.AddValue("filterObjectTransformName", filterObjectTransformName, typeof(string));
			#if UNITY_EDITOR
            info.AddValue("thumbnailURL", thumbnailURL, typeof(string));
			#endif
            
        }

        /// <summary>
        /// On Disable Callback
        /// </summary>
        public void OnDisable()
        {

        }

        /// <summary>
        /// On Draw Gizmos Callback
        /// </summary>
        public void OnDrawGizmos()
        {

        }

        /// <summary>
        /// On Enable Callback
        /// </summary>
        public void OnEnable()
        {

        }

        /// <summary>
        /// On Inspector GUI Callback
        /// </summary>
		public void OnInspectorGUI()
		{
			#if UNITY_EDITOR
            GUILayout.BeginVertical();

			if (filterObjectTransformName != null)
			{
				GUILayout.Label ("Object Filter: " + filterObjectTransformName);
			}
			else
			{
				GUILayout.Label ("Object Filter: " + "Error Game Object is null");
			}

            GUILayout.BeginHorizontal();

            GUILayout.BeginVertical();

			if (this.thumbnail != null)
			{
				GUILayout.Box (thumbnail, GUILayout.Width (72), GUILayout.Height (72));
			}
			else
			{
				if (filterObjectTransformName != null)
				{
					GUILayout.Box (filterObjectTransformName, GUILayout.Width (72), GUILayout.Height (72));
				}
				else
				{
					GUILayout.Box ("Error Game Object is null", GUILayout.Width (72), GUILayout.Height (72));
				}
			}

            GUILayout.EndVertical();

            GUILayout.BeginVertical();

                //GUILayout.Space(20);
                this.enabled = EditorGUILayout.Toggle("Enabled:", this.enabled);
                this.InclusionMode = (InclusionMode)EditorGUILayout.EnumPopup("Inclusion Mode:", this.InclusionMode);

            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
			#endif
        }
    
        /// <summary>
        /// On Scene GUI Callback
        /// </summary>
		public void OnSceneGUI()
		{

		}
	}
}
