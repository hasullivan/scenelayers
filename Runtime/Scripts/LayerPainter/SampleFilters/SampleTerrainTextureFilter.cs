﻿// <copyright file="SampleTerrainTextureFilter.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>07/01/2016</date>
// <summary>Terrain Texture Filter for Layer Painter</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using UnityEngine;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SceneLayers
{

    /// <summary>
    /// 
    /// </summary>
    [System.Serializable]
	public class SampleTerrainTextureFilter : ISampleFilter, ISerializable
    {   
		private bool enabled;
        private InclusionMode inclusionMode = InclusionMode.Permitted;
        private Texture2D texture = new Texture2D(0, 0);
        private float threshold = 1.0f;

        /// <summary>
        /// Default Constructor
        /// </summary>
		public SampleTerrainTextureFilter() { }

        /// <summary>
        /// Serialization Constructor
        /// </summary>
        /// <param name="info">Serialization Info</param>
        /// <param name="context">Streaming Context</param>
        protected SampleTerrainTextureFilter(SerializationInfo info, StreamingContext context)
        {
            this.enabled = (bool)info.GetValue("enabled", typeof(bool));
            this.inclusionMode = (InclusionMode)info.GetValue("inclusionMode", typeof(InclusionMode));
            this.texture = (Texture2D)info.GetValue("texture", typeof(Texture2D));
            this.threshold = (float)info.GetValue("threashold", typeof(float));
        }

        /// <summary>
        /// Get or set enabled
        /// </summary>
        public bool Enabled
		{
			get
			{
				return this.enabled;
			}
			set
			{
				this.enabled = value;
			}
		}

        /// <summary>
        /// Gets or sets the inclusion mode.
        /// </summary>
        /// <value>The inclusion mode.</value>
        public InclusionMode InclusionMode
        {
            get
            {
                return this.inclusionMode;
            }
            set
            {
                this.inclusionMode = value;
            }
        }

        /// <summary>
        /// Gets or sets the texture.
        /// </summary>
        /// <value>The texture.</value>
        public Texture2D Texture
		{
			get
			{
				return this.texture;
			}
			set
			{
				this.texture = value;
			}
		}

        /// <summary>
        /// Gets or sets the Threshold
        /// </summary>
        public float Threshold
        {
            get
            {
                return this.threshold;
            }
            set
            {
                this.threshold = value;
            }
        }

        /// <summary>
        /// Apply Filter to list
        /// </summary>
        /// <param name="list">List or Raycasthits</param>
        /// <returns></returns>
		public List<RaycastHit> ApplyFilter(List<RaycastHit> list)
		{
			List<RaycastHit> newList = new List<RaycastHit> ();


			return newList;
		}

        /// <summary>
        /// Apply Fitler
        /// </summary>
        /// <param name="ray">Raycasthits</param>
        /// <returns></returns>
		public bool ApplyFilter (RaycastHit ray)
		{
			throw new NotImplementedException ();
		}

        /// <summary>
        /// Serialize
        /// </summary>
        /// <param name="info">Serializaion Info</param>
        /// <param name="context">Streaming Context</param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("enabled", enabled, typeof(bool));
            info.AddValue("inclusionMode", inclusionMode, typeof(InclusionMode));
            info.AddValue("texture", texture, typeof(Texture2D));
            info.AddValue("threshold", threshold, typeof(float));
        }

        /// <summary>
        /// On Disable Callback
        /// </summary>
        public void OnDisable()
        {

        }

        /// <summary>
        /// On Draw Gizmos Callback
        /// </summary>
        public void OnDrawGizmos()
        {

        }

        /// <summary>
        /// On Enable Callback
        /// </summary>
        public void OnEnable()
        {

        }

        /// <summary>
        /// On Inspector GUI Callback
        /// </summary>
		public void OnInspectorGUI()
		{

		}

        /// <summary>
        /// On Scene GUI Callback
        /// </summary>
		public void OnSceneGUI()
		{

		}
    }
}
