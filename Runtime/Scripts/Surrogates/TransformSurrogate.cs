﻿// <copyright file="TransformSurrogate.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>07/01/2016</date>
// <summary>Serialization Surrogate for Transform</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using UnityEngine;
using System.Runtime.Serialization;

namespace SceneLayers
{
    /// <summary>
    /// Serialization Surrogate for Transform
    /// </summary>
    [System.Serializable]
    class TransformSurrogate : ISerializationSurrogate
    {
        /// <summary>
        /// Serialize
        /// </summary>
        /// <param name="obj">Object</param>
        /// <param name="info">Serialization Info</param>
        /// <param name="context">Streaming Context</param>
        public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
        {
            Transform transform = (Transform)obj;
            info.AddValue("position", transform.position);
            info.AddValue("rotation", transform.rotation);

            info.AddValue("localPosition", transform.localPosition);
            info.AddValue("localRotation", transform.localRotation);
            info.AddValue("localScale", transform.localScale);

            info.AddValue("parent", transform.parent);
        }

        /// <summary>
        /// Deserialize
        /// </summary>
        /// <param name="obj">Object</param>
        /// <param name="info">Serialization Info</param>
        /// <param name="context">Streaming Context</param>
        /// <param name="selector">Surroagte Selector</param>
        /// <returns></returns>
        public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
        {
            Transform transform = (Transform)obj;

            transform.position = (Vector3)info.GetValue("position", typeof(Vector3));
            transform.rotation = (Quaternion)info.GetValue("rotation", typeof(Quaternion));

            transform.localPosition = (Vector3)info.GetValue("localPosition", typeof(Vector3));
            transform.localRotation = (Quaternion)info.GetValue("localRotation", typeof(Quaternion));
            transform.localScale = (Vector3)info.GetValue("localScale", typeof(Vector3));

            transform.parent = (Transform)info.GetValue("parent", typeof(Transform));

            obj = transform;
            return obj;
        }

    }
}
