﻿// <copyright file="MaxDrawDistance.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>07/06/2016  </date>
// <summary>Layer Painter</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using UnityEngine;

namespace SceneLayers
{
    /// <summary>
    /// This is a Quick testing script, for an old PC. DO not use. When implimented it will be a system that manages draw distances
    /// for detail objects. It is too heavy to run this on every instance.
    /// </summary>
    public class MaxDrawDistance : MonoBehaviour
    {
        public float drawDistance = 25.0f;
        Renderer[] renderers = new Renderer[0];

        /// <summary>
        /// Start Callback
        /// </summary>
        void Start()
        {
            renderers = this.transform.GetComponentsInChildren<Renderer>();
            InvokeRepeating("CheckDistance", .001f, Random.Range(1.0f, 5.0f));
        }
 
        /// <summary>
        /// Checks the distance between Main Camera and Transform, toggles renderers
        /// </summary>
        private void CheckDistance()
        {
            //Get the main Camera
            Camera camera = Camera.main;

            if(Vector3.Distance(camera.transform.position, this.transform.position) > drawDistance)
            {
                for(int i = 0; i < renderers.Length; i++)
                {
                    renderers[i].enabled = false;
                }
            }
            else
            {
                for (int i = 0; i < renderers.Length; i++)
                {
                    renderers[i].enabled = true;
                }
            }

        }
    }
}
