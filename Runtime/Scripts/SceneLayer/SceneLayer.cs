﻿// <copyright file="SceneLayer.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>04/11/2016  </date>
// <summary>Scene Layer</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Scene layer Component, apply organizational logic to an object and its children
/// </summary>
namespace SceneLayers {

	// Add Ability to Create Asset Bundle from layer
	// Add ability to save out the layer (json/ XML/ Binary ?)
    // Add is Ability to bulk set Unity Layer or tag to children
    // Add in ability to convert to detail patch (once implimented)
    /// <summary>
    /// Scene Layer is a faily basic script that aids in scene hierarchy organization.
    /// </summary>
    [ExecuteInEditMode]
    [System.Serializable]
    public class SceneLayer : MonoBehaviour {

        [SerializeField]
        private bool frozen = false;
        [SerializeField]
        private bool hidden = false;
        [SerializeField]
        private bool layerIsIsolated = false;
        [SerializeField]
        private Renderer[] childVisibility;
        [SerializeField]
        private List<Renderer> childVisibilityHidden = new List<Renderer>();
        [SerializeField]
        private List<Renderer> sceneRenderers = new List<Renderer>();
        [SerializeField]
        private List<Renderer> sceneRenderersHidden = new List<Renderer>();
        [SerializeField]
        private Transform[] childEditable;
        [SerializeField]
        public string description = "";
        [SerializeField]
        public string notes = "";
        [SerializeField]
        public Color tintColor;
        [SerializeField]
        private bool colorCreated = false;
        [SerializeField]
        private bool isPersistant = false;

        /// <summary>
        /// Description of the Layer, for example, "Vegitation" or "Last house on the left". Shows up on the layer in the hierarcy as a tool tip.
        /// </summary>
        public string Description
        {
            get
            {
                return description;
            }

            set
            {
                description = value;
            }
        }

        /// <summary>
        /// Any Notes about this layer
        /// </summary>
        public string Notes
        {
            get
            {
                return notes;
            }

            set
            {
                notes = value;
            }
        }

        /// <summary>
        /// The color that is displayed as a bar in the hierarchy window
        /// </summary>
        public Color TintColor
        {
            get
            {
                return tintColor;
            }

            set
            {
                tintColor = value;
            }
        }

        /// <summary>
        /// Gets The frozen property, if frozen the children can not be moved
        /// </summary>
        public bool Frozen
        {
            get
            {
                return frozen;
            }

            set
            {
                frozen = value;
            }
        }

        /// <summary>
        /// Gets the Hidden Propery, if hidden the renderers for child objects are turned off, but no other components
        /// </summary>
        public bool Hidden
        {
            get
            {
                return this.hidden;
            }

            set
            {
                this.hidden = value;
            }
        }

        /// <summary>
        /// Gets the Layer Isolation property, if the Layer is Isolated then all non children objects will have their renderers turned off
        /// </summary>
        public bool LayerIsIsolated
        {
            get
            {
                return layerIsIsolated;
            }
        }

        /// <summary>
        /// Gets Is persistant. If the Layer is persistance, it and its children are marked Do Not Destray On Load
        /// </summary>
        public bool IsPersistant
        {
            get
            {
                return isPersistant;
            }
        }

        /// <summary>
        /// Unity Engine Awake Callback
        /// </summary>
        void Awake()
        {
	    	if(IsPersistant == true)
            {
	    		DontDestroyOnLoad(this);
	    	}

	    	float rVal = Random.Range(0.1f,0.6f);
	    	float gVal = Random.Range(0.2f,1.0f);
	    	float bVal = Random.Range(0.4f,0.7f);

	    	if(colorCreated == false)
            {
	    	    TintColor = new Color(rVal,gVal,bVal);
	    	    colorCreated = true;		
	    	}
	}

        /// <summary>
        /// Freeze this Layers children
        /// </summary>
	    public void FreezeLayer(){
            //Undo.RegisterSceneUndo("Freeze Scene Layer");
            this.frozen = !this.frozen;
		    childEditable = gameObject.GetComponentsInChildren<Transform>();
            foreach (Transform child in childEditable)
            {
		 	    if(child.gameObject != transform.gameObject)
                {
      			     child.gameObject.hideFlags = child.hideFlags ^ HideFlags.NotEditable;
			    }
		    }	
	    }
	
        /// <summary>
        /// Unfreeze this layers children
        /// </summary>
	    public void UnfreezeLayer(){
            //Undo.RegisterSceneUndo("Unfreeze Scene Layer");
            this.frozen = !this.frozen;
            //childEditable = gameObject.GetComponentsInChildren<Transform>();
            foreach (Transform child in childEditable)
            {
			    if(child.gameObject != transform.gameObject)
                {
       			    child.gameObject.hideFlags = child.hideFlags ^ HideFlags.NotEditable;
			    }
		}		
	}
	
        /// <summary>
        /// Hide this layers children
        /// </summary>
	    public void HideLayer(){
            //Undo.RegisterSceneUndo("Hide Scene Layer");
            this.hidden = true;
		    childVisibility = gameObject.GetComponentsInChildren<Renderer>();

		    if(childVisibilityHidden != null)
            {
			    childVisibilityHidden.Clear();
		    }

       	    foreach (Renderer renderer in childVisibility)
            {
			    if(renderer.enabled == true)
                {		
				    childVisibilityHidden.Add(renderer);
                    renderer.enabled = false;
			    }
		    }	
	    }
	
        /// <summary>
        /// Unhide this layers children
        /// </summary>
	    public void UnhideLayer()
        {
            //Debug.Log("Unhide Called");
            this.hidden = false;

            foreach (Renderer renderer in childVisibilityHidden)
            {
       		    renderer.enabled = true;
		    }

		    childVisibilityHidden.Clear();
	    }
	
        /// <summary>
        /// Isolate the Children of this layer
        /// </summary>
	    public void IsolateLayer(){

		    sceneRenderers = GetSceneRenderers();
            layerIsIsolated = true;

		    if(sceneRenderersHidden != null)
            {
			    sceneRenderersHidden.Clear();
		    }

        	foreach (Renderer sceneRenderer in sceneRenderers)
            {
			    if(sceneRenderer.enabled == true)
                {			
				    sceneRenderersHidden.Add(sceneRenderer);
       			    sceneRenderer.enabled = false;
			    }
		    }
	    }
		
        /// <summary>
        /// Unisolate the children of this layer
        /// </summary>
	    public void UnisolateLayer()
        {
            layerIsIsolated = false;
		    foreach (Renderer renderer in sceneRenderersHidden)
            {	
       		    renderer.enabled = true;	
		    }

		    sceneRenderersHidden.Clear();
	    }

        /// <summary>
        /// Make this layer and its children persistant across scene loads, set Do Not Destroy On Load
        /// </summary>
        public void MakePersistant()
        {
		    isPersistant = true;	
	    }

        /// <summary>
        /// Remove Persistants for this layer and its children
        /// </summary>
        public void RemovePersistance()
        {
		    isPersistant = false;		
	    }

        /// <summary>
        /// Gets a List of every renderer in the current scene. This is used for the Isolation Option
        /// </summary>
        /// <returns></returns>
        private List<Renderer> GetSceneRenderers()
        {
            List<Renderer> isolatedRenderers = new List<Renderer>();
            Renderer[] childRenderers;
            Renderer[] allSceneRenderers;
            allSceneRenderers = (Renderer[])GameObject.FindObjectsOfType(typeof(Renderer));
            childRenderers = gameObject.GetComponentsInChildren<Renderer>();
            foreach (Renderer scene in allSceneRenderers)
            {
                bool same = false;
                foreach (Renderer child in childRenderers)
                {
                    if (scene == child)
                    {
                        same = true;
                    }
                }
                if (same == false)
                {
                    isolatedRenderers.Add(scene);
                }
            }
            return isolatedRenderers;
        }
    }	
}
	
	





