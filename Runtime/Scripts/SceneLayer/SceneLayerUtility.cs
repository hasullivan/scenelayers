﻿// <copyright file="SceneLayerUtility.cs" company="H.A. Sullivan">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>H.A. Sullivan</author>
// <date>07/01/2016</date>
// <summary>Scene Layer Utility Methods</summary>
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections.Generic;
using System.Collections;
using System.Reflection;

// I no longer recall why this was done
using GameObject = UnityEngine.GameObject;
using Transform = UnityEngine.Transform;
using Object = UnityEngine.Object;
using Texture2D = UnityEngine.Texture2D;

namespace SceneLayers{
	
    /// <summary>
    /// Utiltiy class for scene layers
    /// </summary>
    public static class SceneLayerUtility {
		
        /// <summary>
        /// Gets all Scene Layers
        /// </summary>
        /// <returns></returns>
	    public static List<SceneLayer> GetSceneLayers()
        {
            return new List<SceneLayer>((SceneLayer[])Resources.FindObjectsOfTypeAll(typeof(SceneLayer)));
	    }
	    
        /// <summary>
        /// Creates a Game Object with Scene Layer Component attached
        /// </summary>
        /// <param name="name">Name of new Scene Layer</param>
        /// <returns></returns>
	    public static GameObject CreateLayerObject(string name)
        {            
		    GameObject newSceneLayer = new GameObject( name);
			#if UNITY_EDITOR
            Undo.RegisterCreatedObjectUndo(newSceneLayer, "Created Scene Layer");
			#endif

            newSceneLayer.AddComponent<SceneLayer>();

			#if UNITY_EDITOR
		    if(Selection.gameObjects.Length > 0)
            {
			    for(int i = 0; i < Selection.gameObjects.Length; i++)
                {
				    Selection.gameObjects[i].transform.parent = newSceneLayer.transform;				
			    }
		    }
			#endif

		    return newSceneLayer;	
	    }
	
        /// <summary>
        /// Sets the tag property for every child transform
        /// </summary>
        /// <param name="children">Child transforms</param>
        /// <param name="newTag">Name to set tag to</param>
	    public static void SetTags(Transform[] children, string newTag)
        {	
            foreach(Transform child in children)
            {
			    child.gameObject.tag = newTag;
		    }
        }

		#if UNITY_EDITOR
        /// <summary>
        /// Log Window Types
        /// </summary>
		public static void DumpAllWindows()
		{
			
			EditorWindow[] windows = (EditorWindow[])Resources.FindObjectsOfTypeAll(typeof(EditorWindow));

			foreach(EditorWindow window in windows)
			{
				Debug.Log (window.GetType());
			}
		}
		#endif
		// TODO Move this out
		#if UNITY_EDITOR
        /// <summary>
        /// Dump info on objects the the debug console
        /// </summary>
        /// <param name="objects">List of objects to dump info</param>
	    public static void DumpInfo(Object[] objects)
        {
		    const BindingFlags flags = BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static;

		    foreach(Object myObject in objects)
            {
			    if(myObject.name.Contains("Scene"))
                {		
			 	    PropertyInfo[] propertyInfo = myObject.GetType().GetProperties(flags);

				    foreach(PropertyInfo info in propertyInfo)
                    {
					    Debug.Log("name " + myObject.name + ", Property : " + info.Name);
				    }		
			    }
		    }
	    }
		#endif
		#if UNITY_EDITOR
        /// <summary>
        /// Gets a list of all scene layers selected in the editor
        /// </summary>
        /// <returns></returns>
	    public static List<GameObject> GetSelectedLayers()
        {
		    List<GameObject> selectedLayers = new List<GameObject>();

		    if(Selection.gameObjects.Length > 0)
            {
			    foreach(GameObject selection in Selection.gameObjects)
                {	
				    if(selection.GetComponent<SceneLayer>())
                    {
					    selectedLayers.Add(selection);
				    }			
			    }
		    }		

		    return selectedLayers;
	    }
		#endif

        /// <summary>
        /// Merges the selected scene layers
        /// </summary>
	    public static void MergeSelectedLayers()
        {	
		    List<Transform> combinedChildren = new List<Transform>();
			#if UNITY_EDITOR
		    List<GameObject> selectedSceneLayers = SceneLayerUtility.GetSelectedLayers();	
			#else
			List<GameObject> selectedSceneLayers = new List<GameObject>();
			#endif

		    if(selectedSceneLayers.Count > 1)
            {
		    	foreach(GameObject selection in selectedSceneLayers)
                {
		    		foreach(Transform child in selection.transform)
                    {
		    				combinedChildren.Add(child);
		    		}
		    	}

		    	GameObject newSceneLayer = new GameObject(selectedSceneLayers[0].name);
		    	newSceneLayer.AddComponent<SceneLayer>();

		    	foreach(Transform child in combinedChildren)
                {
		    		child.parent = newSceneLayer.transform;
		    	}

		    	foreach(GameObject old in selectedSceneLayers)
                {
		    		Object.DestroyImmediate(old);
		    	}
		    }
		    else
		    {
				#if UNITY_EDITOR
		    	EditorGUI.HelpBox(new Rect(Screen.width / 2, Screen.height / 2, 200, 100),"You must have two or more Scene Layers selected to merge Scene Layers.", MessageType.Error);
		    	EditorUtility.DisplayDialog("Merge Failed","More than one Scene Layer must be selected.", "Ok");
		    	#endif
			}		    	
		}
	    

        /// <summary>
        /// Gets a list of all renderers in the scene that are not a child of gameObject
        /// </summary>
        /// <param name="gameObject">Game object whos chold renderers you wish to exclude from the list</param>
        /// <returns>List of Renderers</returns>
	    public static List<Renderer> GetOtherSceneRenderers(GameObject gameObject)
        {
		    List<Renderer> isolatedRenderers = new List<Renderer>();		
		    Renderer[] childRenderers;  
		    Renderer[] allSceneRenderers;
		
		    allSceneRenderers = (Renderer[])GameObject.FindObjectsOfType(typeof (Renderer));
		    childRenderers =  gameObject.GetComponentsInChildren<Renderer>();
			
		    foreach(Renderer scene in allSceneRenderers)
            {
			    bool same = false;

			    foreach(Renderer child in childRenderers)
			    {
				    if(scene == child)
				    {
					    same = true;
				    }
			    }

			    if(same == false)
			    {
				    isolatedRenderers.Add(scene);		
			    }
		    }	
            		
			return isolatedRenderers;
	    }
	    
        /// <summary>
        /// Freezes a scene layer and its contens (locks from editing)
        /// </summary>
        /// <param name="sceneLayer">Scene Layer to lock</param>
	    public static void FreezeLayer(GameObject sceneLayer)
	    {
		    //Undo.RegisterSceneUndo("Freeze Scene Layer");
		    Transform[] childEditable = sceneLayer.GetComponentsInChildren<Transform>();
			
       	    foreach (Transform transform in childEditable)
		    {
		 	    if(transform.gameObject != sceneLayer)
			    {
       			    transform.gameObject.hideFlags = transform.hideFlags ^ HideFlags.NotEditable;
			    }
		    }
		
	    }
	
        /// <summary>
        /// Unfreeze a scene layer
        /// </summary>
        /// <param name="sceneLayer">Scene Layer to unfreeze</param>
	    public static void UnfreezeLayer(GameObject sceneLayer)
	    {
		    //Undo.RegisterSceneUndo("Unfreeze Scene Layer");
		    Transform[] childEditable = sceneLayer.GetComponentsInChildren<Transform>();
			
       	    foreach (Transform transform in childEditable)
		    {
			    if(transform.gameObject != sceneLayer)
			    {
				    transform.gameObject.hideFlags = transform.hideFlags ^ HideFlags.NotEditable;
			    }
		    }	
	    }	
		
        /// <summary>
        /// Turns off rendering on a scene layer
        /// </summary>
        /// <param name="sceneLayer">Scene layer to Hide</param>
        /// <returns>List of renderers turned off</returns>
	    public static List<Renderer> HideLayer(GameObject sceneLayer)
	    {	
		    //Undo.RegisterSceneUndo("Hide Scene Layer");
		    Renderer[] childVisibility = sceneLayer.GetComponentsInChildren<Renderer>();
		    List<Renderer> childVisibilityHidden = new List<Renderer>();
		
        	foreach (Renderer renderer in childVisibility)
		    {
			    if(renderer.enabled == true)
			    {		
				    childVisibilityHidden.Add(renderer);
      			    renderer.enabled = false;
			    }
		    }
            			
		    return childVisibilityHidden;
	    }
		
        /// <summary>
        /// Turns the hidden renderers back on
        /// </summary>
        /// <param name="childVisibilityHidden">List of renderers that were hidden</param>
	    public static void UnhideLayer(List<Renderer> childVisibilityHidden)
	    {		
		    //Undo.RegisterSceneUndo("Unhide Scene Layer");
       	    foreach (Renderer renderer in childVisibilityHidden)
		    {   
			    renderer.enabled = true;
		    }

		    childVisibilityHidden.Clear();
	    }
	
        /// <summary>
        /// Turns off all renderers not in this scene layer
        /// </summary>
        /// <param name="sceneLayer">Scene layer to keep visible</param>
        /// <param name="sceneRenderersHidden">Renderers</param>
        /// <returns></returns>
	    public static List<Renderer> IsolateLayer(GameObject sceneLayer)
	    {
		    //Undo.RegisterSceneUndo("Isolate Scene Layer");
		    List<Renderer> sceneRenderers = GetOtherSceneRenderers(sceneLayer);
		    List<Renderer> hiddenRenderers = new List<Renderer>();
	
       	    foreach (Renderer sceneRenderer in sceneRenderers)
		    {
			    if(sceneRenderer.enabled == true)
			    {
				    hiddenRenderers.Add(sceneRenderer);
       			    sceneRenderer.enabled = false;
			    }
		    }
		    return hiddenRenderers;	
	    }
		
        /// <summary>
        /// Turns back on all the renderers that were previously hidden by isolation
        /// </summary>
        /// <param name="sceneRenderersHidden"></param>
        /// <returns></returns>
	    public static List<Renderer> UnisolateLayer(List<Renderer> sceneRenderersHidden)
	    {
		    //Undo.RegisterSceneUndo("Unisolate Scene Layer");
		    List<Renderer> hiddenRenderers = new List<Renderer>();
		    foreach (Renderer renderer in sceneRenderersHidden)
		    {		
  	 	    	 renderer.enabled = true;	
		    }
		    return hiddenRenderers;
	    }
        
        /// <summary>
        /// Delete The contents of a Scene Layer
        /// </summary>
        /// <param name="sceneLayer"></param>
		public static void DeleteSceneLayerContents(SceneLayer sceneLayer)
		{
			List<GameObject> gameObjects = new List<GameObject> ();

			foreach(Transform child in sceneLayer.transform)
			{
				gameObjects.Add (child.gameObject);
			}

			foreach (GameObject gameObject in gameObjects)
			{
				GameObject.DestroyImmediate (gameObject);
			}
		}
	}
}
